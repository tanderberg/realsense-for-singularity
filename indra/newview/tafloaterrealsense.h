/** 
 * @file tafloaterrealsense.h
 * @brief RealSense preferences panel
 *
 * $LicenseInfo:firstyear=2014&license=viewergpl$
 * 
 * Copyright (c) 2014-2015, Tommy Anderberg. Portions of this code are derived
 * from llfloaterjoystick.h in this distribution, which is 
 * Copyright (c) 2007-2009, Linden Research, Inc.
 * 
 * The source code in this file ("Source Code") is provided by Tommy Anderberg
 * to you under the terms of the GNU General Public License, version 2.0
 * ("GPL"), unless you have obtained a separate licensing agreement
 * ("Other License"), formally executed by you and Tommy Anderberg. Terms of
 * the GPL can be found in doc/GPL-license.txt in this distribution.
 * 
 * There are special exceptions to the terms and conditions of the GPL as
 * it is applied to this Source Code. View the full text of the exception
 * in the file doc/FLOSS-exception.txt in this software distribution.
 * 
 * By copying, modifying or distributing this software, you acknowledge
 * that you have read and understood your obligations described above,
 * and agree to abide by those obligations.
 * 
 * ALL TOMMY ANDERBERG SOURCE CODE IS PROVIDED "AS IS." TOMMY ANDERBERG MAKES 
 * NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS ACCURACY, 
 * COMPLETENESS OR PERFORMANCE.
 * $/LicenseInfo$
 */

#ifndef TA_TAFLOATERREALSENSE_H
#define TA_TAFLOATERREALSENSE_H

#include "llfloater.h"
#include "llstatview.h"
#include "llcombobox.h"
#include "llradiogroup.h"
#include "llstartup.h"
#include "tarealsense.h"

class LLCheckBoxCtrl;
class LLSliderCtrl;

class TAFloaterRealSense : public LLFloater, public LLFloaterSingleton<TAFloaterRealSense>
{
public:
	TAFloaterRealSense(const LLSD& data);
	virtual ~TAFloaterRealSense();

	virtual BOOL postBuild();
	virtual void refresh();
	virtual void apply();
	virtual void restore();
	virtual void draw();
	void close(bool app = false);

	static void onVoiceStatus(void* self, S32 status);
	static void onVoiceRecognition(void* realsense_panel, const TCommandActionArgument* command_action_argument, U32 confidence);

private:
	EStartupState mPrevStartupState;
	bool mCloseOK;

	void loadPosition();
	void savePosition();

	S32 mLastVoiceStatus;
	void displayVoiceStatus(S32 status);
	void displayVoiceRecognition(const TCommandActionArgument* command_action_argument, U32 confidence);

	void setVoiceInputDevice(std::wstring deviceID, bool restorePoint);
	TCommandActionArgumentVector makeCommandActionArgumentVector();
	void loadVoiceCommandList(bool default, bool restorePoint);
	void saveVoiceCommandList(LLControlGroup &controlGroup, 
		TCommandActionArgumentVector &commandActionArgumentVector);

	void loadVoiceSettings(bool default, bool restorePoint);
	void loadHandSettings(bool default, bool restorePoint);
	void loadGestureSettings(bool defaults, bool restorePoint);
	void loadPoseSettings(bool defaults, bool restorePoint);
	void loadEmotionSettings(bool defaults, bool restorePoint);

	static void onCommitRealSenseEnabled(LLUICtrl*, void*);
	static void onClickVoicePause(void *realsense_panel);
	static void onClickAddToList(void *realsense_panel);
	static void onClickClearCommand(void *realsense_panel);
	static void onClickDropFromList(void *realsense_panel);
	static void onDoubleClickList(void *realsense_panel);
	static void onClickVoiceDefaults(void *realsense_panel);
	static void onClickHandDefaults(void*);
	static void onClickGestureDefaults(void*);
	static void onClickPoseDefaults(void*);
	static void onClickEmotionDefaults(void*);

	void cleanUpVoiceHandling();
	void saveChanges();

	static void onClickTest(void*);
	static void onClickCancel(void*);
	static void onClickOK(void*);

	// Device prefs
	bool mRealSenseEnabled;
	bool mVoiceCommandsEnabled;
	bool mHandTrackingEnabled;
	bool mPoseTrackingEnabled;
	bool mEmotionTrackingEnabled;

	bool mTouchlessMouseEnabled;

	U32 mVoiceInputLevel;
	U32 mVoiceCommandThreshold;
	F32 mTouchlessOffsetX;
	F32 mTouchlessOffsetY;
	F32 mHandSpeed;
	F32 mHandSmoothing;
	F32 mHandReloadSeconds;

	TCommandActionArgumentVector mCommandActionArgumentVector;

	std::wstring mVoiceInputDevice;

	std::string mSpreadFingersActionString;
	std::string mFistActionString;
	std::string mTapActionString;
	std::string mThumbDownActionString;
	std::string mThumbUpActionString;
	std::string mClickActionString;
	std::string mTwoFingersPinchOpenActionString;
	std::string mVSignActionString;
	std::string mFullPinchActionString;
	std::string mSwipeLeftActionString;
	std::string mSwipeRightActionString;
	std::string mWaveActionString;
	std::string mSwipeDownActionString;
	std::string mSwipeUpActionString;

	std::string mSpreadFingersArgument;
	std::string mFistArgument;
	std::string mTapArgument;
	std::string mThumbDownArgument;
	std::string mThumbUpArgument;
	std::string mClickArgument;
	std::string mTwoFingersPinchOpenArgument;
	std::string mVSignArgument;
	std::string mFullPinchArgument;
	std::string mSwipeLeftArgument;
	std::string mSwipeRightArgument;
	std::string mWaveArgument;
	std::string mSwipeDownArgument;
	std::string mSwipeUpArgument;

	S32 mHandBuildMode;

	F32 mYawDegreesOffset;
	F32 mPitchDegreesOffset;
	F32 mRollDegreesOffset;

	F32 mYawDegreesThreshold;
	F32 mPitchUpDegreesThreshold;
	F32 mPitchDownDegreesThreshold;
	F32 mRollDegreesThreshold;

	F32 mYawMaxMovesPerSecond;
	F32 mPitchMaxMovesPerSecond;
	F32 mRollMaxMovesPerSecond;

	S32 mPoseBuildMode;

	F32 mLoEmoThreshold;
	F32 mHiEmoThreshold;

	S32 mAngerEvidenceBias;
	S32 mContemptEvidenceBias;
	S32 mDisgustEvidenceBias;
	S32 mFearEvidenceBias;
	S32 mJoyEvidenceBias;
	S32 mSadnessEvidenceBias;
	S32 mSurpriseEvidenceBias;

	// Controls
	LLCheckBoxCtrl* mCheckRealSenseEnabled;

	LLCheckBoxCtrl* mCheckVoiceCommandsEnabled;
	LLCheckBoxCtrl* mCheckHandTrackingEnabled;
	LLCheckBoxCtrl* mCheckPoseTrackingEnabled;
	LLCheckBoxCtrl* mCheckEmotionTrackingEnabled;

	LLTextBox* mTextVoiceStatus[12];

	LLUICtrl* mSliderVoiceInputLevel;
	LLUICtrl* mSliderVoiceCommandThreshold;

	LLPanel* mPanelVoiceStatusInfo;
	LLTextBox* mTextVoiceStatusInfoConfidence;
	LLTextBox* mTextVoiceStatusInfo;

	LLLineEditor* mTextVoiceCommand;
	LLLineEditor* mTextVoiceArgument;

	LLCheckBoxCtrl* mCheckTouchlessMouseEnabled;

	LLUICtrl* mSliderTouchlessOffsetX;
	LLUICtrl* mSliderTouchlessOffsetY;

	LLUICtrl* mSliderHandSpeed;
	LLUICtrl* mSliderHandSmoothing;
	LLUICtrl* mSliderHandReloadSeconds;

	LLComboBox* mComboVoiceInputDevice;
	LLComboBox* mComboVoiceAction;

	LLComboBox* mComboSpreadFingers;
	LLComboBox* mComboFist;
	LLComboBox* mComboTap;
	LLComboBox* mComboThumbDown;
	LLComboBox* mComboThumbUp;
	LLComboBox* mComboClick;
	LLComboBox* mComboTwoFingersPinchOpen;
	LLComboBox* mComboVSign;
	LLComboBox* mComboFullPinch;
	LLComboBox* mComboSwipeLeft;
	LLComboBox* mComboSwipeRight;
	LLComboBox* mComboWave;
	LLComboBox* mComboSwipeDown;
	LLComboBox* mComboSwipeUp;

	LLLineEditor* mTextSpreadFingers;
	LLLineEditor* mTextFist;
	LLLineEditor* mTextTap;
	LLLineEditor* mTextThumbDown;
	LLLineEditor* mTextThumbUp;
	LLLineEditor* mTextClick;
	LLLineEditor* mTextTwoFingersPinchOpen;
	LLLineEditor* mTextVSign;
	LLLineEditor* mTextFullPinch;
	LLLineEditor* mTextSwipeLeft;
	LLLineEditor* mTextSwipeRight;
	LLLineEditor* mTextWave;
	LLLineEditor* mTextSwipeDown;
	LLLineEditor* mTextSwipeUp;

	LLScrollListCtrl* mVoiceCommandList;

	LLRadioGroup* mRadioGroupHandBuildMode;

	LLUICtrl* mSliderYawDegreesOffset;
	LLUICtrl* mSliderPitchDegreesOffset;
	LLUICtrl* mSliderRollDegreesOffset;

	LLUICtrl* mSliderYawDegreesThreshold;
	LLUICtrl* mSliderPitchUpDegreesThreshold;
	LLUICtrl* mSliderPitchDownDegreesThreshold;
	LLUICtrl* mSliderRollDegreesThreshold;

	LLUICtrl* mSliderYawMaxMovesPerSecond;
	LLUICtrl* mSliderPitchMaxMovesPerSecond;
	LLUICtrl* mSliderRollMaxMovesPerSecond;

	LLRadioGroup* mRadioGroupPoseBuildMode;

	LLUICtrl* mSliderLoEmoThreshold;
	LLUICtrl* mSliderHiEmoThreshold;

	LLUICtrl* mSliderAngerEvidenceBias;
	LLUICtrl* mSliderContemptEvidenceBias;
	LLUICtrl* mSliderDisgustEvidenceBias;
	LLUICtrl* mSliderFearEvidenceBias;
	LLUICtrl* mSliderJoyEvidenceBias;
	LLUICtrl* mSliderSadnessEvidenceBias;
	LLUICtrl* mSliderSurpriseEvidenceBias;
};

#endif // TA_TAFLOATERREALSENSE_H


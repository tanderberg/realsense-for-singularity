/** 
 * @file tarealsense.h
 * @brief  Support for Intel RealSense devices
 *
 * $LicenseInfo:firstyear=2014&license=viewergpl$
 * 
 * Copyright (c) 2014-2015, Tommy Anderberg.
 * 
 * The source code in this file ("Source Code") is provided by Tommy Anderberg
 * to you under the terms of the GNU General Public License, version 2.0
 * ("GPL"), unless you have obtained a separate licensing agreement
 * ("Other License"), formally executed by you and Tommy Anderberg. Terms of
 * the GPL can be found in doc/GPL-license.txt in this distribution.
 * 
 * By copying, modifying or distributing this software, you acknowledge
 * that you have read and understood your obligations described above,
 * and agree to abide by those obligations.
 * 
 * ALL TOMMY ANDERBERG SOURCE CODE IS PROVIDED "AS IS." TOMMY ANDERBERG MAKES 
 * NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS ACCURACY, 
 * COMPLETENESS OR PERFORMANCE.
 * $/LicenseInfo$
 */


#ifndef TA_TAREALSENSE_H
#define TA_TAREALSENSE_H

#define TA_INCLUDE_REALSENSE (LL_WINDOWS && _WIN64)

#include "stdtypes.h"
#include "lluuid.h"
#include "llcontrol.h"

struct TCommandActionArgument
{
	std::wstring command; 
	S32 action; 
	std::string argument; 
	S32 overrider; 
	S32 nextCandidate;

	TCommandActionArgument()
	{ 
		command = L"";
		action = -1;
		argument = "";
		overrider = -1;
		nextCandidate = -1;
	}
};

typedef std::vector<TCommandActionArgument> TCommandActionArgumentVector;

#if TA_INCLUDE_REALSENSE
#include "llselectmgr.h"			  // For access to EGridMode, LLSelectMgr
#include "llview.h"					  // For LLView
#include "pxcsession.h"			
#include "pxcsensemanager.h"
#include "pxcspeechrecognition.h"
#include "pxcfacedata.h"

#include <unordered_map>

class PXCSpeechRecognitionHandler: public PXCSpeechRecognition::Handler
{
public:
	PXCSpeechRecognitionHandler() { mPaused = false; mRSFloater = mOwner = NULL; mRSOnStatus = mOwnerOnStatus = NULL; 
									mRSOnRecognition = mOwnerOnRecognition = NULL; mOwnerOnDictation = NULL; 
									mNExpectedDictationSentences = 0; mMinDictationFrameTimeSeconds = 0; };
	virtual ~PXCSpeechRecognitionHandler() {};

	virtual void PXCAPI OnRecognition(const PXCSpeechRecognition::RecognitionData *data);
	virtual void PXCAPI OnAlert(const PXCSpeechRecognition::AlertData *data);

	void setPaused(bool dopause) { mPaused = dopause; }
	bool getPaused() { return mPaused; }
	void setExpectedDictationSentences(S32 n, F32 fromFrameTimeSeconds) 
		{ mNExpectedDictationSentences = n; mMinDictationFrameTimeSeconds = fromFrameTimeSeconds; }

	void setVoiceCommands(TCommandActionArgumentVector commandActionArgumentVector)	
		{ mCommandActionArgumentVector = commandActionArgumentVector; };

	S32 getNCommands() { return mCommandActionArgumentVector.size(); }

	TCommandActionArgument* getCommandActionArgumentPtr(S32 idx) 
		{ return ((idx >= 0) && (((std::size_t)idx) < mCommandActionArgumentVector.size())) ? &(mCommandActionArgumentVector[idx]) : NULL; }

	void setRSFloaterCallbacks(void* pFloater, void (*pOnStatus)(void*, S32), void (*pOnRecognition)(void*, const TCommandActionArgument*, U32)) 
		{ mRSFloater = pFloater; mRSOnStatus = pOnStatus; mRSOnRecognition = pOnRecognition; }

	void setOwnerCallbacks(void* pOwner, void (*pOnStatus)(void*, S32), 
						   void (*pOnRecognition)(void*, const TCommandActionArgument*, U32), void (*pOnDictation)(void*, const pxcCHAR*, bool)) 
		{ mOwner = pOwner; mOwnerOnStatus = pOnStatus; mOwnerOnRecognition = pOnRecognition; mOwnerOnDictation = pOnDictation; }

private:
	TCommandActionArgumentVector mCommandActionArgumentVector;
	TCommandActionArgument*  getOverriderPtr(S32 idx);
	bool mPaused;
	S32 mNExpectedDictationSentences;
	F32 mMinDictationFrameTimeSeconds;
	void* mRSFloater;
	void (*mRSOnStatus)(void*, S32);
	void (*mRSOnRecognition)(void*, const TCommandActionArgument*, U32);
	void* mOwner;
	void (*mOwnerOnStatus)(void*, S32);
	void (*mOwnerOnRecognition)(void*, const TCommandActionArgument*, U32);
	void (*mOwnerOnDictation)(void*, const pxcCHAR*, bool);
};

class PXCSenseManagerHandler: public PXCSenseManager::Handler
{
public:
	PXCSenseManagerHandler() { mOwner = NULL; mOwnerOnNewSample = NULL; }

	virtual pxcStatus PXCAPI OnModuleProcessedFrame(pxcUID streamID, PXCBase* module, PXCCapture::Sample* sample)
		{ if ((mOwner) && (mOwnerOnNewSample)) mOwnerOnNewSample(mOwner, streamID, module, sample); return PXC_STATUS_NO_ERROR; }

	void setOwnerCallbacks(void* pOwner, void (*pOnNewSample)(void*, pxcUID, PXCBase*, PXCCapture::Sample*))
		{ mOwner = pOwner; mOwnerOnNewSample = pOnNewSample; }

private:
	void* mOwner;
	void (*mOwnerOnNewSample)(void*, pxcUID, PXCBase*, PXCCapture::Sample*);
};
#endif

class TARealSense
{
private:
#if TA_INCLUDE_REALSENSE
	static const S32 BUILD_HEAD_TRACKING_OFF = 0;
	static const S32 BUILD_HEAD_TRACKING_NORMAL = 1;
	static const S32 BUILD_HEAD_TRACKING_ORBIT_CAMERA = 2;

	static const S32 BUILD_HAND_TRACKING_OFF = 0;
	static const S32 BUILD_HAND_TRACKING_NORMAL = 1;
	static const S32 BUILD_HAND_TRACKING_SPECIAL = 2;

	static const S32 HAND_UNKNOWN = -1;
	static const S32 HAND_RELAXED = 0;
	static const S32 HAND_OPEN = 1;
	static const S32 HAND_GUN = 2;
	static const S32 HAND_POINTING_FINGER = 3;
	static const S32 HAND_CLOSED = 4;

	static const S32 HANDBUILDSTATE_IDLE = 0;
	static const S32 HANDBUILDSTATE_SWITCH = 1;
	static const S32 HANDBUILDSTATE_MOVE = 2;
	static const S32 HANDBUILDSTATE_ROTATE_XY = 3;
	static const S32 HANDBUILDSTATE_ROTATE_XZ = 4;
	static const S32 HANDBUILDSTATE_ROTATE_YZ = 5;
	static const S32 HANDBUILDSTATE_RESIZE_UNIFORM = 6;
	static const S32 HANDBUILDSTATE_RESIZE_X = 7;
	static const S32 HANDBUILDSTATE_RESIZE_Y = 8;
	static const S32 HANDBUILDSTATE_RESIZE_Z = 9;
	static const S32 HANDBUILDSTATE_RESIZE_LEFT_X = 10;
	static const S32 HANDBUILDSTATE_RESIZE_LEFT_Y = 11;
	static const S32 HANDBUILDSTATE_RESIZE_LEFT_Z = 12;
	static const S32 HANDBUILDSTATE_RESIZE_RIGHT_X = 13;
	static const S32 HANDBUILDSTATE_RESIZE_RIGHT_Y = 14;
	static const S32 HANDBUILDSTATE_RESIZE_RIGHT_Z = 15;
	static const S32 HANDBUILDSTATE_TRACK_TWO = 16;

	typedef struct { const TCommandActionArgument* pCommandActionArgument; bool touchlessMouseOnly; bool enforceHandReload; } TActionTrigger;
	TActionTrigger mLastActionTrigger;

	std::unordered_map<std::string, WORD> mVKTable;

	PXCSession* mPXCSession;

	PXCSenseManager* mPXCSenseManager;
	PXCHandConfiguration* mPXCHandConfiguration;
	PXCFaceConfiguration* mPXCFaceConfiguration;
	PXCHandModule* mPXCHandModule;
	PXCFaceModule* mPXCFaceModule;
	PXCHandData* mPXCHandData;
	PXCFaceData* mPXCFaceData;
	PXCAudioSource* mPXCAudioSource;
	PXCSpeechRecognition* mPXCSpeechRecognition;
	PXCSpeechRecognition* mPXCSpeechRecognition_Dictation;

	PXCSenseManagerHandler mPXCSenseManagerHandler;

	void* mImageDataHandlerObject;
	void (*mOnImageData)(void*, void*, void*);

	static void onModuleProcessedFrame(void* self, pxcUID streamID, PXCBase* module, PXCCapture::Sample* sample);

	bool mHandsDirty;
	bool mFaceDirty;
	bool mEmotionsDirty;

	pxcBool mSpreadFingersDirty;
	pxcBool mFistDirty;
	pxcBool mTapDirty;
	pxcBool mThumbDownDirty;
	pxcBool mThumbUpDirty;
	pxcBool mTwoFingersPinchOpenDirty;
	pxcBool mVSignDirty;
	pxcBool mFullPinchDirty;
	pxcBool mSwipeLeftDirty;
	pxcBool mSwipeRightDirty;
	pxcBool mWaveDirty;
	pxcBool mClickDirty;
	pxcBool mSwipeDownDirty;
	pxcBool mSwipeUpDirty;

	void cleanGestures();
	void noteGestures();

	bool mShutdownRealSense;
	bool mShuttingDownRealSense;

	bool mRestartSenseManager;
	bool mStartingSenseManager;

	bool mBuildMode;

	F32 mStartScale;
	EGridMode mStartGridMode;
	LLQuaternion mStartRotation;
	
	S32 mPoseBuildMode;
	S32 mHandBuildMode;

	pxcI32 mHandOpenThreshold;
	pxcI32 mHandClosedThreshold;
	pxcI32 mExtendedFingerThreshold;
	pxcI32 mFoldedMiddleRingPinkyThreshold;

	pxcF32 mHandSpeed;
	pxcF32 mHandDistanceNorm;
	pxcF32 mHeadDistanceNorm;
	pxcF32 mPanDistanceNorm;
	pxcF32 mDeltaMetersToRad;
	pxcF32 mCameraOrbitStepDegrees;
	pxcF32 mSmoothingFactor;
	pxcF32 mMinRSDistance;

	LLVector3 mInitialRSMassCenterLeft;
	LLVector3 mInitialRSMassCenterRight;

	pxcF32 mInitialRSDistance;
	pxcF32 mInitialFaceDepth;

	F32 mInitialCameraObjectDistance;

	U32 mNRSDistanceUpdates;
	U32 mNRSMassCenterLeftUpdates;
	U32 mNRSMassCenterRightUpdates;
	U32 mNInitialRSMassCenterLeftUpdates;
	U32 mNInitialRSMassCenterRightUpdates;
	U32 mNFaceDepthUpdates;
	U32 mNInitialFaceDepthUpdates;
	U32 mNTouchlessCursorUpdates;

	S32 mHandStateLeft;
	S32 mHandStateRight;
	F32 mHandStateLastUpdatedLeft;
	F32 mHandStateLastUpdatedRight;
	F32 mLastSimulatedKey;
	F32 mLastSimulatedXUIClick;
	F32 mTouchlessMouseLastLeftClick;
	F32 mTouchlessMouseLastRightClick;
	PXCPointF32 mTouchlessCursor;
	PXCPointF32 mLatestDragPoint;

	S32 mHandBuildState;

	void updateTouchlessCursor(PXCPointF32 p);
	void moveTouchlessCursor();
	void updateHandState();

	F32 mRSDistance;
	LLVector3 mRSMassCenterLeft;
	LLVector3 mRSMassCenterRight;
	LLVector3 mRSMassCenterDelta;
	F32 mFaceDepth;

	F32 lowPass(F32 x, F32 y);
	void updateRSDistance(F32 rsDistance);
	void updateRSMassCenterLeft(PXCPoint3DF32 rsMassCenter);
	void updateRSMassCenterRight(PXCPoint3DF32 rsMassCenter);
	void updateFaceDepth(F32 faceDepth);
	void updateInitialRSMassCenterLeft();
	void updateInitialRSMassCenterRight();
	void updateInitialFaceDepth();
	void updateInitialCameraObjectDistance();

	LLVector3 real2sim(LLVector3 v);
	LLVector3 keepObjectInFrontOfCamera(LLVector3 simDeltas);

	void saveStartRotation(bool unconditional);

	LLVector3 sim2selectionFrame(LLVector3 v);
	LLVector3 selectionFrame2sim(LLVector3 v);

	F32 symmetricThreshold(F32 value, F32 threshold);
	LLVector3 deltaSimMassCenter(LLVector3 rsMassCenter, 
								 LLVector3 initialRSMassCenter);

	void defocusHandBuild();
	void setHandBuildState(int state);

	bool mEmotionTrackingEnabled;
	bool mEmotionTrackingPaused;

	LLUUID mEmotionAnimationID;

	pxcF32 mLoEmoThreshold;
	pxcF32 mHiEmoThreshold;

	pxcI32 mAngerEvidenceBias;
	pxcI32 mContemptEvidenceBias;
	pxcI32 mDisgustEvidenceBias;
	pxcI32 mFearEvidenceBias;
	pxcI32 mJoyEvidenceBias;
	pxcI32 mSadnessEvidenceBias;
	pxcI32 mSurpriseEvidenceBias;

	bool mPoseTrackingEnabled;
	bool mPoseTrackingPaused;

	pxcF32 mYawDegreesOffset;
	pxcF32 mPitchDegreesOffset;
	pxcF32 mRollDegreesOffset;

	pxcF32 mYawDegreesThreshold;
	pxcF32 mPitchUpDegreesThreshold;
	pxcF32 mPitchDownDegreesThreshold;
	pxcF32 mRollDegreesThreshold;

	F32 mYawMaxMovesPerSecond;
	F32 mPitchMaxMovesPerSecond;
	F32 mRollMaxMovesPerSecond;

	F32 mnYawMoves;
	F32 mnPitchMoves;
	F32 mnRollMoves;

	bool mHandTrackingEnabled;
	bool mHandTrackingPaused;

	TCommandActionArgument mSpreadFingersCommandActionArgument;
	TCommandActionArgument mFistCommandActionArgument;
	TCommandActionArgument mTapCommandActionArgument;
	TCommandActionArgument mThumbDownCommandActionArgument;
	TCommandActionArgument mThumbUpCommandActionArgument;
	TCommandActionArgument mTwoFingersPinchOpenCommandActionArgument;
	TCommandActionArgument mVSignCommandActionArgument;
	TCommandActionArgument mFullPinchCommandActionArgument;
	TCommandActionArgument mSwipeLeftCommandActionArgument;
	TCommandActionArgument mSwipeRightCommandActionArgument;
	TCommandActionArgument mWaveCommandActionArgument;
	TCommandActionArgument mClickCommandActionArgument;
	TCommandActionArgument mSwipeDownCommandActionArgument;
	TCommandActionArgument mSwipeUpCommandActionArgument;

	F32 mHandReloadSeconds;

	bool mDragLeft;
	bool mDragRight;

	bool mTouchlessMouseEnabled;

	PXCPointF32 mTouchlessOffset;

	std::wstring mVoiceInputDevice;
	U32 mVoiceInputLevel;
	U32 mVoiceCommandThreshold;
	F32 mDictationDelaySeconds;
	pxcCHAR** mVoiceCommands;
	size_t mNVoiceCommands;

	PXCSpeechRecognitionHandler mPXCSpeechRecognitionHandler;

	S32 mGear;
	S32 mGearTurning;
	S32 mGearVertical;
	F32 mMinGearTurningIntervalSeconds;
	F32 mLastGearTurning;
	F32 mLastPoseTime;

	void initVKTable();
	WORD string2keycode(std::string s);

	void clearFocusedControl();
	void setFocusedControlText(const pxcCHAR* pText);
	void takeDictation();

	typedef std::vector<WORD> TKeyStroke;
	TKeyStroke string2keystroke(std::string s);

	typedef std::vector<TKeyStroke> TKeyStrokeVector;
	TKeyStrokeVector string2keystrokeVector(std::string s);

	void simulateMouseDown(bool right);
	void simulateMouseUp(bool right);
	void simulateMouseMoveTo(POINT wp);
	void simulateMouseMoveBy(POINT delta);
	void simulateMouseClick(bool right);
	void simulateKey(WORD key);
	void simulateKeyStroke(TKeyStroke keyStroke);
	void simulateKeyStrokeSequence(TKeyStrokeVector keyStrokeVector);
	LLView* findView(std::string xuiPath);
	TCommandActionArgument* xuiClick(const TCommandActionArgument* pCommandActionArgument);

	U32 mMaxAgains;

	bool editingObject();
	void triggerActionCore(const TCommandActionArgument* pCommandActionArgument, bool touchlessMouseOnly, bool enforceHandReload);
	void triggerAction(const TCommandActionArgument* pCommandActionArgument, bool touchlessMouseOnly, bool enforceHandReload);
	S32 handState(PXCHandData::IHand* handData);

	void handleSpecialBuildModeGestures();
	S32 gesture2action(std::wstring gestureName, bool body_side_left);
	void handleNormalGestures(bool touchlessMouseOnly);

	PXCFaceData::PoseEulerAngles mPoseAngles;

	void notePose();
	void handlePose();
	void handleEmotions();

public:
	TARealSense() { mPXCSpeechRecognitionHandler.setOwnerCallbacks(this, &onVoiceStatus, 
																	&onVoiceRecognition, &onVoiceDictation);
					mPXCSenseManagerHandler.setOwnerCallbacks(this, &onModuleProcessedFrame); }

	static void onVoiceStatus(void* obj, S32 status);
	static void onVoiceRecognition(void* obj, const TCommandActionArgument* pCommandActionArgument, U32 confidence);
	static void onVoiceDictation(void* obj, const pxcCHAR* pSentence, bool lastSentence);

	static void say(std::string s, pxcF32 value);
	static void sayVector(std::string s, LLVector3 v);
	static void sayQuaternion(std::string s, LLQuaternion q);
#endif

public:
	// RealSense control and status

	static const S32 STATUS_UNKNOWN = 0;
	static const S32 STATUS_REALSENSE_DISABLED = 1;
	static const S32 STATUS_VOICE_DISABLED = 2;
	static const S32 STATUS_VOICE_PAUSED = 3;
	static const S32 STATUS_VOLUME_HIGH = 4;
	static const S32 STATUS_VOLUME_LOW = 5;
	static const S32 STATUS_SNR_LOW = 6;
	static const S32 STATUS_SPEECH_UNRECOGNIZABLE = 7;
	static const S32 STATUS_SPEECH_BEGIN = 8;
	static const S32 STATUS_SPEECH_END = 9;
	static const S32 STATUS_RECOGNITION_ABORTED = 10;
	static const S32 STATUS_RECOGNITION_ENDED = 11;

	static const S32 ACTION_NONE = 0;
	static const S32 ACTION_AGAIN = 1;
	static const S32 ACTION_STEP = 2;
	static const S32 ACTION_STEPBACK = 3;
	static const S32 ACTION_FORWARD = 4;
	static const S32 ACTION_BACKWARD = 5;
	static const S32 ACTION_RUN = 6;
	static const S32 ACTION_RUNBACK = 7;
	static const S32 ACTION_STOPHORIZONTAL = 8;
	static const S32 ACTION_LEFT = 9;
	static const S32 ACTION_RIGHT = 10;
	static const S32 ACTION_TURNLEFT = 11;
	static const S32 ACTION_TURNRIGHT = 12;
	static const S32 ACTION_STOPTURNING = 13;
	static const S32 ACTION_SLIDELEFT = 14;
	static const S32 ACTION_SLIDERIGHT = 15;
	static const S32 ACTION_UP = 16;
	static const S32 ACTION_DOWN = 17;
	static const S32 ACTION_CLIMB = 18;
	static const S32 ACTION_DESCEND = 19;
	static const S32 ACTION_STOPVERTICAL = 20;
	static const S32 ACTION_FLY = 21;
	static const S32 ACTION_STOPFLYING = 22;
	static const S32 ACTION_STOP = 23;
	static const S32 ACTION_SLOW = 24;
	static const S32 ACTION_FAST = 25;
	static const S32 ACTION_WHISPER = 26;
	static const S32 ACTION_SAY = 27;
	static const S32 ACTION_SHOUT = 28;
	static const S32 ACTION_REGIONSAY = 29;
	static const S32 ACTION_TALKON = 30;
	static const S32 ACTION_TALKOFF = 31;
	static const S32 ACTION_DICTATE = 32;
	static const S32 ACTION_CLEAR = 33;
	static const S32 ACTION_KEYSTROKE = 34;
	static const S32 ACTION_LEFTCLICK = 35;
	static const S32 ACTION_RIGHTCLICK = 36;
	static const S32 ACTION_LEFTDRAG = 37;
	static const S32 ACTION_RIGHTDRAG = 38;
	static const S32 ACTION_XUICLICK = 39;

	static const S32 IMAGE_WIDTH = 640;
	static const S32 IMAGE_HEIGHT = 360;

	typedef struct {std::wstring name; std::wstring id;} TVoiceInputDevice;
	typedef std::vector<TVoiceInputDevice> TVoiceInputDeviceVector;

	void initialize();
	void processFrame();

	void setOnImageData(void* pImageDataHandlerObject, void (*pOnImageData)(void*, void*, void*));

	void requestRealSenseShutdown();
	void requestSenseManagerReset();

	void runSenseManager(bool hands, bool poses, bool emotions, 
						 bool pauseHands, bool pausePoses, bool pauseEmotions);

	bool realSenseAvailable();
	void releaseSenseManager();
	void releaseSpeechRecognition();
	void enableRealSense(bool doenable);
	bool realSenseEnabled();

	void setBuildMode(bool buildmode);

	static std::wstring s2ws(std::string s);
	static std::string ws2s(std::wstring ws);

	static S32 stringToAction(std::string s);
	static std::string actionToString(S32 action);

	static LLSD loadControlValue(const std::string& name, bool default);

	void enableVoiceCommands(bool dopause);
	bool voiceCommandsEnabled();
	void pauseVoiceCommands(bool dopause);
	bool voiceCommandsPaused();
	bool voiceCommandsActive(); // i.e. enabled and not paused
	void setVoiceInputDevice(std::wstring deviceID);
	void setVoiceInputLevel(U32 level);
	void setVoiceCommandThreshold(U32 threshold);
	void setVoiceCommands(TCommandActionArgumentVector commandActionArgument);

	void setRSFloaterCallbacks(void* pFloater, void (*pOnStatus)(void*, S32), 
							   void (*pOnRecognition)(void*, const TCommandActionArgument*, U32));

	TVoiceInputDeviceVector getVoiceInputDevices();

	void loadVoiceCommandList(bool defaults);
	void restoreVoiceDefaults();

	void setTouchlessMouseEnabled(bool enable);
	bool getTouchlessMouseEnabled();
	void setTouchlessOffset(F32 x, F32 y);
	void setHandSpeed(F32 speed);
	void setHandSmoothing(F32 smoothing);
	void setHandReloadSeconds(F32 reloadSeconds);

	void enableHandTracking(bool dopause);
	bool handTrackingEnabled();
	void pauseHandTracking(bool dopause);
	bool handTrackingPaused();
	bool handTrackingActive(); // i.e. enabled and not paused
	void setHandBuildMode(S32 buildMode);

	void restoreHandDefaults();

	void setGestureAction(std::string action_string, S32 &action);
	void setSpreadFingersAction(std::string action_string);
	void setFistAction(std::string action_string);
	void setTapAction(std::string action_string);
	void setThumbDownAction(std::string action_string);
	void setThumbUpAction(std::string action_string);
	void setTwoFingersPinchOpenAction(std::string action_string);
	void setVSignAction(std::string action_string);
	void setFullPinchAction(std::string action_string);
	void setSwipeLeftAction(std::string action_string);
	void setSwipeRightAction(std::string action_string);
	void setWaveAction(std::string action_string);
	void setClickAction(std::string action_string);
	void setSwipeDownAction(std::string action_string);
	void setSwipeUpAction(std::string action_string);

	void setSpreadFingersArgument(std::string argument);
	void setFistArgument(std::string argument);
	void setTapArgument(std::string argument);
	void setThumbDownArgument(std::string argument);
	void setThumbUpArgument(std::string argument);
	void setTwoFingersPinchOpenArgument(std::string argument);
	void setVSignArgument(std::string argument);
	void setFullPinchArgument(std::string argument);
	void setSwipeLeftArgument(std::string argument);
	void setSwipeRightArgument(std::string argument);
	void setWaveArgument(std::string argument);
	void setClickArgument(std::string argument);
	void setSwipeDownArgument(std::string argument);
	void setSwipeUpArgument(std::string argument);
	void setPoseBuildMode(S32 builMode);

	void restoreGestureDefaults();

	void enablePoseTracking(bool dopause);
	bool poseTrackingEnabled();
	void pausePoseTracking(bool dopause);
	bool poseTrackingPaused();
	bool poseTrackingActive(); // i.e. enabled and not paused

	void setYawDegreesOffset(F32 offset);
	void setPitchDegreesOffset(F32 offset);
	void setRollDegreesOffset(F32 offset);

	void setYawDegreesThreshold(F32 degrees);
	void setPitchUpDegreesThreshold(F32 degrees);
	void setPitchDownDegreesThreshold(F32 degrees);
	void setRollDegreesThreshold(F32 degrees);

	void setYawMaxMovesPerSecond(F32 movesPerSecond);
	void setPitchMaxMovesPerSecond(F32 movesPerSecond);
	void setRollMaxMovesPerSecond(F32 movesPerSecond);

	void restorePoseDefaults();

	void enableEmotionTracking(bool dopause);
	bool emotionTrackingEnabled();
	void pauseEmotionTracking(bool dopause);
	bool emotionTrackingPaused();
	bool emotionTrackingActive(); // i.e. enabled and not paused

	void setEmotionThresholds(F32 lo, F32 hi);
	void setAngerEvidenceBias(S32 bias);
	void setContemptEvidenceBias(S32 bias);
	void setDisgustEvidenceBias(S32 bias);
	void setFearEvidenceBias(S32 bias);
	void setJoyEvidenceBias(S32 bias);
	void setSadnessEvidenceBias(S32 bias);
	void setSurpriseEvidenceBias(S32 bias);

	void restoreEmotionDefaults();

	void loadPersonalRealSenseSettings();
};

#endif // TA_TAREALSENSE_H




/** 
 * @file tafloaterrealsense.cpp
 * @brief RealSense preferences panel
 *
 * $LicenseInfo:firstyear=2014&license=viewergpl$
 * 
 * Copyright (c) 2014-2015, Tommy Anderberg. Portions of this code are derived
 * from llfloaterjoystick.cpp in this distribution, which is 
 * Copyright (c) 2007-2009, Linden Research, Inc.
 * 
 * The source code in this file ("Source Code") is provided by Tommy Anderberg
 * to you under the terms of the GNU General Public License, version 2.0
 * ("GPL"), unless you have obtained a separate licensing agreement
 * ("Other License"), formally executed by you and Tommy Anderberg. Terms of
 * the GPL can be found in doc/GPL-license.txt in this distribution.
 * 
 * There are special exceptions to the terms and conditions of the GPL as
 * it is applied to this Source Code. View the full text of the exception
 * in the file doc/FLOSS-exception.txt in this software distribution.
 * 
 * By copying, modifying or distributing this software, you acknowledge
 * that you have read and understood your obligations described above,
 * and agree to abide by those obligations.
 * 
 * ALL TOMMY ANDERBERG SOURCE CODE IS PROVIDED "AS IS." TOMMY ANDERBERG MAKES 
 * NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS ACCURACY, 
 * COMPLETENESS OR PERFORMANCE.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

// file include
#include "tafloaterrealsense.h"

// linden library includes
#include "llerror.h"
#include "llrect.h"
#include "llstring.h"

// project includes
#include "lluictrlfactory.h"
#include "llviewercontrol.h"
#include "llappviewer.h"
#include "llcheckboxctrl.h"

#include <boost/algorithm/string.hpp>

TAFloaterRealSense::TAFloaterRealSense(const LLSD& data)
	: LLFloater("FloaterRealSense")
{
	LLUICtrlFactory::getInstance()->buildFloater(this, "floater_realsense.xml");
	loadPosition();

	if (gFloaterView) gFloaterView->addChild(this);

	mLastVoiceStatus = TARealSense::STATUS_UNKNOWN;
	mPrevStartupState = STATE_FIRST;
	mCloseOK = false;
}

void TAFloaterRealSense::loadPosition()
{
	if ((LLStartUp::getStartupState() != STATE_STARTED) || 
		(gSavedPerAccountSettings.getBOOL("RealSenseFloaterCenter")))
	{
		center();
	}
	else
	{
		S32 bottom = max(0, gSavedPerAccountSettings.getS32("RealSenseFloaterBottom"));
		S32 left = max(0, gSavedPerAccountSettings.getS32("RealSenseFloaterLeft"));

		LLRect rect = getRect();

		const LLRect &parentRect = gFloaterView->getRect();

		if (left + rect.getWidth() > parentRect.mRight)
		{
			left = max(0, parentRect.mRight - rect.getWidth());
		}

		if (bottom + rect.getHeight() > parentRect.mTop)
		{
			bottom = max(0, parentRect.mTop - rect.getHeight());
		}

		rect.mRight = left + rect.getWidth();
		rect.mTop = bottom + rect.getHeight();
		rect.mLeft = left;
		rect.mBottom = bottom;

		setRect(rect);
	}
}

void TAFloaterRealSense::savePosition()
{
	if (LLStartUp::getStartupState() != STATE_STARTED) return;

	LLRect rect = getRect();

	gSavedPerAccountSettings.setS32("RealSenseFloaterBottom", rect.mBottom);
	gSavedPerAccountSettings.setS32("RealSenseFloaterLeft", rect.mLeft);
	gSavedPerAccountSettings.setBOOL("RealSenseFloaterCenter", false);
}

void TAFloaterRealSense::draw()
{
	TARealSense* pRS = LLAppViewer::instance()->getRealSensePointer();

	childSetEnabled("Enable RealSense", pRS->realSenseAvailable());

	EStartupState startupState = LLStartUp::getStartupState();

	bool state_started = (STATE_STARTED == startupState);

	if (startupState == mPrevStartupState)
	{
		// Startup state unchanged, only one button might need updating

		childSetEnabled("Drop button", state_started && mVoiceCommandList->getFirstSelected());
	}
	else
	{
		// Startup state changed, need to update all controls

		mPrevStartupState = startupState;

		// Main

		childSetEnabled("Enable voice", state_started);
		childSetEnabled("Enable hands", state_started);
		childSetEnabled("Enable pose", state_started);
		childSetEnabled("Enable emotion", state_started);

		childSetEnabled("Test button", state_started);

		// Voice

		childSetEnabled("Microphone combo", state_started);

		childSetEnabled("Microphone level", state_started);
		childSetEnabled("Command threshold", state_started);

		childSetEnabled("Voice command", state_started);
		childSetEnabled("Voice action", state_started);
		childSetEnabled("Voice argument", state_started);

		childSetEnabled("Add button", state_started);
		childSetEnabled("Clear button", state_started);
		childSetEnabled("Drop button", state_started && mVoiceCommandList->getFirstSelected());

		childSetEnabled("Command list", state_started); // Does this do anything?

		childSetEnabled("Voice defaults button", state_started);

		// Hands

		childSetEnabled("Enable touchless mouse", state_started);

		childSetEnabled("Horizontal offset", state_started);
		childSetEnabled("Vertical offset", state_started);

		childSetEnabled("Hand speed", state_started);
		childSetEnabled("Hand smoothing", state_started);

		childSetEnabled("Hand reload", state_started);
		childSetEnabled("Hands when building", state_started);

		childSetEnabled("Hand defaults button", state_started);

		// Gestures

		childSetEnabled("Spread fingers combo", state_started);
		childSetEnabled("Fist combo", state_started);
		childSetEnabled("Tap combo", state_started);
		childSetEnabled("Thumb down combo", state_started);
		childSetEnabled("Thumb up combo", state_started);
		childSetEnabled("Click combo", state_started);
		childSetEnabled("Two fingers pinch combo", state_started);
		childSetEnabled("V sign combo", state_started);
		childSetEnabled("Full pinch combo", state_started);
		childSetEnabled("Swipe left combo", state_started);
		childSetEnabled("Swipe right combo", state_started);
		childSetEnabled("Wave combo", state_started);
		childSetEnabled("Swipe down combo", state_started);
		childSetEnabled("Swipe up combo", state_started);

		childSetEnabled("Spread fingers argument", state_started);
		childSetEnabled("Fist argument", state_started);
		childSetEnabled("Tap argument", state_started);
		childSetEnabled("Thumb down argument", state_started);
		childSetEnabled("Thumb up argument", state_started);
		childSetEnabled("Click argument", state_started);
		childSetEnabled("Two fingers pinch argument", state_started);
		childSetEnabled("V sign argument", state_started);
		childSetEnabled("Full pinch argument", state_started);
		childSetEnabled("Swipe left argument", state_started);
		childSetEnabled("Swipe right argument", state_started);
		childSetEnabled("Wave argument", state_started);
		childSetEnabled("Swipe down argument", state_started);
		childSetEnabled("Swipe up argument", state_started);

		childSetEnabled("Gesture defaults button", state_started);

		// Pose

		childSetEnabled("Yaw offset", state_started);
		childSetEnabled("Pitch offset", state_started);
		childSetEnabled("Roll offset", state_started);

		childSetEnabled("Yaw threshold", state_started);
		childSetEnabled("Pitch up threshold", state_started);
		childSetEnabled("Pitch down threshold", state_started);
		childSetEnabled("Roll threshold", state_started);

		childSetEnabled("Yaw rate", state_started);
		childSetEnabled("Pitch rate", state_started);
		childSetEnabled("Roll rate", state_started);

		childSetEnabled("Head when building", state_started);

		childSetEnabled("Head defaults button", state_started);

		// Emotions

		childSetEnabled("Low emotion threshold", state_started);
		childSetEnabled("High emotion threshold", state_started);

		childSetEnabled("Anger evidence bias", state_started);
		childSetEnabled("Contempt evidence bias", state_started);
		childSetEnabled("Disgust evidence bias", state_started);
		childSetEnabled("Fear evidence bias", state_started);
		childSetEnabled("Joy evidence bias", state_started);
		childSetEnabled("Sadness evidence bias", state_started);
		childSetEnabled("Surprise evidence bias", state_started);

		childSetEnabled("Emotion defaults button", state_started);
	}

	LLFloater::draw();

	if (state_started)
	{
		// Voice status callbacks

		pRS->setRSFloaterCallbacks(this, &onVoiceStatus, &onVoiceRecognition);

		if (pRS->realSenseEnabled())
		{
			if (!pRS->voiceCommandsEnabled())
			{
				displayVoiceStatus(TARealSense::STATUS_VOICE_DISABLED);
			}
			else
			{
				if (pRS->voiceCommandsPaused())
				{
					displayVoiceStatus(TARealSense::STATUS_VOICE_PAUSED);
				}
				else
				{
					displayVoiceStatus(TARealSense::STATUS_UNKNOWN);
				}

				childSetEnabled("Pause button", true);
			}
		}
		else
		{
			displayVoiceStatus(TARealSense::STATUS_REALSENSE_DISABLED);
		}
	}
}

BOOL TAFloaterRealSense::postBuild()
{	
	// Main

	mCheckRealSenseEnabled			= getChild<LLCheckBoxCtrl>("Enable RealSense");

	mCheckVoiceCommandsEnabled		= getChild<LLCheckBoxCtrl>("Enable voice");
	mCheckHandTrackingEnabled		= getChild<LLCheckBoxCtrl>("Enable hands");
	mCheckPoseTrackingEnabled		= getChild<LLCheckBoxCtrl>("Enable pose");
	mCheckEmotionTrackingEnabled	= getChild<LLCheckBoxCtrl>("Enable emotion");

	// Voice

	mComboVoiceInputDevice										= getChild<LLComboBox>("Microphone combo");
	mSliderVoiceInputLevel										= getChild<LLUICtrl>("Microphone level");
	mSliderVoiceCommandThreshold								= getChild<LLUICtrl>("Command threshold");

	mTextVoiceStatus[TARealSense::STATUS_REALSENSE_DISABLED]	= getChild<LLTextBox>("voice_status_realsense_disabled");
	mTextVoiceStatus[TARealSense::STATUS_VOICE_DISABLED]		= getChild<LLTextBox>("voice_status_voice_disabled");
	mTextVoiceStatus[TARealSense::STATUS_VOICE_PAUSED]			= getChild<LLTextBox>("voice_status_voice_paused");
	mTextVoiceStatus[TARealSense::STATUS_VOLUME_HIGH]			= getChild<LLTextBox>("voice_status_volume_high");
	mTextVoiceStatus[TARealSense::STATUS_VOLUME_LOW]			= getChild<LLTextBox>("voice_status_volume_low");
	mTextVoiceStatus[TARealSense::STATUS_SNR_LOW]				= getChild<LLTextBox>("voice_status_snr_low");
	mTextVoiceStatus[TARealSense::STATUS_SPEECH_UNRECOGNIZABLE]	= getChild<LLTextBox>("voice_status_speech_unrecognizable");
	mTextVoiceStatus[TARealSense::STATUS_SPEECH_BEGIN]			= getChild<LLTextBox>("voice_status_speech_begin");
	mTextVoiceStatus[TARealSense::STATUS_SPEECH_END]			= getChild<LLTextBox>("voice_status_speech_end");
	mTextVoiceStatus[TARealSense::STATUS_RECOGNITION_ABORTED]	= getChild<LLTextBox>("voice_status_recognition_aborted");
	mTextVoiceStatus[TARealSense::STATUS_RECOGNITION_ENDED]		= getChild<LLTextBox>("voice_status_recognition_ended");

	mPanelVoiceStatusInfo										= getChild<LLPanel>("voice_status_info_panel");
	mTextVoiceStatusInfoConfidence								= getChild<LLTextBox>("voice_status_info_confidence");
	mTextVoiceStatusInfo										= getChild<LLTextBox>("voice_status_info_text");

	mTextVoiceCommand											= getChild<LLLineEditor>("Voice command");
	mComboVoiceAction											= getChild<LLComboBox>("Voice action");
	mTextVoiceArgument											= getChild<LLLineEditor>("Voice argument");

	mVoiceCommandList											= getChild<LLScrollListCtrl>("Command list");

	childSetAction("Pause button", onClickVoicePause, this);
	childSetAction("Add button", onClickAddToList, this);
	childSetAction("Clear button", onClickClearCommand, this);
	childSetAction("Drop button", onClickDropFromList, this);

	mVoiceCommandList->setDoubleClickCallback(onDoubleClickList, this);

	childSetAction("Voice defaults button", onClickVoiceDefaults, this);

	// Hands

	mCheckTouchlessMouseEnabled		 = getChild<LLCheckBoxCtrl>("Enable touchless mouse");

	mSliderTouchlessOffsetX			 = getChild<LLUICtrl>("Horizontal offset");
	mSliderTouchlessOffsetY			 = getChild<LLUICtrl>("Vertical offset");

	mSliderHandSpeed				 = getChild<LLUICtrl>("Hand speed");
	mSliderHandSmoothing			 = getChild<LLUICtrl>("Hand smoothing");
	mSliderHandReloadSeconds		 = getChild<LLUICtrl>("Hand reload");

	mRadioGroupHandBuildMode		 = getChild<LLRadioGroup>("Hands when building");

	childSetAction("Hand defaults button", onClickHandDefaults, this);

	// Gestures

	mComboSpreadFingers              = getChild<LLComboBox>("Spread fingers combo");
	mComboFist                       = getChild<LLComboBox>("Fist combo");
	mComboTap                        = getChild<LLComboBox>("Tap combo");
	mComboThumbDown                  = getChild<LLComboBox>("Thumb down combo");
	mComboThumbUp                    = getChild<LLComboBox>("Thumb up combo");
	mComboClick                      = getChild<LLComboBox>("Click combo");
	mComboTwoFingersPinchOpen        = getChild<LLComboBox>("Two fingers pinch combo");
	mComboVSign                      = getChild<LLComboBox>("V sign combo");
	mComboFullPinch                  = getChild<LLComboBox>("Full pinch combo");
	mComboSwipeLeft                  = getChild<LLComboBox>("Swipe left combo");
	mComboSwipeRight                 = getChild<LLComboBox>("Swipe right combo");
	mComboWave                       = getChild<LLComboBox>("Wave combo");
	mComboSwipeDown                  = getChild<LLComboBox>("Swipe down combo");
	mComboSwipeUp                    = getChild<LLComboBox>("Swipe up combo");

	mTextSpreadFingers               = getChild<LLLineEditor>("Spread fingers argument");
	mTextFist                        = getChild<LLLineEditor>("Fist argument");
	mTextTap                         = getChild<LLLineEditor>("Tap argument");
	mTextThumbDown                   = getChild<LLLineEditor>("Thumb down argument");
	mTextThumbUp                     = getChild<LLLineEditor>("Thumb up argument");
	mTextClick                       = getChild<LLLineEditor>("Click argument");
	mTextTwoFingersPinchOpen         = getChild<LLLineEditor>("Two fingers pinch argument");
	mTextVSign                       = getChild<LLLineEditor>("V sign argument");
	mTextFullPinch                   = getChild<LLLineEditor>("Full pinch argument");
	mTextSwipeLeft                   = getChild<LLLineEditor>("Swipe left argument");
	mTextSwipeRight                  = getChild<LLLineEditor>("Swipe right argument");
	mTextWave                        = getChild<LLLineEditor>("Wave argument");
	mTextSwipeDown                   = getChild<LLLineEditor>("Swipe down argument");
	mTextSwipeUp                     = getChild<LLLineEditor>("Swipe up argument");

	childSetAction("Gesture defaults button", onClickGestureDefaults, this);

	// Pose

	mSliderYawDegreesOffset          = getChild<LLUICtrl>("Yaw offset");
	mSliderPitchDegreesOffset        = getChild<LLUICtrl>("Pitch offset");
	mSliderRollDegreesOffset         = getChild<LLUICtrl>("Roll offset");

	mSliderYawDegreesThreshold       = getChild<LLUICtrl>("Yaw threshold");
	mSliderPitchUpDegreesThreshold   = getChild<LLUICtrl>("Pitch up threshold");
	mSliderPitchDownDegreesThreshold = getChild<LLUICtrl>("Pitch down threshold");
	mSliderRollDegreesThreshold      = getChild<LLUICtrl>("Roll threshold");

	mSliderYawMaxMovesPerSecond      = getChild<LLUICtrl>("Yaw rate");
	mSliderPitchMaxMovesPerSecond    = getChild<LLUICtrl>("Pitch rate");
	mSliderRollMaxMovesPerSecond     = getChild<LLUICtrl>("Roll rate");

	mRadioGroupPoseBuildMode         = getChild<LLRadioGroup>("Head when building");

	childSetAction("Head defaults button", onClickPoseDefaults, this);

	// Emotions

	mSliderLoEmoThreshold            = getChild<LLUICtrl>("Low emotion threshold");
	mSliderHiEmoThreshold            = getChild<LLUICtrl>("High emotion threshold");

	mSliderAngerEvidenceBias         = getChild<LLUICtrl>("Anger evidence bias");
	mSliderContemptEvidenceBias      = getChild<LLUICtrl>("Contempt evidence bias");
	mSliderDisgustEvidenceBias       = getChild<LLUICtrl>("Disgust evidence bias");
	mSliderFearEvidenceBias          = getChild<LLUICtrl>("Fear evidence bias");
	mSliderJoyEvidenceBias           = getChild<LLUICtrl>("Joy evidence bias");
	mSliderSadnessEvidenceBias       = getChild<LLUICtrl>("Sadness evidence bias");
	mSliderSurpriseEvidenceBias      = getChild<LLUICtrl>("Surprise evidence bias");

	childSetAction("Emotion defaults button", onClickEmotionDefaults, this);

	childSetAction("Cancel button", onClickCancel, this);
	childSetAction("Test button", onClickTest, this);
	childSetAction("OK button", onClickOK, this);

	refresh();

	return TRUE;
}

TAFloaterRealSense::~TAFloaterRealSense()
{
	// Nothing to do
}

void TAFloaterRealSense::onVoiceStatus(void* realsense_panel, S32 status)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;
		self->displayVoiceStatus(status);
	}
}

void TAFloaterRealSense::onVoiceRecognition(void* realsense_panel, const TCommandActionArgument* command_action_argument, U32 confidence)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;
		self->displayVoiceRecognition(command_action_argument, confidence);
	}
}

void TAFloaterRealSense::displayVoiceStatus(S32 status)
{
	if ((TARealSense::STATUS_UNKNOWN != status) || 
		(TARealSense::STATUS_REALSENSE_DISABLED == mLastVoiceStatus) || 
		(TARealSense::STATUS_VOICE_DISABLED == mLastVoiceStatus) ||
		(TARealSense::STATUS_VOICE_PAUSED == mLastVoiceStatus))
	{
		mTextVoiceStatus[TARealSense::STATUS_REALSENSE_DISABLED]->setVisible(TARealSense::STATUS_REALSENSE_DISABLED == status);
		mTextVoiceStatus[TARealSense::STATUS_VOICE_DISABLED]->setVisible(TARealSense::STATUS_VOICE_DISABLED == status);
		mTextVoiceStatus[TARealSense::STATUS_VOICE_PAUSED]->setVisible(TARealSense::STATUS_VOICE_PAUSED == status);
		mTextVoiceStatus[TARealSense::STATUS_VOLUME_HIGH]->setVisible(TARealSense::STATUS_VOLUME_HIGH == status);
		mTextVoiceStatus[TARealSense::STATUS_VOLUME_LOW]->setVisible(TARealSense::STATUS_VOLUME_LOW == status);
		mTextVoiceStatus[TARealSense::STATUS_SNR_LOW]->setVisible(TARealSense::STATUS_SNR_LOW == status);
		mTextVoiceStatus[TARealSense::STATUS_SPEECH_UNRECOGNIZABLE]->setVisible(TARealSense::STATUS_SPEECH_UNRECOGNIZABLE == status);
		mTextVoiceStatus[TARealSense::STATUS_SPEECH_BEGIN]->setVisible(TARealSense::STATUS_SPEECH_BEGIN == status);
		mTextVoiceStatus[TARealSense::STATUS_SPEECH_END]->setVisible(TARealSense::STATUS_SPEECH_END == status);
		mTextVoiceStatus[TARealSense::STATUS_RECOGNITION_ABORTED]->setVisible(TARealSense::STATUS_RECOGNITION_ABORTED == status);
		mTextVoiceStatus[TARealSense::STATUS_RECOGNITION_ENDED]->setVisible(TARealSense::STATUS_RECOGNITION_ENDED == status);

		mPanelVoiceStatusInfo->setVisible(false);

		mLastVoiceStatus = status;
	}
}

void TAFloaterRealSense::displayVoiceRecognition(const TCommandActionArgument* command_action_argument, U32 confidence)
{
	mTextVoiceCommand->setValue(TARealSense::ws2s(command_action_argument->command));
	mComboVoiceAction->setValue(TARealSense::actionToString(command_action_argument->action));
	mTextVoiceArgument->setValue(command_action_argument->argument);

	displayVoiceStatus(1000); // Make sure all status texts are invisible

	std::ostringstream s;
	s << mTextVoiceStatusInfoConfidence->getText() << " " << confidence;
	std::string ss(s.str());

	mTextVoiceStatusInfo->setText(ss);
	mPanelVoiceStatusInfo->setVisible(true);
}

void TAFloaterRealSense::apply()
{
	// Main settings

	bool realsense_enabled = mCheckRealSenseEnabled->get();
	bool voice_enabled = mCheckVoiceCommandsEnabled->get();
	bool hands_enabled = mCheckHandTrackingEnabled->get();
	bool pose_enabled = mCheckPoseTrackingEnabled->get();
	bool emotion_enabled = mCheckEmotionTrackingEnabled->get();

	TARealSense* pRS = LLAppViewer::instance()->getRealSensePointer();

	// Voice settings

	pRS->setVoiceInputDevice(TARealSense::s2ws(mComboVoiceInputDevice->getSelectedValue()));
	pRS->setVoiceInputLevel(mSliderVoiceInputLevel->getValue().asInteger());
	pRS->setVoiceCommandThreshold(mSliderVoiceCommandThreshold->getValue().asInteger());

	pRS->setVoiceCommands(makeCommandActionArgumentVector());

	// Hand settings

	pRS->setTouchlessMouseEnabled(mCheckTouchlessMouseEnabled->get());
	pRS->setTouchlessOffset(mSliderTouchlessOffsetX->getValue(), 
							mSliderTouchlessOffsetY->getValue());
	pRS->setHandSpeed(mSliderHandSpeed->getValue());
	pRS->setHandSmoothing(mSliderHandSmoothing->getValue());
	pRS->setHandReloadSeconds(mSliderHandReloadSeconds->getValue());

	pRS->setHandBuildMode(mRadioGroupHandBuildMode->getSelectedIndex());

	// Gesture settings

	pRS->setSpreadFingersAction(mComboSpreadFingers->getSelectedValue());
	pRS->setFistAction(mComboFist->getSelectedValue());
	pRS->setTapAction(mComboTap->getSelectedValue());
	pRS->setThumbDownAction(mComboThumbDown->getSelectedValue());
	pRS->setThumbUpAction(mComboThumbUp->getSelectedValue());
	pRS->setClickAction(mComboClick->getSelectedValue());
	pRS->setTwoFingersPinchOpenAction(mComboTwoFingersPinchOpen->getSelectedValue());
	pRS->setVSignAction(mComboVSign->getSelectedValue());
	pRS->setFullPinchAction(mComboFullPinch->getSelectedValue());
	pRS->setSwipeLeftAction(mComboSwipeLeft->getSelectedValue());
	pRS->setSwipeRightAction(mComboSwipeRight->getSelectedValue());
	pRS->setWaveAction(mComboWave->getSelectedValue());
	pRS->setSwipeDownAction(mComboSwipeDown->getSelectedValue());
	pRS->setSwipeUpAction(mComboSwipeUp->getSelectedValue());

	pRS->setSpreadFingersArgument(mTextSpreadFingers->getText());
	pRS->setFistArgument(mTextFist->getText());
	pRS->setTapArgument(mTextTap->getText());
	pRS->setThumbDownArgument(mTextThumbDown->getText());
	pRS->setThumbUpArgument(mTextThumbUp->getText());
	pRS->setClickArgument(mTextClick->getText());
	pRS->setTwoFingersPinchOpenArgument(mTextTwoFingersPinchOpen->getText());
	pRS->setVSignArgument(mTextVSign->getText());
	pRS->setFullPinchArgument(mTextFullPinch->getText());
	pRS->setSwipeLeftArgument(mTextSwipeLeft->getText());
	pRS->setSwipeRightArgument(mTextSwipeRight->getText());
	pRS->setWaveArgument(mTextWave->getText());
	pRS->setSwipeDownArgument(mTextSwipeDown->getText());
	pRS->setSwipeUpArgument(mTextSwipeUp->getText());

	// Pose settings

	pRS->setYawDegreesOffset(mSliderYawDegreesOffset->getValue());
	pRS->setPitchDegreesOffset(mSliderPitchDegreesOffset->getValue());
	pRS->setRollDegreesOffset(mSliderRollDegreesOffset->getValue());

	pRS->setYawDegreesThreshold(mSliderYawDegreesThreshold->getValue());
	pRS->setPitchUpDegreesThreshold(mSliderPitchUpDegreesThreshold->getValue());
	pRS->setPitchDownDegreesThreshold(mSliderPitchDownDegreesThreshold->getValue());
	pRS->setRollDegreesThreshold(mSliderRollDegreesThreshold->getValue());

	pRS->setYawMaxMovesPerSecond(mSliderYawMaxMovesPerSecond->getValue());
	pRS->setPitchMaxMovesPerSecond(mSliderPitchMaxMovesPerSecond->getValue());
	pRS->setRollMaxMovesPerSecond(mSliderRollMaxMovesPerSecond->getValue());

	pRS->setPoseBuildMode(mRadioGroupPoseBuildMode->getSelectedIndex());

	// Emotion settings

	pRS->setEmotionThresholds(mSliderLoEmoThreshold->getValue(), 
							  mSliderHiEmoThreshold->getValue());

	pRS->setAngerEvidenceBias(mSliderAngerEvidenceBias->getValue());
	pRS->setContemptEvidenceBias(mSliderContemptEvidenceBias->getValue());
	pRS->setDisgustEvidenceBias(mSliderDisgustEvidenceBias->getValue());
	pRS->setFearEvidenceBias(mSliderFearEvidenceBias->getValue());
	pRS->setJoyEvidenceBias(mSliderJoyEvidenceBias->getValue());
	pRS->setSadnessEvidenceBias(mSliderSadnessEvidenceBias->getValue());
	pRS->setSurpriseEvidenceBias(mSliderSurpriseEvidenceBias->getValue());

	// Apply

	if (voice_enabled)
	{
		LLButton* pauseButton = getChild<LLButton>("Pause button");
		pRS->enableVoiceCommands((pauseButton && pauseButton->getToggleState()));
	}
	else
	{
		pRS->pauseVoiceCommands(true);
	}

	if (hands_enabled)
	{
		pRS->enableHandTracking(false);
	}
	else
	{
		pRS->pauseHandTracking(true);
	}

	if (pose_enabled)
	{
		pRS->enablePoseTracking(false);
	}
	else
	{
		pRS->pausePoseTracking(true);
	}

	if (emotion_enabled)
	{
		pRS->enableEmotionTracking(false);
	}
	else
	{
		pRS->pauseEmotionTracking(true);
	}

	if (realsense_enabled != pRS->realSenseEnabled())
	{
		if (realsense_enabled) 
		{
			pRS->requestSenseManagerReset();
		}
		else
		{
			pRS->requestRealSenseShutdown();
		}
	}
}

void TAFloaterRealSense::setVoiceInputDevice(std::wstring deviceID, bool restorePoint)
{
	// Get list of available device IDs from RealSense

	TARealSense* pRS = LLAppViewer::instance()->getRealSensePointer();
	TARealSense::TVoiceInputDeviceVector devices = pRS->getVoiceInputDevices();

	// Populate voice combo box

	mComboVoiceInputDevice->removeall();

	if (0 >= devices.size()) return;

	for (TARealSense::TVoiceInputDeviceVector::iterator element = devices.begin(); element != devices.end(); ++element)
	{
		mComboVoiceInputDevice->add(TARealSense::ws2s(element->name), TARealSense::ws2s(element->id));

		if (deviceID == element->id)
		{
			if (restorePoint) mVoiceInputDevice = deviceID;
			mComboVoiceInputDevice->setValue(TARealSense::ws2s(deviceID));
		}
	}
}

TCommandActionArgumentVector TAFloaterRealSense::makeCommandActionArgumentVector()
{
	std::vector<LLScrollListItem*> listItems = mVoiceCommandList->getAllData();

	TCommandActionArgumentVector result;
	result.reserve(listItems.size());

	std::vector<LLScrollListItem*>::iterator iter;
	for(iter = listItems.begin(); iter !=listItems.end(); iter++)
	{
		TCommandActionArgument command_action_argument;

		command_action_argument.command = TARealSense::s2ws((*iter)->getColumn(0)->getValue());
		command_action_argument.action = TARealSense::stringToAction((*iter)->getColumn(1)->getValue());
		command_action_argument.argument = (*iter)->getColumn(2)->getValue();

		command_action_argument.overrider = -1;		// Not set
		command_action_argument.nextCandidate = -1;	// Not set

		result.push_back(command_action_argument);
	}

	return result;
}

void TAFloaterRealSense::loadVoiceCommandList(bool defaults, bool restorePoint)
{
	mVoiceCommandList->deleteAllItems();

	if (restorePoint) mCommandActionArgumentVector.clear();

	U32 nCommands = TARealSense::loadControlValue("RealSenseVoiceCommandCount", defaults).asInteger();

	for (U32 i = 0; i < nCommands; i++)
	{
		TCommandActionArgument command_action_argument;

		std::string nr = std::to_string((ULONGLONG)i);
		std::string command = TARealSense::loadControlValue("RealSenseVoiceCommand#" + nr, defaults);

		boost::algorithm::trim(command);
		if (command.empty()) continue;

		command_action_argument.command = TARealSense::s2ws(command);
		command_action_argument.action = TARealSense::stringToAction(TARealSense::loadControlValue("RealSenseVoiceAction#" + nr, defaults));

		if (!command_action_argument.action) continue;

		command_action_argument.overrider = -1;		// Not set
		command_action_argument.nextCandidate = -1;	// Not set

		command_action_argument.argument = TARealSense::loadControlValue("RealSenseVoiceArgument#" + nr, defaults);

		if (restorePoint) mCommandActionArgumentVector.push_back(command_action_argument);

		LLScrollListItem::Params row;
		row.value = command;

		LLScrollListCell::Params command_column;
		command_column.column = "column_command";
		command_column.type = "text";
		command_column.value = command;

		LLScrollListCell::Params action_column;
		action_column.column = "column_action";
		action_column.type = "text";
		action_column.value = TARealSense::actionToString(command_action_argument.action);

		LLScrollListCell::Params argument_column;
		argument_column.column = "column_argument";
		argument_column.type = "text";
		argument_column.value = command_action_argument.argument;

		row.columns.add(command_column);
		row.columns.add(action_column);
		row.columns.add(argument_column);

		mVoiceCommandList->addRow(row);
	}
}

void TAFloaterRealSense::saveVoiceCommandList(LLControlGroup &controlGroup, 
											  TCommandActionArgumentVector &commandActionArgumentVector)
{
	U32 nCommands = commandActionArgumentVector.size();

	for (U32 i = 0; i < nCommands; i++)
	{
		std::string nr = std::to_string((ULONGLONG)i);

		std::string name_command = "RealSenseVoiceCommand#" + nr;
		std::string name_action = "RealSenseVoiceAction#" + nr;
		std::string name_argument = "RealSenseVoiceArgument#" + nr;

		if (!controlGroup.getControl(name_command))
		{
			controlGroup.declareString(name_command, LLStringUtil::null, "Autodeclared");
		}

		controlGroup.setString(name_command, TARealSense::ws2s(commandActionArgumentVector[i].command));

		if (!controlGroup.getControl(name_action))
		{
			controlGroup.declareString(name_action, LLStringUtil::null, "Autodeclared");
		}

		controlGroup.setString(name_action, TARealSense::actionToString(commandActionArgumentVector[i].action));
		
		if (!controlGroup.getControl(name_argument))
		{
			controlGroup.declareString(name_argument, LLStringUtil::null, "Autodeclared");
		}

		controlGroup.setString(name_argument, commandActionArgumentVector[i].argument);
	}

	controlGroup.setU32("RealSenseVoiceCommandCount", nCommands);
}

void TAFloaterRealSense::loadVoiceSettings(bool defaults, bool restorePoint)
{
	setVoiceInputDevice(TARealSense::s2ws(TARealSense::loadControlValue("RealSenseVoiceInputDevice", defaults)), restorePoint);

	U32 voiceInputLevel = TARealSense::loadControlValue("RealSenseVoiceInputLevel", defaults).asInteger();
	U32 voiceCommandThreshold = TARealSense::loadControlValue("RealSenseVoiceCommandThreshold", defaults).asInteger();

	mSliderVoiceInputLevel->setValue(LLSD::Integer(voiceInputLevel));
	mSliderVoiceCommandThreshold->setValue(LLSD::Integer(voiceCommandThreshold));

	if (restorePoint)
	{
		mVoiceInputLevel = voiceInputLevel;
		mVoiceCommandThreshold = voiceCommandThreshold;
	}

	loadVoiceCommandList(defaults, restorePoint);
}

void TAFloaterRealSense::loadHandSettings(bool defaults, bool restorePoint)
{
	BOOL touchlessMouseEnabled = TARealSense::loadControlValue("RealSenseTouchlessMouseEnabled", defaults);
	F32 touchlessOffsetX = TARealSense::loadControlValue("RealSenseTouchlessOffsetX", defaults);
	F32 touchlessOffsetY = TARealSense::loadControlValue("RealSenseTouchlessOffsetY", defaults);
	F32 handSpeed = TARealSense::loadControlValue("RealSenseHandSpeed", defaults);
	F32 handSmoothing = TARealSense::loadControlValue("RealSenseHandSmoothing", defaults);
	F32 handReloadSeconds = TARealSense::loadControlValue("RealSenseHandReloadSeconds", defaults);

	S32 handBuildMode = TARealSense::loadControlValue("RealSenseHandBuildMode", defaults);

	mCheckTouchlessMouseEnabled->set(touchlessMouseEnabled);
	mSliderTouchlessOffsetX->setValue(touchlessOffsetX);
	mSliderTouchlessOffsetY->setValue(touchlessOffsetY);
	mSliderHandSpeed->setValue(handSpeed);
	mSliderHandSmoothing->setValue(handSmoothing);
	mSliderHandReloadSeconds->setValue(handReloadSeconds);

	mRadioGroupHandBuildMode->setSelectedIndex(handBuildMode);

	if (restorePoint)
	{
		mTouchlessMouseEnabled = touchlessMouseEnabled;
		mTouchlessOffsetX = touchlessOffsetX;
		mTouchlessOffsetY = touchlessOffsetY;
		mHandSpeed = handSpeed;
		mHandSmoothing = handSmoothing;
		mHandReloadSeconds = handReloadSeconds;

		mHandBuildMode = handBuildMode;
	}
}

void TAFloaterRealSense::loadGestureSettings(bool defaults, bool restorePoint)
{
	std::string spreadFingersActionString = TARealSense::loadControlValue("RealSenseGestureSpreadFingersAction", defaults);
	std::string fistActionString = TARealSense::loadControlValue("RealSenseGestureFistAction", defaults);
	std::string tapActionString = TARealSense::loadControlValue("RealSenseGestureTapAction", defaults);
	std::string thumbDownActionString = TARealSense::loadControlValue("RealSenseGestureThumbDownAction", defaults);
	std::string thumbUpActionString = TARealSense::loadControlValue("RealSenseGestureThumbUpAction", defaults);
	std::string clickActionString = TARealSense::loadControlValue("RealSenseGestureClickAction", defaults);
	std::string twoFingersPinchOpenActionString = TARealSense::loadControlValue("RealSenseGestureTwoFingersPinchOpenAction", defaults);
	std::string vSignActionString = TARealSense::loadControlValue("RealSenseGestureVSignAction", defaults);
	std::string fullPinchActionString = TARealSense::loadControlValue("RealSenseGestureFullPinchAction", defaults);
	std::string swipeLeftActionString = TARealSense::loadControlValue("RealSenseGestureSwipeLeftAction", defaults);
	std::string swipeRightActionString = TARealSense::loadControlValue("RealSenseGestureSwipeRightAction", defaults);
	std::string waveActionString = TARealSense::loadControlValue("RealSenseGestureWaveAction", defaults);
	std::string swipeDownActionString = TARealSense::loadControlValue("RealSenseGestureSwipeDownAction", defaults);
	std::string swipeUpActionString = TARealSense::loadControlValue("RealSenseGestureSwipeUpAction", defaults);

	mComboSpreadFingers->setValue(spreadFingersActionString);
	mComboFist->setValue(fistActionString);
	mComboTap->setValue(tapActionString);
	mComboThumbDown->setValue(thumbDownActionString);
	mComboThumbUp->setValue(thumbUpActionString);
	mComboClick->setValue(clickActionString);
	mComboTwoFingersPinchOpen->setValue(twoFingersPinchOpenActionString);
	mComboVSign->setValue(vSignActionString);
	mComboFullPinch->setValue(fullPinchActionString);
	mComboSwipeLeft->setValue(swipeLeftActionString);
	mComboSwipeRight->setValue(swipeRightActionString);
	mComboWave->setValue(waveActionString);
	mComboSwipeDown->setValue(swipeDownActionString);
	mComboSwipeUp->setValue(swipeUpActionString);

	std::string spreadFingersArgument = TARealSense::loadControlValue("RealSenseGestureSpreadFingersArgument", defaults);
	std::string fistArgument = TARealSense::loadControlValue("RealSenseGestureFistArgument", defaults);
	std::string tapArgument = TARealSense::loadControlValue("RealSenseGestureTapArgument", defaults);
	std::string thumbDownArgument = TARealSense::loadControlValue("RealSenseGestureThumbDownArgument", defaults);
	std::string thumbUpArgument = TARealSense::loadControlValue("RealSenseGestureThumbUpArgument", defaults);
	std::string clickArgument = TARealSense::loadControlValue("RealSenseGestureClickArgument", defaults);
	std::string twoFingersPinchOpenArgument = TARealSense::loadControlValue("RealSenseGestureTwoFingersPinchOpenArgument", defaults);
	std::string vSignArgument = TARealSense::loadControlValue("RealSenseGestureVSignArgument", defaults);
	std::string fullPinchArgument = TARealSense::loadControlValue("RealSenseGestureFullPinchArgument", defaults);
	std::string swipeLeftArgument = TARealSense::loadControlValue("RealSenseGestureSwipeLeftArgument", defaults);
	std::string swipeRightArgument = TARealSense::loadControlValue("RealSenseGestureSwipeRightArgument", defaults);
	std::string waveArgument = TARealSense::loadControlValue("RealSenseGestureWaveArgument", defaults);
	std::string swipeDownArgument = TARealSense::loadControlValue("RealSenseGestureSwipeDownArgument", defaults);
	std::string swipeUpArgument = TARealSense::loadControlValue("RealSenseGestureSwipeUpArgument", defaults);

	mTextSpreadFingers->setText(spreadFingersArgument);
	mTextFist->setText(fistArgument);
	mTextTap->setText(tapArgument);
	mTextThumbDown->setText(thumbDownArgument);
	mTextThumbUp->setText(thumbUpArgument);
	mTextClick->setText(clickArgument);
	mTextTwoFingersPinchOpen->setText(twoFingersPinchOpenArgument);
	mTextVSign->setText(vSignArgument);
	mTextFullPinch->setText(fullPinchArgument);
	mTextSwipeLeft->setText(swipeLeftArgument);
	mTextSwipeRight->setText(swipeRightArgument);
	mTextWave->setText(waveArgument);
	mTextSwipeDown->setText(swipeDownArgument);
	mTextSwipeUp->setText(swipeUpArgument);

	if (restorePoint)
	{
		mSpreadFingersActionString = spreadFingersActionString;
		mFistActionString = fistActionString;
		mTapActionString = tapActionString;
		mThumbDownActionString = thumbDownActionString;
		mThumbUpActionString = thumbUpActionString;
		mClickActionString = clickActionString;
		mTwoFingersPinchOpenActionString = twoFingersPinchOpenActionString;
		mVSignActionString = vSignActionString;
		mFullPinchActionString = fullPinchActionString;
		mSwipeLeftActionString = swipeLeftActionString;
		mSwipeRightActionString = swipeRightActionString;
		mWaveActionString = waveActionString;
		mSwipeDownActionString = swipeDownActionString;
		mSwipeUpActionString = swipeUpActionString;

		mSpreadFingersArgument = spreadFingersArgument;
		mFistArgument = fistArgument;
		mTapArgument = tapArgument;
		mThumbDownArgument = thumbDownArgument;
		mThumbUpArgument = thumbUpArgument;
		mClickArgument = clickArgument;
		mTwoFingersPinchOpenArgument = twoFingersPinchOpenArgument;
		mVSignArgument = vSignArgument;
		mFullPinchArgument = fullPinchArgument;
		mSwipeLeftArgument = swipeLeftArgument;
		mSwipeRightArgument = swipeRightArgument;
		mWaveArgument = waveArgument;
		mSwipeDownArgument = swipeDownArgument;
		mSwipeUpArgument = swipeUpArgument;
	}
}

void TAFloaterRealSense::loadPoseSettings(bool defaults, bool restorePoint)
{
	F32 yawDegreesOffset = TARealSense::loadControlValue("RealSensePoseYawDegreesOffset", defaults);
	F32 pitchDegreesOffset = TARealSense::loadControlValue("RealSensePosePitchDegreesOffset", defaults);
	F32 rollDegreesOffset = TARealSense::loadControlValue("RealSensePoseRollDegreesOffset", defaults);

	F32 yawDegreesThreshold = TARealSense::loadControlValue("RealSensePoseYawDegreesThreshold", defaults);
	F32 pitchUpDegreesThreshold = TARealSense::loadControlValue("RealSensePosePitchUpDegreesThreshold", defaults);
	F32 pitchDownDegreesThreshold = TARealSense::loadControlValue("RealSensePosePitchDownDegreesThreshold", defaults);
	F32 rollDegreesThreshold = TARealSense::loadControlValue("RealSensePoseRollDegreesThreshold", defaults);

	F32 yawMaxMovesPerSecond = TARealSense::loadControlValue("RealSensePoseYawMaxMovesPerSecond", defaults);
	F32 pitchMaxMovesPerSecond = TARealSense::loadControlValue("RealSensePosePitchMaxMovesPerSecond", defaults);
	F32 rollMaxMovesPerSecond = TARealSense::loadControlValue("RealSensePoseRollMaxMovesPerSecond", defaults);

	F32 poseBuildMode = TARealSense::loadControlValue("RealSensePoseBuildMode", defaults);

	mSliderYawDegreesOffset->setValue(yawDegreesOffset);
	mSliderPitchDegreesOffset->setValue(pitchDegreesOffset);
	mSliderRollDegreesOffset->setValue(rollDegreesOffset);

	mSliderYawDegreesThreshold->setValue(yawDegreesThreshold);
	mSliderPitchUpDegreesThreshold->setValue(pitchUpDegreesThreshold);
	mSliderPitchDownDegreesThreshold->setValue(pitchDownDegreesThreshold);
	mSliderRollDegreesThreshold->setValue(rollDegreesThreshold);

	mSliderYawMaxMovesPerSecond->setValue(yawMaxMovesPerSecond);
	mSliderPitchMaxMovesPerSecond->setValue(pitchMaxMovesPerSecond);
	mSliderRollMaxMovesPerSecond->setValue(rollMaxMovesPerSecond);

	mRadioGroupPoseBuildMode->setSelectedIndex(poseBuildMode);

	if (restorePoint)
	{
		mYawDegreesOffset = yawDegreesOffset;
		mPitchDegreesOffset = pitchDegreesOffset;
		mRollDegreesOffset = rollDegreesOffset;

		mYawDegreesThreshold = yawDegreesThreshold;
		mPitchUpDegreesThreshold = pitchUpDegreesThreshold;
		mPitchDownDegreesThreshold = pitchDownDegreesThreshold;
		mRollDegreesThreshold = rollDegreesThreshold;

		mYawMaxMovesPerSecond = yawMaxMovesPerSecond;
		mPitchMaxMovesPerSecond = pitchMaxMovesPerSecond;
		mRollMaxMovesPerSecond = rollMaxMovesPerSecond;

		mPoseBuildMode = poseBuildMode;
	}
}

void TAFloaterRealSense::loadEmotionSettings(bool defaults, bool restorePoint)
{
	F32 loEmoThreshold = TARealSense::loadControlValue("RealSenseEmotionLoThreshold", defaults);
	F32 hiEmoThreshold = TARealSense::loadControlValue("RealSenseEmotionHiThreshold", defaults);

	mSliderLoEmoThreshold->setValue(loEmoThreshold);
	mSliderHiEmoThreshold->setValue(hiEmoThreshold);

	S32 angerEvidenceBias = TARealSense::loadControlValue("RealSenseEmotionEvidenceBiasAnger", defaults);
	S32 contemptEvidenceBias = TARealSense::loadControlValue("RealSenseEmotionEvidenceBiasContempt", defaults);
	S32 disgustEvidenceBias = TARealSense::loadControlValue("RealSenseEmotionEvidenceBiasDisgust", defaults);
	S32 fearEvidenceBias = TARealSense::loadControlValue("RealSenseEmotionEvidenceBiasFear", defaults);
	S32 joyEvidenceBias = TARealSense::loadControlValue("RealSenseEmotionEvidenceBiasJoy", defaults);
	S32 sadnessEvidenceBias = TARealSense::loadControlValue("RealSenseEmotionEvidenceBiasSadness", defaults);
	S32 surpriseEvidenceBias = TARealSense::loadControlValue("RealSenseEmotionEvidenceBiasSurprise", defaults);

	mSliderAngerEvidenceBias->setValue(angerEvidenceBias);
	mSliderContemptEvidenceBias->setValue(contemptEvidenceBias);
	mSliderDisgustEvidenceBias->setValue(disgustEvidenceBias);
	mSliderFearEvidenceBias->setValue(fearEvidenceBias);
	mSliderJoyEvidenceBias->setValue(joyEvidenceBias);
	mSliderSadnessEvidenceBias->setValue(sadnessEvidenceBias);
	mSliderSurpriseEvidenceBias->setValue(surpriseEvidenceBias);

	if (restorePoint)
	{
		mLoEmoThreshold = loEmoThreshold;
		mHiEmoThreshold = hiEmoThreshold;

		mAngerEvidenceBias = angerEvidenceBias;
		mContemptEvidenceBias = contemptEvidenceBias;
		mDisgustEvidenceBias = disgustEvidenceBias;
		mFearEvidenceBias = fearEvidenceBias;
		mJoyEvidenceBias = joyEvidenceBias;
		mSadnessEvidenceBias = sadnessEvidenceBias;
		mSurpriseEvidenceBias = surpriseEvidenceBias;
	}
}

void TAFloaterRealSense::refresh()
{
	LLFloater::refresh();

	// Main

	mRealSenseEnabled = gSavedSettings.getBOOL("RealSenseEnabled");

	mVoiceCommandsEnabled = gSavedPerAccountSettings.getBOOL("RealSenseVoiceCommandsEnabled");
	mHandTrackingEnabled = gSavedPerAccountSettings.getBOOL("RealSenseHandTrackingEnabled");
	mPoseTrackingEnabled = gSavedPerAccountSettings.getBOOL("RealSensePoseTrackingEnabled");
	mEmotionTrackingEnabled = gSavedPerAccountSettings.getBOOL("RealSenseEmotionTrackingEnabled");

	mCheckRealSenseEnabled->set(mRealSenseEnabled);
	mCheckVoiceCommandsEnabled->set(mVoiceCommandsEnabled);
	mCheckHandTrackingEnabled->set(mHandTrackingEnabled);
	mCheckPoseTrackingEnabled->set(mPoseTrackingEnabled);
	mCheckEmotionTrackingEnabled->set(mEmotionTrackingEnabled);

	// Tabs

	loadVoiceSettings(false, true);
	loadHandSettings(false, true);
	loadGestureSettings(false, true);
	loadPoseSettings(false, true);
	loadEmotionSettings(false, true);
}

void TAFloaterRealSense::restore()
{
	// Main

	gSavedSettings.setBOOL("RealSenseEnabled", mRealSenseEnabled);

	gSavedPerAccountSettings.setBOOL("RealSenseVoiceCommandsEnabled", mVoiceCommandsEnabled);
	gSavedPerAccountSettings.setBOOL("RealSenseHandTrackingEnabled", mHandTrackingEnabled);
	gSavedPerAccountSettings.setBOOL("RealSensePoseTrackingEnabled", mPoseTrackingEnabled);
	gSavedPerAccountSettings.setBOOL("RealSenseEmotionTrackingEnabled", mEmotionTrackingEnabled);

	// Voice

	gSavedPerAccountSettings.setString("RealSenseVoiceInputDevice", TARealSense::ws2s(mVoiceInputDevice));
	gSavedPerAccountSettings.setU32("RealSenseVoiceInputLevel", mVoiceInputLevel);
	gSavedPerAccountSettings.setU32("RealSenseVoiceCommandThreshold", mVoiceCommandThreshold);

	saveVoiceCommandList(gSavedPerAccountSettings, mCommandActionArgumentVector);

	// Hands

	gSavedPerAccountSettings.setBOOL("RealSenseTouchlessMouseEnabled", mTouchlessMouseEnabled);
	gSavedPerAccountSettings.setF32("RealSenseTouchlessOffsetX", mTouchlessOffsetX);
	gSavedPerAccountSettings.setF32("RealSenseTouchlessOffsetY", mTouchlessOffsetY);
	gSavedPerAccountSettings.setF32("RealSenseHandSpeed", mHandSpeed);
	gSavedPerAccountSettings.setF32("RealSenseHandSmoothing", mHandSmoothing);
	gSavedPerAccountSettings.setF32("RealSenseHandReloadSeconds", mHandReloadSeconds);

	gSavedPerAccountSettings.setS32("RealSenseHandBuildMode", mHandBuildMode);

	// Gestures

	gSavedPerAccountSettings.setString("RealSenseGestureSpreadFingersAction", mSpreadFingersActionString);
	gSavedPerAccountSettings.setString("RealSenseGestureFistAction", mFistActionString);
	gSavedPerAccountSettings.setString("RealSenseGestureTapAction", mTapActionString);
	gSavedPerAccountSettings.setString("RealSenseGestureThumbDownAction", mThumbDownActionString);
	gSavedPerAccountSettings.setString("RealSenseGestureThumbUpAction", mThumbUpActionString);
	gSavedPerAccountSettings.setString("RealSenseGestureClickAction", mClickActionString);
	gSavedPerAccountSettings.setString("RealSenseGestureTwoFingersPinchOpenAction", mTwoFingersPinchOpenActionString);
	gSavedPerAccountSettings.setString("RealSenseGestureVSignAction", mVSignActionString);
	gSavedPerAccountSettings.setString("RealSenseGestureFullPinchAction", mFullPinchActionString);
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeLeftAction", mSwipeLeftActionString);
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeRightAction", mSwipeRightActionString);
	gSavedPerAccountSettings.setString("RealSenseGestureWaveAction", mWaveActionString);
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeDownAction", mSwipeDownActionString);
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeUpAction", mSwipeUpActionString);

	gSavedPerAccountSettings.setString("RealSenseGestureSpreadFingersArgument", mSpreadFingersArgument);
	gSavedPerAccountSettings.setString("RealSenseGestureFistArgument", mFistArgument);
	gSavedPerAccountSettings.setString("RealSenseGestureTapArgument", mTapArgument);
	gSavedPerAccountSettings.setString("RealSenseGestureThumbDownArgument", mThumbDownArgument);
	gSavedPerAccountSettings.setString("RealSenseGestureThumbUpArgument", mThumbUpArgument);
	gSavedPerAccountSettings.setString("RealSenseGestureClickArgument", mClickArgument);
	gSavedPerAccountSettings.setString("RealSenseGestureTwoFingersPinchOpenArgument", mTwoFingersPinchOpenArgument);
	gSavedPerAccountSettings.setString("RealSenseGestureVSignArgument", mVSignArgument);
	gSavedPerAccountSettings.setString("RealSenseGestureFullPinchArgument", mFullPinchArgument);
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeLeftArgument", mSwipeLeftArgument);
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeRightArgument", mSwipeRightArgument);
	gSavedPerAccountSettings.setString("RealSenseGestureWaveArgument", mWaveArgument);
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeDownArgument", mSwipeDownArgument);
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeUpArgument", mSwipeUpArgument);

	// Pose

	gSavedPerAccountSettings.setF32("RealSensePoseYawDegreesOffset", mYawDegreesOffset);
	gSavedPerAccountSettings.setF32("RealSensePosePitchDegreesOffset", mPitchDegreesOffset);
	gSavedPerAccountSettings.setF32("RealSensePoseRollDegreesOffset", mRollDegreesOffset);

	gSavedPerAccountSettings.setF32("RealSensePoseYawDegreesThreshold", mYawDegreesThreshold);
	gSavedPerAccountSettings.setF32("RealSensePosePitchUpDegreesThresholdUp", mPitchUpDegreesThreshold);
	gSavedPerAccountSettings.setF32("RealSensePosePitchDownDegreesThresholdDown", mPitchDownDegreesThreshold);
	gSavedPerAccountSettings.setF32("RealSensePoseRollDegreesThreshold", mRollDegreesThreshold);

	gSavedPerAccountSettings.setF32("RealSensePoseYawMaxMovesPerSecond", mYawMaxMovesPerSecond);
	gSavedPerAccountSettings.setF32("RealSensePosePitchMaxMovesPerSecond", mPitchMaxMovesPerSecond);
	gSavedPerAccountSettings.setF32("RealSensePoseRollMaxMovesPerSecond", mRollMaxMovesPerSecond);

	gSavedPerAccountSettings.setS32("RealSensePoseBuildMode", mPoseBuildMode);

	// Emotions

	gSavedPerAccountSettings.setF32("RealSenseEmotionLoThreshold", mLoEmoThreshold);
	gSavedPerAccountSettings.setF32("RealSenseEmotionHiThreshold", mHiEmoThreshold);

	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasAnger", mAngerEvidenceBias);
	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasContempt", mContemptEvidenceBias);
	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasDisgust", mDisgustEvidenceBias);
	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasFear", mFearEvidenceBias);
	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasJoy", mJoyEvidenceBias);
	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasSadness", mSadnessEvidenceBias);
	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasSurprise", mSurpriseEvidenceBias);

	// Controls

	refresh();
}

void TAFloaterRealSense::onClickVoicePause(void *realsense_panel)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;

		if (self)
		{
			LLButton* pauseButton = self->getChild<LLButton>("Pause button");
			TARealSense* pRS = LLAppViewer::instance()->getRealSensePointer();

			pRS->pauseVoiceCommands(!pRS->voiceCommandsPaused());

			if (pRS->voiceCommandsPaused())
			{
				((TAFloaterRealSense*)realsense_panel)->displayVoiceStatus(TARealSense::STATUS_VOICE_PAUSED);
				pauseButton->setToggleState(true);
			}
			else
			{
				pauseButton->setToggleState(false);
			}
		}
	}
}

void TAFloaterRealSense::onClickAddToList(void *realsense_panel)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;

		if (self)
		{
			std::string command = self->mTextVoiceCommand->getValue();
			std::string action = self->mComboVoiceAction->getValue();
			std::string argument = self->mTextVoiceArgument->getValue();

			boost::algorithm::trim(command);
			boost::algorithm::trim(argument);

			LLScrollListItem::Params row;
			row.value = command;

			LLScrollListCell::Params command_column;
			command_column.column = "column_command";
			command_column.type = "text";
			command_column.value = command;

			LLScrollListCell::Params action_column;
			action_column.column = "column_action";
			action_column.type = "text";
			action_column.value = action;

			LLScrollListCell::Params argument_column;
			argument_column.column = "column_argument";
			argument_column.type = "text";
			argument_column.value = argument;

			row.columns.add(command_column);
			row.columns.add(action_column);
			row.columns.add(argument_column);

			self->mVoiceCommandList->addRow(row);
		}
	}
}

void TAFloaterRealSense::onClickClearCommand(void *realsense_panel)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;

		if (self)
		{
			self->mTextVoiceCommand->setValue(LLStringUtil::null);
			self->mComboVoiceAction->setValue("noaction");
			self->mTextVoiceArgument->setValue(LLStringUtil::null);
		}
	}
}

void TAFloaterRealSense::onClickDropFromList(void *realsense_panel)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;

		if (self)
		{
			self->mVoiceCommandList->deleteSelectedItems();
		}
	}
}

void TAFloaterRealSense::onDoubleClickList(void *realsense_panel)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;

		if (self)
		{
			LLScrollListItem* pRow = self->mVoiceCommandList->getFirstSelected();

			if (pRow)
			{
				self->mTextVoiceCommand->setValue(pRow->getColumn(0)->getValue());
				self->mComboVoiceAction->setValue(pRow->getColumn(1)->getValue());
				self->mTextVoiceArgument->setValue(pRow->getColumn(2)->getValue());
			}
		}
	}
}

void TAFloaterRealSense::onClickVoiceDefaults(void *realsense_panel)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;

		if (self)
		{
			self->loadVoiceSettings(true, false);
		}
	}
}

void TAFloaterRealSense::onClickHandDefaults(void *realsense_panel)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;

		if (self)
		{
			self->loadHandSettings(true, false);
		}
	}
}

void TAFloaterRealSense::onClickGestureDefaults(void *realsense_panel)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;

		if (self)
		{
			self->loadGestureSettings(true, false);
		}
	}
}

void TAFloaterRealSense::onClickPoseDefaults(void *realsense_panel)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;

		if (self)
		{
			self->loadPoseSettings(true, false);
		}
	}
}

void TAFloaterRealSense::onClickEmotionDefaults(void *realsense_panel)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;

		if (self)
		{
			self->loadEmotionSettings(true, false);
		}
	}
}

void TAFloaterRealSense::cleanUpVoiceHandling()
{
	TARealSense* pRS = LLAppViewer::instance()->getRealSensePointer();

	// Remove voice status callbacks

	pRS ->setRSFloaterCallbacks(NULL, NULL, NULL);

	// If voice commands are paused, unpause them

	LLButton* pauseButton = getChild<LLButton>("Pause button");
	if (pauseButton->getToggleState()) pauseButton->toggleState();
	pRS->pauseVoiceCommands(false);
}

void TAFloaterRealSense::saveChanges()
{
	// Main

	gSavedSettings.setBOOL("RealSenseEnabled", 
		mCheckRealSenseEnabled->get());

	gSavedPerAccountSettings.setBOOL("RealSenseVoiceCommandsEnabled", 
		mCheckVoiceCommandsEnabled->get());
	gSavedPerAccountSettings.setBOOL("RealSenseHandTrackingEnabled", 
		mCheckHandTrackingEnabled->get());
	gSavedPerAccountSettings.setBOOL("RealSensePoseTrackingEnabled", 
		mCheckPoseTrackingEnabled->get());
	gSavedPerAccountSettings.setBOOL("RealSenseEmotionTrackingEnabled", 
		mCheckEmotionTrackingEnabled->get());

	// Voice

	std::string selectedValue = mComboVoiceInputDevice->getSelectedValue();
	if (!selectedValue.empty()) // Otherwise, leave latest known value in place
	{
		gSavedPerAccountSettings.setString("RealSenseVoiceInputDevice",
			selectedValue);
	}
	gSavedPerAccountSettings.setU32("RealSenseVoiceInputLevel", 
		mSliderVoiceInputLevel->getValue().asInteger());
	gSavedPerAccountSettings.setU32("RealSenseVoiceCommandThreshold",
		mSliderVoiceCommandThreshold->getValue().asInteger());

	saveVoiceCommandList(gSavedPerAccountSettings, 
				                makeCommandActionArgumentVector());

	// Hands

	gSavedPerAccountSettings.setBOOL("RealSenseTouchlessMouseEnabled", 
		mCheckTouchlessMouseEnabled->get());
	gSavedPerAccountSettings.setF32("RealSenseTouchlessOffsetX", 
		mSliderTouchlessOffsetX->getValue());
	gSavedPerAccountSettings.setF32("RealSenseTouchlessOffsetY", 
		mSliderTouchlessOffsetY->getValue());
	gSavedPerAccountSettings.setF32("RealSenseHandSpeed", 
		mSliderHandSpeed->getValue());
	gSavedPerAccountSettings.setF32("RealSenseHandSmoothing", 
		mSliderHandSmoothing->getValue());
	gSavedPerAccountSettings.setF32("RealSenseHandReloadSeconds", 
		mSliderHandReloadSeconds->getValue());

	gSavedPerAccountSettings.setS32("RealSenseHandBuildMode", 
		mRadioGroupHandBuildMode->getSelectedIndex());

	// Gestures

	gSavedPerAccountSettings.setString("RealSenseGestureSpreadFingersAction", 
		mComboSpreadFingers->getSelectedValue());
	gSavedPerAccountSettings.setString("RealSenseGestureFistAction", 
		mComboFist->getSelectedValue());
	gSavedPerAccountSettings.setString("RealSenseGestureTapAction", 
		mComboTap->getSelectedValue());
	gSavedPerAccountSettings.setString("RealSenseGestureThumbDownAction", 
		mComboThumbDown->getSelectedValue());
	gSavedPerAccountSettings.setString("RealSenseGestureThumbUpAction", 
		mComboThumbUp->getSelectedValue());
	gSavedPerAccountSettings.setString("RealSenseGestureClickAction",
		mComboClick->getSelectedValue());
	gSavedPerAccountSettings.setString("RealSenseGestureTwoFingersPinchOpenAction",
		mComboTwoFingersPinchOpen->getSelectedValue());
	gSavedPerAccountSettings.setString("RealSenseGestureVSignAction",
		mComboVSign->getSelectedValue());
	gSavedPerAccountSettings.setString("RealSenseGestureFullPinchAction", 
		mComboFullPinch->getSelectedValue());
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeLeftAction", 
		mComboSwipeLeft->getSelectedValue());
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeRightAction", 
		mComboSwipeRight->getSelectedValue());
	gSavedPerAccountSettings.setString("RealSenseGestureWaveAction", 
		mComboWave->getSelectedValue());
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeDownAction",
		mComboSwipeDown->getSelectedValue());
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeUpAction",
		mComboSwipeUp->getSelectedValue());

	gSavedPerAccountSettings.setString("RealSenseGestureSpreadFingersArgument", 
		mTextSpreadFingers->getValue());
	gSavedPerAccountSettings.setString("RealSenseGestureFistArgument", 
		mTextFist->getValue());
	gSavedPerAccountSettings.setString("RealSenseGestureTapArgument", 
		mTextTap->getValue());
	gSavedPerAccountSettings.setString("RealSenseGestureThumbDownArgument", 
		mTextThumbDown->getValue());
	gSavedPerAccountSettings.setString("RealSenseGestureThumbUpArgument", 
		mTextThumbUp->getValue());
	gSavedPerAccountSettings.setString("RealSenseGestureClickArgument",
		mTextClick->getValue());
	gSavedPerAccountSettings.setString("RealSenseGestureTwoFingersPinchOpenArgument",
		mTextTwoFingersPinchOpen->getValue());
	gSavedPerAccountSettings.setString("RealSenseGestureVSignArgument", 
		mTextVSign->getValue());
	gSavedPerAccountSettings.setString("RealSenseGestureFullPinchArgument", 
		mTextFullPinch->getValue());
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeLeftArgument", 
		mTextSwipeLeft->getValue());
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeRightArgument", 
		mTextSwipeRight->getValue());
	gSavedPerAccountSettings.setString("RealSenseGestureWaveArgument", 
		mTextWave->getValue());
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeDownArgument",
		mTextSwipeDown->getValue());
	gSavedPerAccountSettings.setString("RealSenseGestureSwipeUpArgument",
		mTextSwipeUp->getValue());
	// Pose

	gSavedPerAccountSettings.setF32("RealSensePoseYawDegreesOffset", 
		mSliderYawDegreesOffset->getValue());
	gSavedPerAccountSettings.setF32("RealSensePosePitchDegreesOffset", 
		mSliderPitchDegreesOffset->getValue());
	gSavedPerAccountSettings.setF32("RealSensePoseRollDegreesOffset", 
		mSliderRollDegreesOffset->getValue());

	gSavedPerAccountSettings.setF32("RealSensePoseYawDegreesThreshold", 
		mSliderYawDegreesThreshold->getValue());
	gSavedPerAccountSettings.setF32("RealSensePosePitchUpDegreesThreshold", 
		mSliderPitchUpDegreesThreshold->getValue());
	gSavedPerAccountSettings.setF32("RealSensePosePitchDownDegreesThreshold", 
		mSliderPitchDownDegreesThreshold->getValue());
	gSavedPerAccountSettings.setF32("RealSensePoseRollDegreesThreshold", 
		mSliderRollDegreesThreshold->getValue());

	gSavedPerAccountSettings.setF32("RealSensePoseYawMaxMovesPerSecond", 
		mSliderYawMaxMovesPerSecond->getValue());
	gSavedPerAccountSettings.setF32("RealSensePosePitchMaxMovesPerSecond", 
		mSliderPitchMaxMovesPerSecond->getValue());
	gSavedPerAccountSettings.setF32("RealSensePoseRollMaxMovesPerSecond", 
		mSliderRollMaxMovesPerSecond->getValue());

	gSavedPerAccountSettings.setS32("RealSensePoseBuildMode", 
		mRadioGroupPoseBuildMode->getSelectedIndex());

	// Emotion

	gSavedPerAccountSettings.setF32("RealSenseEmotionLoThreshold", 
		mSliderLoEmoThreshold->getValue());
	gSavedPerAccountSettings.setF32("RealSenseEmotionHiThreshold", 
		mSliderHiEmoThreshold->getValue());

	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasAnger", 
		mSliderAngerEvidenceBias->getValue());
	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasContempt", 
		mSliderContemptEvidenceBias->getValue());
	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasDisgust", 
		mSliderDisgustEvidenceBias->getValue());
	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasFear", 
		mSliderFearEvidenceBias->getValue());
	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasJoy", 
		mSliderJoyEvidenceBias->getValue());
	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasSadness", 
		mSliderSadnessEvidenceBias->getValue());
	gSavedPerAccountSettings.setS32("RealSenseEmotionEvidenceBiasSurprise", 
		mSliderSurpriseEvidenceBias->getValue());
}

void TAFloaterRealSense::onClickTest(void *realsense_panel)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;

		if (self)
		{
			self->apply();
		}
	}
}

void TAFloaterRealSense::close(bool app)
{
	cleanUpVoiceHandling();

	if (mCloseOK)
	{
		apply();
		saveChanges();

		mCloseOK = false;
	}
	else
	{
		// Undo changes

		restore();
		apply();
	}

	if (gFloaterView) gFloaterView->removeChild(this);

	savePosition();

	LLFloater::close(app);
}

void TAFloaterRealSense::onClickCancel(void *realsense_panel)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;

		if (self)
		{
			self->mCloseOK = false;
			self->close();
		}
	}
}

void TAFloaterRealSense::onClickOK(void *realsense_panel)
{
	if (realsense_panel)
	{
		TAFloaterRealSense* self = (TAFloaterRealSense*)realsense_panel;

		if (self)
		{
			self->mCloseOK = true;
			self->close();
		}
	}
}

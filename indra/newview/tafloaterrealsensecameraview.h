/** 
 * @file tafloaterrealsensecameraview.h
 * @brief RealSense Camera View floater
 *
 * $LicenseInfo:firstyear=2014&license=viewergpl$
 * 
 * Copyright (c) 2014-2015, Tommy Anderberg.
 * 
 * The source code in this file ("Source Code") is provided by Tommy Anderberg
 * to you under the terms of the GNU General Public License, version 2.0
 * ("GPL"), unless you have obtained a separate licensing agreement
 * ("Other License"), formally executed by you and Tommy Anderberg. Terms of
 * the GPL can be found in doc/GPL-license.txt in this distribution.
 * 
 * There are special exceptions to the terms and conditions of the GPL as
 * it is applied to this Source Code. View the full text of the exception
 * in the file doc/FLOSS-exception.txt in this software distribution.
 * 
 * By copying, modifying or distributing this software, you acknowledge
 * that you have read and understood your obligations described above,
 * and agree to abide by those obligations.
 * 
 * ALL TOMMY ANDERBERG SOURCE CODE IS PROVIDED "AS IS." TOMMY ANDERBERG MAKES 
 * NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS ACCURACY, 
 * COMPLETENESS OR PERFORMANCE.
 * $/LicenseInfo$
 */

#ifndef TA_TAFLOATERREALSENSECAMERAVIEW_H
#define TA_TAFLOATERREALSENSECAMERAVIEW_H

#include "llfloater.h"
#include "tarealsense.h"
#include "llviewertexture.h"

#if TA_INCLUDE_REALSENSE
#include "pxcsession.h"
#endif

class TAFloaterRealSenseCameraView : public LLFloater, public LLFloaterSingleton<TAFloaterRealSenseCameraView>
{
public:
	TAFloaterRealSenseCameraView(const LLSD& data);
	virtual ~TAFloaterRealSenseCameraView();

	virtual void draw();
	void close(bool app = false);

	static void onImageData(void* self, void* imageData, void* imageInfo);

private:
	void loadPosition();
	void savePosition();

	void flipXandColors(U32 *srcBuffer, U32 *dstBuffer, S32 height = TARealSense::IMAGE_WIDTH, S32 heightStep = 1, U32 widthStep = 1);

#if TA_INCLUDE_REALSENSE
	void copyImageData(const PXCImage::ImageData* imageData, const PXCImage::ImageInfo* imageInfo);
#endif

	bool mImageDirty;
	U8 mImageBuffer[4 * TARealSense::IMAGE_WIDTH * TARealSense::IMAGE_HEIGHT];

	LLPointer<LLImageRaw> mImageRaw;
	LLPointer<LLViewerTexture> mTexturePtr;
};

#endif // TA_TAFLOATERREALSENSECAMERAVIEW_H


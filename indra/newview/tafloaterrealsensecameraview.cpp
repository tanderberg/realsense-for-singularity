/** 
 * @file tafloaterrealsensecameraview.cpp
 * @brief RealSense Camera View floater
 *
 * $LicenseInfo:firstyear=2014&license=viewergpl$
 * 
 * Copyright (c) 2014-2015, Tommy Anderberg.
 * 
 * The source code in this file ("Source Code") is provided by Tommy Anderberg
 * to you under the terms of the GNU General Public License, version 2.0
 * ("GPL"), unless you have obtained a separate licensing agreement
 * ("Other License"), formally executed by you and Tommy Anderberg. Terms of
 * the GPL can be found in doc/GPL-license.txt in this distribution.
 * 
 * There are special exceptions to the terms and conditions of the GPL as
 * it is applied to this Source Code. View the full text of the exception
 * in the file doc/FLOSS-exception.txt in this software distribution.
 * 
 * By copying, modifying or distributing this software, you acknowledge
 * that you have read and understood your obligations described above,
 * and agree to abide by those obligations.
 * 
 * ALL TOMMY ANDERBERG SOURCE CODE IS PROVIDED "AS IS." TOMMY ANDERBERG MAKES 
 * NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS ACCURACY, 
 * COMPLETENESS OR PERFORMANCE.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "tafloaterrealsensecameraview.h"

#include "lluictrlfactory.h"
#include "llappviewer.h"

TAFloaterRealSenseCameraView::TAFloaterRealSenseCameraView(const LLSD& data)
	: LLFloater("FloaterRealSenseCameraView")
{
	mImageRaw = new LLImageRaw(TARealSense::IMAGE_WIDTH, TARealSense::IMAGE_HEIGHT, 4);

	mImageDirty = false;
	mTexturePtr = NULL;

	LLUICtrlFactory::getInstance()->buildFloater(this, "floater_realsense_camera_view.xml");
	loadPosition();

	gSavedPerAccountSettings.setBOOL("RealSenseCameraViewFloaterShow", true);

	if (gFloaterView) gFloaterView->addChild(this);
}

void TAFloaterRealSenseCameraView::loadPosition()
{
	S32 bottom;
	S32 left;

	LLRect rect = getRect();
	LLRect parentRect = gFloaterView->getRect();

	if (gSavedPerAccountSettings.getBOOL("RealSenseCameraViewFloaterTopCenter"))
	{
		left = max(0, (parentRect.getWidth() - rect.getWidth()) / 2);
		bottom = max(0, parentRect.mTop - rect.getHeight());
	}
	else
	{
		bottom = max(0, gSavedPerAccountSettings.getS32("RealSenseCameraViewFloaterBottom"));
		left = max(0, gSavedPerAccountSettings.getS32("RealSenseCameraViewFloaterLeft"));
	}

	if (left + rect.getWidth() > parentRect.mRight)
	{
		left = max(0, parentRect.mRight - rect.getWidth());
	}

	if (bottom + rect.getHeight() > parentRect.mTop)
	{
		bottom = max(0, parentRect.mTop - rect.getHeight());
	}

	rect.mRight = left + rect.getWidth();
	rect.mTop = bottom + rect.getHeight();
	rect.mLeft = left;
	rect.mBottom = bottom;

	setRect(rect);
}

void TAFloaterRealSenseCameraView::savePosition()
{
	LLRect rect = getRect();

	gSavedPerAccountSettings.setS32("RealSenseCameraViewFloaterBottom", rect.mBottom);
	gSavedPerAccountSettings.setS32("RealSenseCameraViewFloaterLeft", rect.mLeft);
	gSavedPerAccountSettings.setBOOL("RealSenseCameraViewFloaterTopCenter", false);
}

void TAFloaterRealSenseCameraView::flipXandColors(U32 *srcBuffer, U32 *dstBuffer, S32 height, S32 heightStep, U32 widthStep)
// srcBuffer and dstBuffer can point to the same location.
// Input format is ARGB, but A is ignored.
// Output format is ABGR, with A always set to 0xff (i.e. fully opaque).
{
	U32 left_offset_S = 0;
	U32 left_offset_D = 0;

	for (S32 iD = 0; iD < height; iD++)
	{
		U32 right_offset_S = left_offset_S + (TARealSense::IMAGE_WIDTH - 1) * widthStep;
		U32 right_offset_D = left_offset_D + TARealSense::IMAGE_WIDTH - 1;

		for (U32 jD = 0, jS = 0; jD < (TARealSense::IMAGE_WIDTH / 2); jD++, jS += widthStep)
		{
			// Read

			U32 src = srcBuffer[left_offset_S + jS];

			U32 dst = ((src & 0x00ff0000) >> 16) | // Red
				       (src & 0x0000ff00) |        // Green
					  ((src & 0x000000ff) << 16) | // Blue
					          0xff000000;          // Alpha

			src = srcBuffer[right_offset_S - jS];

			// Write

			dstBuffer[right_offset_D - jD] = dst;

			dst = ((src & 0x00ff0000) >> 16) | // Red
			       (src & 0x0000ff00) |        // Green
				  ((src & 0x000000ff) << 16) | // Blue
				          0xff000000;          // Alpha

			dstBuffer[left_offset_D + jD] = dst;
		}

		left_offset_D += TARealSense::IMAGE_WIDTH;
		left_offset_S += TARealSense::IMAGE_WIDTH * widthStep * heightStep;
	}
}

void TAFloaterRealSenseCameraView::draw()
{
	if (mImageDirty)
	{
		mImageRaw->setSubImage(0, 0, TARealSense::IMAGE_WIDTH, TARealSense::IMAGE_HEIGHT, &(mImageBuffer[0]), 0, true);
		mTexturePtr = LLViewerTextureManager::getLocalTexture(mImageRaw.get(), false);
		mImageDirty = false;
	}

	LLFloater::draw();

	LLRect border(5, getRect().getHeight() - LLFLOATER_HEADER_SIZE - 3, 
				  getRect().getWidth() - 3, LLFLOATER_HEADER_SIZE - 12);
	gl_rect_2d(border, LLColor4::black, FALSE);

	if (mTexturePtr.isNull())
	{
		gl_rect_2d_checkerboard(calcScreenRect(), border);
	}
	else
	{
		gl_draw_scaled_image(border.mLeft, border.mBottom, border.getWidth() - 1, border.getHeight() - 1, mTexturePtr);
	}

	TARealSense* pRS = LLAppViewer::instance()->getRealSensePointer();

	if (pRS) pRS->setOnImageData(this, &onImageData);
}

TAFloaterRealSenseCameraView::~TAFloaterRealSenseCameraView()
{
	// Not much to do
}

#if TA_INCLUDE_REALSENSE
void TAFloaterRealSenseCameraView::copyImageData(const PXCImage::ImageData* imageData, const PXCImage::ImageInfo* imageInfo)
{
	if (!imageInfo) return;
	if (!imageData) return;

	S32 heightStep;
	U32 widthStep;

	if (TARealSense::IMAGE_WIDTH == imageInfo->width)
	{
		heightStep = 1;
		widthStep = 1;
	}
	else if (2 * TARealSense::IMAGE_WIDTH == imageInfo->width)
	{
		heightStep = 2;
		widthStep = 2;
	}
	else return;

	if (PXCImage::PIXEL_FORMAT_RGB32 != imageData->format) return;

	S32 height = min(TARealSense::IMAGE_HEIGHT, imageInfo->height);

	if (height < TARealSense::IMAGE_HEIGHT)
	{
		if (height < TARealSense::IMAGE_HEIGHT / 2) return;

		memset(&mImageBuffer, 0, sizeof(mImageBuffer));
	}

	flipXandColors((U32*)(imageData->planes[0]), (U32*)(&(mImageBuffer[0])), height, heightStep, widthStep);

	mImageDirty = true;
}
#endif

void TAFloaterRealSenseCameraView::onImageData(void* self, void* imageData, void* imageInfo)
{
#if TA_INCLUDE_REALSENSE
	if (self) ((TAFloaterRealSenseCameraView*)self)->copyImageData((PXCImage::ImageData*)imageData, (PXCImage::ImageInfo*)imageInfo);
#endif
}

void TAFloaterRealSenseCameraView::close(bool app)
{
	TARealSense* pRS = LLAppViewer::instance()->getRealSensePointer();

	if (pRS) pRS->setOnImageData(NULL, NULL);

	if (gFloaterView) gFloaterView->removeChild(this);

	savePosition();

	if (!app) gSavedPerAccountSettings.setBOOL("RealSenseCameraViewFloaterShow", false);

	LLFloater::close(app);
}

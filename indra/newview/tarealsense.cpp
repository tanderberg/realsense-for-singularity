/** 
 * @file tarealsense.cpp
 * @brief Support for Intel RealSense devices
 *
 * $LicenseInfo:firstyear=2014&license=viewergpl$
 * 
 * Copyright (c) 2014-2015, Tommy Anderberg.
 * 
 * The source code in this file ("Source Code") is provided by Tommy Anderberg
 * to you under the terms of the GNU General Public License, version 2.0
 * ("GPL"), unless you have obtained a separate licensing agreement
 * ("Other License"), formally executed by you and Tommy Anderberg. Terms of
 * the GPL can be found in doc/GPL-license.txt in this distribution.
 * 
 * By copying, modifying or distributing this software, you acknowledge
 * that you have read and understood your obligations described above,
 * and agree to abide by those obligations.
 * 
 * ALL TOMMY ANDERBERG SOURCE CODE IS PROVIDED "AS IS." TOMMY ANDERBERG MAKES 
 * NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS ACCURACY, 
 * COMPLETENESS OR PERFORMANCE.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include "tarealsense.h"
#include "llagent.h"				  // For access to gAgent
#include "llvoavatarself.h"			  // For access to gAgentAvatarp
#include "llappviewer.h"			  // For access to gFrameTimeSeconds (and AGENT_UPDATES_PER_SECOND)
#include "llfloatertools.h"			  // For access to gFloaterTools
#include "llcheckboxctrl.h"			  // For access to LLCheckBoxCtrl
#include "lltoolmgr.h"				  // For access to LLToolMgr

#if TA_INCLUDE_REALSENSE
	#include "llanimationstates.h"    // For RealSense emotion tracking
	#include "pxcfaceconfiguration.h" // For RealSense face tracking
	#include "pxchandconfiguration.h" // For RealSense hand tracking
	#include "llchatbar.h"			  // For RealSense output to chat
	#include "llfloaterchat.h"		  // For diagnostic messages to chat history floater
	#include "llvoiceclient.h"		  // For access to LLVoiceClient
	#include "llmenugl.h"			  // For RealSense menu counting
	#include "lltoolcomp.h"			  // For access to LLToolCompTranslate, LLToolCompRotate, LLToolCompScale
	#include "llpanelobject.h"		  // For access to the object editing controls
	#include "llviewercamera.h"		  // For LLViewerCamera
	#include "llagentcamera.h"		  // For access to gAgentCamera
	#include "lltoolfocus.h"		  // For access gCameraBtnZoom, gCameraBtnOrbit, gCameraBtnPan
	#include "llviewerwindow.h"		  // For access to gViewerWindow
	#include "llwindow.h"			  // For access to LLWindow
	#include "llviewerkeyboard.h"	  // For access to gViewerKeyboard
	#include "lltexteditor.h"		  // For access to LLTextEditor
	#include "lluictrlfactory.h"	  // For access to LLUICtrlFactory
	#include "llflyoutbutton.h"		  // For access to LLFlyoutButton
#endif

void TARealSense::initialize()
{
#if TA_INCLUDE_REALSENSE
	mPXCSession = NULL;

	mPXCSenseManager = NULL;
	mPXCHandConfiguration = NULL;
	mPXCFaceConfiguration = NULL;
	mPXCHandModule = NULL;
	mPXCFaceModule = NULL;
	mPXCHandData = NULL;
	mPXCFaceData = NULL;
	mPXCAudioSource = NULL;
	mPXCSpeechRecognition = NULL;
	mPXCSpeechRecognition_Dictation = NULL;

	mImageDataHandlerObject = NULL;
	mOnImageData = NULL;

	mHandsDirty = false;
	mFaceDirty = false;
	mEmotionsDirty = false;

	cleanGestures();

	mShutdownRealSense = false;
	mShuttingDownRealSense = false;
	mRestartSenseManager = false;
	mStartingSenseManager = false;

	mBuildMode = false;

	mStartScale = 1.0;
	mStartGridMode = GRID_MODE_WORLD;
	mStartRotation.loadIdentity();

	mPoseBuildMode = BUILD_HEAD_TRACKING_ORBIT_CAMERA;
	mHandBuildMode = BUILD_HAND_TRACKING_SPECIAL;

	mHandReloadSeconds = 1.0;

	mHandOpenThreshold = 55;
	mHandClosedThreshold = 45;
	mExtendedFingerThreshold = 55;
	mFoldedMiddleRingPinkyThreshold = 45;

	mHandSpeed = 2.0;
	mHandDistanceNorm = 2.0;
	mHeadDistanceNorm = 0.0001;
	mPanDistanceNorm = 0.1;
	mDeltaMetersToRad = F_PI;
	mCameraOrbitStepDegrees = 2.0;
	mSmoothingFactor = 0.15;
	mMinRSDistance = 0.05;

	mRSDistance = 0.0;

	mRSMassCenterLeft.mV[VX] = 0.0;
	mRSMassCenterLeft.mV[VY] = 0.0;
	mRSMassCenterLeft.mV[VZ] = 0.0;

	mRSMassCenterRight.mV[VX] = 0.0;
	mRSMassCenterRight.mV[VY] = 0.0;
	mRSMassCenterRight.mV[VZ] = 0.0;

	mRSMassCenterDelta.mV[VX] = 0.0;
	mRSMassCenterDelta.mV[VY] = 0.0;
	mRSMassCenterDelta.mV[VZ] = 0.0;
	
	mFaceDepth = 0.0;
	mInitialFaceDepth = 0.0;
	mInitialRSDistance = 0.0;
	mInitialCameraObjectDistance = 0.0;

	mNRSDistanceUpdates = 0;
	mNRSMassCenterLeftUpdates = 0;
	mNRSMassCenterRightUpdates = 0;
	mNInitialRSMassCenterLeftUpdates = 0;
	mNInitialRSMassCenterRightUpdates = 0;
	mNFaceDepthUpdates = 0;
	mNInitialFaceDepthUpdates = 0;
	mNTouchlessCursorUpdates = 0;

	mHandStateLeft = HAND_UNKNOWN;
	mHandStateRight = HAND_UNKNOWN;
	mHandStateLastUpdatedLeft = gFrameTimeSeconds;
	mHandStateLastUpdatedRight = mHandStateLastUpdatedLeft;

	mTouchlessMouseLastLeftClick = gFrameTimeSeconds;
	mTouchlessMouseLastRightClick = mTouchlessMouseLastLeftClick;
	mLastSimulatedKey = gFrameTimeSeconds;
	mLastSimulatedXUIClick = gFrameTimeSeconds;

	mHandBuildState = HANDBUILDSTATE_IDLE;

	mEmotionTrackingEnabled = false;
	mEmotionTrackingPaused = false;

	mPoseTrackingEnabled = false;
	mPoseTrackingPaused = false;

	mTouchlessOffset.x = 0.5;
	mTouchlessOffset.y = 0.5;

	mTouchlessMouseEnabled = true;

	mHandTrackingEnabled = false;
	mHandTrackingPaused = false;

	mDragLeft = false;
	mDragRight = false;

	mVoiceInputDevice = L"";
	mVoiceInputLevel = 50;
	mVoiceCommands = NULL;
	mNVoiceCommands = 0;

	mVoiceCommandThreshold = 50;
	mDictationDelaySeconds = 1.0;

	mGear = 0;
	mGearTurning = 0;
	mGearVertical = 0;

	mnYawMoves = 0.0;
	mnPitchMoves = 0.0;
	mnRollMoves= 0.0;

	mLastPoseTime = gFrameTimeSeconds;
	mLastGearTurning = gFrameTimeSeconds;

	mPoseAngles.yaw = 0.0;
	mPoseAngles.pitch = 0.0;
	mPoseAngles.roll = 0.0;

	mMaxAgains = 10;

	mLastActionTrigger.pCommandActionArgument = NULL;

	mMinGearTurningIntervalSeconds = 0.33;

	restoreEmotionDefaults();
	restorePoseDefaults();
	restoreGestureDefaults();
	restoreHandDefaults();
	restoreVoiceDefaults();

	initVKTable();

	if (!gSavedSettings.getBOOL("RealSenseEnabled"))
	{
		LL_INFOS("InitInfo") << "RealSense disabled" << LL_ENDL;
	}
#endif
}

#if TA_INCLUDE_REALSENSE
pxcF32 TARealSense::lowPass(pxcF32 x, pxcF32 y)
{
	return mSmoothingFactor * x + (1.0 - mSmoothingFactor) * y;
}

void TARealSense::updateRSDistance(pxcF32 rsDistance)
{
	if (mNRSDistanceUpdates)
	{
		mRSDistance = lowPass(rsDistance, mRSDistance);
	}
	else
	{
		mRSDistance = rsDistance;
	}

	mNRSDistanceUpdates++;
}

void TARealSense::updateRSMassCenterLeft(PXCPoint3DF32 rsMassCenter)
{
	if (mNRSMassCenterLeftUpdates)
	{
		mRSMassCenterLeft.mV[VX] = lowPass(rsMassCenter.x, mRSMassCenterLeft.mV[VX]);
		mRSMassCenterLeft.mV[VY] = lowPass(rsMassCenter.y, mRSMassCenterLeft.mV[VY]);
		mRSMassCenterLeft.mV[VZ] = lowPass(rsMassCenter.z, mRSMassCenterLeft.mV[VZ]);
	}
	else
	{
		mRSMassCenterLeft.mV[VX] = rsMassCenter.x;
		mRSMassCenterLeft.mV[VY] = rsMassCenter.y;
		mRSMassCenterLeft.mV[VZ] = rsMassCenter.z;
	}

	mNRSMassCenterLeftUpdates++;
}

void TARealSense::updateRSMassCenterRight(PXCPoint3DF32 rsMassCenter)
{
	if (mNRSMassCenterRightUpdates)
	{
		mRSMassCenterRight.mV[VX] = lowPass(rsMassCenter.x, mRSMassCenterRight.mV[VX]);
		mRSMassCenterRight.mV[VY] = lowPass(rsMassCenter.y, mRSMassCenterRight.mV[VY]);
		mRSMassCenterRight.mV[VZ] = lowPass(rsMassCenter.z, mRSMassCenterRight.mV[VZ]);
	}
	else
	{
		mRSMassCenterRight.mV[VX] = rsMassCenter.x;
		mRSMassCenterRight.mV[VY] = rsMassCenter.y;
		mRSMassCenterRight.mV[VZ] = rsMassCenter.z;
	}

	mNRSMassCenterRightUpdates++;
}

void TARealSense::updateFaceDepth(pxcF32 faceDepth)
{
	if (mNFaceDepthUpdates)
	{
		mFaceDepth = lowPass(faceDepth, mFaceDepth);
	}
	else
	{
		mFaceDepth = faceDepth;
	}

	mNFaceDepthUpdates++;
}

void TARealSense::updateInitialRSMassCenterLeft()
{
	if (mNInitialRSMassCenterLeftUpdates)
	{
		mInitialRSMassCenterLeft.mV[VX] = lowPass(mRSMassCenterLeft.mV[VX], mInitialRSMassCenterLeft.mV[VX]);
		mInitialRSMassCenterLeft.mV[VY] = lowPass(mRSMassCenterLeft.mV[VY], mInitialRSMassCenterLeft.mV[VY]);
		mInitialRSMassCenterLeft.mV[VZ] = lowPass(mRSMassCenterLeft.mV[VZ], mInitialRSMassCenterLeft.mV[VZ]);
	}
	else
	{
		mInitialRSMassCenterLeft = mRSMassCenterLeft;
	}

	mNInitialRSMassCenterLeftUpdates++;
}

void TARealSense::updateInitialRSMassCenterRight()
{
	if (mNInitialRSMassCenterRightUpdates)
	{
		mInitialRSMassCenterRight.mV[VX] = lowPass(mRSMassCenterRight.mV[VX], mInitialRSMassCenterRight.mV[VX]); 
		mInitialRSMassCenterRight.mV[VY] = lowPass(mRSMassCenterRight.mV[VY], mInitialRSMassCenterRight.mV[VY]);
		mInitialRSMassCenterRight.mV[VZ] = lowPass(mRSMassCenterRight.mV[VZ], mInitialRSMassCenterRight.mV[VZ]);
	}
	else
	{
		mInitialRSMassCenterRight = mRSMassCenterRight;
	}

	mNInitialRSMassCenterRightUpdates++;
}

void TARealSense::updateInitialFaceDepth()
{
	if (mNInitialFaceDepthUpdates)
	{
		mInitialFaceDepth = lowPass(mFaceDepth, mInitialFaceDepth); 
	}
	else
	{
		mInitialFaceDepth = mFaceDepth;
	}

	mNInitialFaceDepthUpdates++;
}

void TARealSense::updateInitialCameraObjectDistance()
{
	LLPointer<LLViewerObject> editObject = gFloaterTools->mPanelObject->getObject();
	if ((editObject) && (!editObject.isNull()))
	{
		LLVector3 editObjectPosition = editObject->getPosition();

		LLViewerCamera& cam(LLViewerCamera::instance());

		mInitialCameraObjectDistance = abs(cam.visibleDistance(editObjectPosition, 0.0));
	}
	else
	{
		mInitialCameraObjectDistance = 0.0;
	}
}

void TARealSense::initVKTable()
{
	mVKTable["lbutton"]				= VK_LBUTTON;
	mVKTable["leftbutton"]			= VK_LBUTTON;
	mVKTable["rbutton"]				= VK_RBUTTON;
	mVKTable["rightbutton"]			= VK_RBUTTON;

	mVKTable["cancel"]				= VK_CANCEL;
	mVKTable["back"]				= VK_BACK;
	mVKTable["backspace"]			= VK_BACK;
	mVKTable["tab"]					= VK_TAB;
	mVKTable["clear"]				= VK_OEM_CLEAR;		// VK_CLEAR
	mVKTable["return"]				= VK_RETURN;
	mVKTable["enter"]				= VK_RETURN;

	mVKTable["shift"]				= VK_SHIFT;
	mVKTable["control"]				= VK_CONTROL;
	mVKTable["ctrl"]				= VK_CONTROL;
	mVKTable["ctl"]					= VK_CONTROL;
	mVKTable["alt"]					= VK_MENU;			// Yes, VK_MENU is the Alt key. See http://msdn.microsoft.com/en-us/library/windows/desktop/dd375731(v=vs.85).aspx
	mVKTable["pause"]				= VK_PAUSE;
	mVKTable["capital"]				= VK_CAPITAL;
	mVKTable["caps"]				= VK_CAPITAL;
	mVKTable["capslock"]			= VK_CAPITAL;

	mVKTable["escape"]				= VK_ESCAPE;
	mVKTable["esc"]					= VK_ESCAPE;

	mVKTable["space"]				= VK_SPACE;
	mVKTable["prior"]				= VK_PRIOR;
	mVKTable["pgup"]				= VK_PRIOR;
	mVKTable["pageup"]				= VK_PRIOR;
	mVKTable["pgdn"]				= VK_NEXT;
	mVKTable["pgdown"]				= VK_NEXT;
	mVKTable["pagedown"]			= VK_NEXT;
	mVKTable["end"]					= VK_END;
	mVKTable["home"]				= VK_HOME;
	mVKTable["left"]				= VK_LEFT;
	mVKTable["up"]					= VK_UP;
	mVKTable["right"]				= VK_RIGHT;
	mVKTable["down"]				= VK_DOWN;
	mVKTable["select"]				= VK_SELECT;
	mVKTable["sel"]					= VK_SELECT;
	mVKTable["print"]				= VK_PRINT;
	mVKTable["execute"]				= VK_EXECUTE;
	mVKTable["snapshot"]			= VK_SNAPSHOT;
	mVKTable["prtsc"]				= VK_SNAPSHOT;
	mVKTable["printscreen"]			= VK_SNAPSHOT;
	mVKTable["insert"]				= VK_INSERT;
	mVKTable["ins"]					= VK_INSERT;
	mVKTable["delete"]				= VK_DELETE;
	mVKTable["del"]					= VK_DELETE;
	mVKTable["help"]				= VK_HELP;

	mVKTable["lwin"]				= VK_LWIN;
	mVKTable["leftwin"]				= VK_LWIN;
	mVKTable["rwin"]				= VK_RWIN;
	mVKTable["rightwin"]			= VK_RWIN;
	mVKTable["apps"]				= VK_APPS;

	mVKTable["sleep"]				= VK_SLEEP;

	mVKTable["numpad0"]				= VK_NUMPAD0;
	mVKTable["num0"]				= VK_NUMPAD0;
	mVKTable["numpad1"]				= VK_NUMPAD1;
	mVKTable["num1"]				= VK_NUMPAD1;
	mVKTable["numpad2"]				= VK_NUMPAD2;
	mVKTable["num2"]				= VK_NUMPAD2;
	mVKTable["numpad3"]				= VK_NUMPAD3;
	mVKTable["num3"]				= VK_NUMPAD3;
	mVKTable["numpad4"]				= VK_NUMPAD4;
	mVKTable["num4"]				= VK_NUMPAD4;
	mVKTable["numpad5"]				= VK_NUMPAD5;
	mVKTable["num5"]				= VK_NUMPAD5;
	mVKTable["numpad6"]				= VK_NUMPAD6;
	mVKTable["num6"]				= VK_NUMPAD6;
	mVKTable["numpad7"]				= VK_NUMPAD7;
	mVKTable["num7"]				= VK_NUMPAD7;
	mVKTable["numpad8"]				= VK_NUMPAD8;
	mVKTable["num8"]				= VK_NUMPAD8;
	mVKTable["numpad9"]				= VK_NUMPAD9;
	mVKTable["num9"]				= VK_NUMPAD9;

	mVKTable["multiply"]			= VK_MULTIPLY;
	mVKTable["mul"]					= VK_MULTIPLY;
	mVKTable["*"]					= VK_MULTIPLY;
	mVKTable["add"]					= VK_OEM_PLUS;		// VK_ADD;
	mVKTable["plus"]				= VK_OEM_PLUS;		// VK_ADD;
	mVKTable["+"]					= VK_OEM_PLUS;		// VK_ADD;
	mVKTable["separator"]			= VK_OEM_COMMA;		// VK_SEPARATOR;
	mVKTable["sep"]					= VK_OEM_COMMA;		// VK_SEPARATOR;
	mVKTable["comma"]				= VK_OEM_COMMA;		// VK_SEPARATOR;
	mVKTable[","]					= VK_OEM_COMMA;		// VK_SEPARATOR;
	mVKTable["subtract"]			= VK_OEM_MINUS;		// VK_SUBTRACT;
	mVKTable["sub"]					= VK_OEM_MINUS;		// VK_SUBTRACT;
	mVKTable["minus"]				= VK_OEM_MINUS;		// VK_SUBTRACT;
	mVKTable["-"]					= VK_OEM_MINUS;		// VK_SUBTRACT;
	mVKTable["decimal"]				= VK_OEM_PERIOD;	// VK_DECIMAL;
	mVKTable["period"]				= VK_OEM_PERIOD;	// VK_DECIMAL;
	mVKTable["dot"]					= VK_OEM_PERIOD;	// VK_DECIMAL;
	mVKTable["."]					= VK_OEM_PERIOD;	// VK_DECIMAL;
	mVKTable["divide"]				= VK_DIVIDE;
	mVKTable["div"]					= VK_DIVIDE;
	mVKTable["/"]					= VK_DIVIDE;

	mVKTable["f1"]					= VK_F1;
	mVKTable["f2"]					= VK_F2;
	mVKTable["f3"]					= VK_F3;
	mVKTable["f4"]					= VK_F4;
	mVKTable["f5"]					= VK_F5;
	mVKTable["f6"]					= VK_F6;
	mVKTable["f7"]					= VK_F7;
	mVKTable["f8"]					= VK_F8;
	mVKTable["f9"]					= VK_F9;
	mVKTable["f10"]					= VK_F10;
	mVKTable["f11"]					= VK_F11;
	mVKTable["f12"]					= VK_F12;
	mVKTable["f13"]					= VK_F13;
	mVKTable["f14"]					= VK_F14;
	mVKTable["f15"]					= VK_F15;
	mVKTable["f16"]					= VK_F16;
	mVKTable["f17"]					= VK_F17;
	mVKTable["f18"]					= VK_F18;
	mVKTable["f19"]					= VK_F19;
	mVKTable["f20"]					= VK_F20;
	mVKTable["f21"]					= VK_F21;
	mVKTable["f22"]					= VK_F22;
	mVKTable["f23"]					= VK_F23;
	mVKTable["f24"]					= VK_F24;

	mVKTable["numlock"]				= VK_NUMLOCK;
	mVKTable["scroll"]				= VK_SCROLL;

	mVKTable["lshift"]				= VK_LSHIFT;
	mVKTable["leftshift"]			= VK_LSHIFT;
	mVKTable["rshift"]				= VK_RSHIFT;
	mVKTable["rightshift"]			= VK_RSHIFT;
	mVKTable["lcontrol"]			= VK_LCONTROL;
	mVKTable["leftcontrol"]			= VK_LCONTROL;
	mVKTable["lctrl"]				= VK_LCONTROL;
	mVKTable["lctl"]				= VK_LCONTROL;
	mVKTable["rcontrol"]			= VK_RCONTROL;
	mVKTable["rightcontrol"]		= VK_RCONTROL;
	mVKTable["rctrl"]				= VK_RCONTROL;
	mVKTable["rctl"]				= VK_RCONTROL;
	mVKTable["lmenu"]				= VK_LMENU;
	mVKTable["leftmenu"]			= VK_LMENU;
	mVKTable["rmenu"]				= VK_RMENU;
	mVKTable["rightmenu"]			= VK_RMENU;

	mVKTable["browserback"]			= VK_BROWSER_BACK;
	mVKTable["bback"]				= VK_BROWSER_BACK;
	mVKTable["browserforward"]		= VK_BROWSER_FORWARD;
	mVKTable["bforward"]			= VK_BROWSER_FORWARD;
	mVKTable["bfwd"]				= VK_BROWSER_FORWARD;
	mVKTable["browserrefresh"]		= VK_BROWSER_REFRESH;
	mVKTable["browserref"]			= VK_BROWSER_REFRESH;
	mVKTable["brefrefresh"]			= VK_BROWSER_REFRESH;
	mVKTable["bref"]				= VK_BROWSER_REFRESH;
	mVKTable["browserstop"]			= VK_BROWSER_STOP;
	mVKTable["bstop"]				= VK_BROWSER_STOP;
	mVKTable["browsersearch"]		= VK_BROWSER_SEARCH;
	mVKTable["bsearch"]				= VK_BROWSER_SEARCH;
	mVKTable["browserfavorites"]	= VK_BROWSER_FAVORITES;
	mVKTable["browserfavs"]			= VK_BROWSER_FAVORITES;
	mVKTable["bfavs"]				= VK_BROWSER_FAVORITES;
	mVKTable["browserhome"]			= VK_BROWSER_HOME;
	mVKTable["bhome"]				= VK_BROWSER_HOME;

	mVKTable["vmute"]				= VK_VOLUME_MUTE;
	mVKTable["volmute"]				= VK_VOLUME_MUTE;
	mVKTable["volumemute"]			= VK_VOLUME_MUTE;
	mVKTable["vdown"]				= VK_VOLUME_DOWN;
	mVKTable["voldown"]				= VK_VOLUME_DOWN;
	mVKTable["volumedown"]			= VK_VOLUME_DOWN;
	mVKTable["vup"]					= VK_VOLUME_UP;
	mVKTable["volup"]				= VK_VOLUME_UP;
	mVKTable["volumeup"]			= VK_VOLUME_UP;
	mVKTable["medianext"]			= VK_MEDIA_NEXT_TRACK;
	mVKTable["mnext"]				= VK_MEDIA_NEXT_TRACK;
	mVKTable["mediaprev"]			= VK_MEDIA_PREV_TRACK;
	mVKTable["mprev"]				= VK_MEDIA_PREV_TRACK;
	mVKTable["mediastop"]			= VK_MEDIA_STOP;
	mVKTable["mstop"]				= VK_MEDIA_STOP;
	mVKTable["mediaplaypause"]		= VK_MEDIA_PLAY_PAUSE;
	mVKTable["mediaplay"]			= VK_MEDIA_PLAY_PAUSE;
	mVKTable["mediapause"]			= VK_MEDIA_PLAY_PAUSE;
	mVKTable["mpause"]				= VK_MEDIA_PLAY_PAUSE;
	mVKTable["mplay"]				= VK_MEDIA_PLAY_PAUSE;
	mVKTable["launchmail"]			= VK_LAUNCH_MAIL;
	mVKTable["lmail"]				= VK_LAUNCH_MAIL;
	mVKTable["launchmediaselect"]	= VK_LAUNCH_MEDIA_SELECT;
	mVKTable["lmediaselect"]		= VK_LAUNCH_MEDIA_SELECT;
	mVKTable["lmselect"]			= VK_LAUNCH_MEDIA_SELECT;
	mVKTable["lmsel"]				= VK_LAUNCH_MEDIA_SELECT;
	mVKTable["launchapp1"]			= VK_LAUNCH_APP1;
	mVKTable["lapp1"]				= VK_LAUNCH_APP1;
	mVKTable["launchapp2"]			= VK_LAUNCH_APP2;
	mVKTable["lapp2"]				= VK_LAUNCH_APP2;

	for (char c = '0'; c <= '9'; c++)
	{
		std::string s;
		s.push_back(c);
		mVKTable[s] = c;
	}
	
	for (char c = 'A'; c <= 'Z'; c++)
	{
		std::string s;
		s.push_back(tolower(c));
		mVKTable[s] = c;
	}
}

void TARealSense::clearFocusedControl()
{
	LLUICtrl* p = dynamic_cast<LLUICtrl*>(gFocusMgr.getKeyboardFocus()); 
	if (p)
	{
		p->clear();
		simulateKey(VK_DELETE);
	}
}

void TARealSense::setFocusedControlText(const pxcCHAR* pText)
{
	LLUICtrl* p = dynamic_cast<LLUICtrl*>(gFocusMgr.getKeyboardFocus());

	if ((p) && (p->acceptsTextInput()))
	{
		std::string s = ws2s(pText);

		LLLineEditor* pl = dynamic_cast<LLLineEditor*>(p);

		if (pl)
		{
			// Insert new text at cursor position

			pl->insert(s, pl->getCursor());
			return;
		}

		LLTextEditor* pe = dynamic_cast<LLTextEditor*>(p);

		if (pe)
		{
			// Insert new text at cursor position, overwriting extended selection

			pe->insertText(s, true); 
			return;
		}

		// Default behavior: just replace text

		p->setValue(s);

		// Move cursor to end of text
		simulateKey(VK_END);
	}
}

void TARealSense::takeDictation()
{
	if (!voiceCommandsActive())
	{
		make_ui_sound("UISndInvalidOp");
		return;
	}

	LLUICtrl* p = dynamic_cast<LLUICtrl*>(gFocusMgr.getKeyboardFocus());

	if ((p) && (p->acceptsTextInput()))
	{
		mPXCSpeechRecognition->StopRec();

		pxcStatus result = mPXCSpeechRecognition_Dictation->StartRec(mPXCAudioSource, &mPXCSpeechRecognitionHandler);

		if (PXC_STATUS_NO_ERROR == result)
		{
			mPXCSpeechRecognitionHandler.setExpectedDictationSentences(1, gFrameTimeSeconds + mDictationDelaySeconds);
			make_ui_sound("UISndAlert");
		}
		else
		{
			make_ui_sound("UISndInvalidOp");

			llinfos << "Error starting RealSense audio device for dictation. Error code: " << result << llendl;
		}
	}
	else
	{
		make_ui_sound("UISndInvalidOp");
	}
}

WORD TARealSense::string2keycode(std::string s)
{
	if ((s.length() == 4) && ('0' == s[0]) && ('x' == s[1]))
	{
		// Try interpreting as hex-encoded integer (0x??)
		try
		{
			// return std::stoul(s, nullptr, 16); // C++11 version

			WORD w;
			std::stringstream ss;
			ss << std::hex << s.substr(2);
			ss >> w;

			return w;
		}
		catch(...)
		{
			// Just fall through to table lookup
		}
	}

	auto search = mVKTable.find(s);
	if (search != mVKTable.end()) return search->second;

	return 0;
}

TARealSense::TKeyStroke TARealSense::string2keystroke(std::string s)
{
	TKeyStroke ks;

	boost::algorithm::to_lower(s);
	boost::escaped_list_separator<char> els('\\', '-', '\"');
	boost::tokenizer<boost::escaped_list_separator<char>> tok(s, els);

	for (boost::tokenizer<boost::escaped_list_separator<char>>::iterator t = tok.begin(); t != tok.end(); ++t)
	{
		WORD keycode = string2keycode(*t);
		if (keycode) ks.push_back(keycode);
	}

	return ks;
}

TARealSense::TKeyStrokeVector TARealSense::string2keystrokeVector(std::string s)
{
	TKeyStrokeVector ksv;

	boost::escaped_list_separator<char> els('\\', ' ', '\"');
	boost::tokenizer<boost::escaped_list_separator<char>> tok(s, els);

	for (boost::tokenizer<boost::escaped_list_separator<char>>::iterator t = tok.begin(); t != tok.end(); ++t)
	{
		TKeyStroke ks = string2keystroke(*t);
		if (0 < ks.size()) ksv.push_back(ks);
	}

	return ksv;
}

void TARealSense::simulateMouseDown(bool right)
{
	INPUT input = {0};
	input.type = INPUT_MOUSE;
	input.mi.dwFlags = right ? MOUSEEVENTF_RIGHTDOWN : MOUSEEVENTF_LEFTDOWN;
	SendInput(1, &input, sizeof(INPUT));

	if (right) mDragRight = true; else mDragLeft = true;
	mLatestDragPoint = mTouchlessCursor;
}

void TARealSense::simulateMouseUp(bool right)
{
	INPUT input = {0};
	input.type = INPUT_MOUSE;
	input.mi.dwFlags = right ? MOUSEEVENTF_RIGHTUP : MOUSEEVENTF_LEFTUP;
	SendInput(1, &input, sizeof(INPUT));

	if (right) mDragRight = false; else mDragLeft = false;
}

void TARealSense::simulateMouseMoveTo(POINT wp)
{
	const HWND hwnd = GetDesktopWindow();
	RECT desktop;
	GetWindowRect(hwnd, &desktop);

	INPUT input = {0};
	input.type = INPUT_MOUSE;
	input.mi.dx = min(65535, max(0, 65535 * wp.x / desktop.right));
	input.mi.dy = min(65535, max(0, 65535 * wp.y / desktop.bottom));
	input.mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE;

	SendInput(1, &input, sizeof(INPUT));
}

void TARealSense::simulateMouseMoveBy(POINT delta)
{
	INPUT input = {0};
	input.type = INPUT_MOUSE;
	input.mi.dx = delta.x;
	input.mi.dy = delta.y;
	input.mi.dwFlags = MOUSEEVENTF_MOVE;

	SendInput(1, &input, sizeof(INPUT));
}

void TARealSense::simulateMouseClick(bool right)
{
	simulateMouseDown(right);
	simulateMouseUp(right);
}

void TARealSense::simulateKey(WORD key)
{
	INPUT input = {0};
	input.type = INPUT_KEYBOARD;
	input.ki.wVk = key;
	SendInput(1, &input, sizeof(INPUT));

	memset(&input, 0, sizeof(input));
	input.type = INPUT_KEYBOARD;
	input.ki.wVk = key;
	input.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &input, sizeof(INPUT));
}

void TARealSense::simulateKeyStroke(TKeyStroke keyStroke)
{
	int nKeys = keyStroke.size();

	if (1 > nKeys) return;

	for (int i = 0; i < nKeys; i++)
	{
		INPUT input = {0};
		input.type = INPUT_KEYBOARD;
		input.ki.wVk = keyStroke[i];
		SendInput(1, &input, sizeof(INPUT));
	}

	// Release keys in reverse order, so modifier-key combinations are 
	// fired with all modifiers down.

	for (int i = nKeys - 1; -1 < i; i--)
	{
		INPUT input = {0};
		input.type = INPUT_KEYBOARD;
		input.ki.wVk = keyStroke[i];
		input.ki.dwFlags = KEYEVENTF_KEYUP;
		SendInput(1, &input, sizeof(INPUT));
	}
}

void TARealSense::simulateKeyStrokeSequence(TKeyStrokeVector keyStrokeVector)
{
	int nKeyStrokes = keyStrokeVector.size();

	if (1 > nKeyStrokes) return;

	for (int i = 0; i < nKeyStrokes; i++)
	{
		simulateKeyStroke(keyStrokeVector[i]);
	}
}

LLView* TARealSense::findView(std::string xuiPath)
{
	LLView* view = gViewerWindow->getRootView();

	if (view)
	{
		boost::char_separator<char> sep("/");
		boost::tokenizer<boost::char_separator<char>> tok(xuiPath, sep);

		for (boost::tokenizer<boost::char_separator<char>>::iterator t = tok.begin(); t != tok.end(); ++t)
		{
			view = view->getChildView(*t, FALSE, FALSE);
			if (!view) break;
			if ((!view->isInVisibleChain()) || (!view->isInEnabledChain()))
			{
				view = NULL;
				break;
			}
		}
	}

	return view;
}

TCommandActionArgument* TARealSense::xuiClick(const TCommandActionArgument* pCommandActionArgument)
{
	LLView* view = findView(pCommandActionArgument->argument);

	if (!view)
	{
		// The requested view is not available. Look for other matches to the same command.

		const TCommandActionArgument* prevP = pCommandActionArgument;
		TCommandActionArgument* p = mPXCSpeechRecognitionHandler.getCommandActionArgumentPtr(prevP->nextCandidate);

		while ((p) && (p != prevP))
		{
			if (ACTION_XUICLICK == p->action)
			{
				view = findView(p->argument);
				if (view) break;
			}
			else
			{
				// No other XUI click commands left to try, return pointer to first alternative command.

				return p;
			}

			prevP = p;
			p = mPXCSpeechRecognitionHandler.getCommandActionArgumentPtr(prevP->nextCandidate);
		}
	}

	if (!view) return NULL;

	LLRect rect = view->calcScreenRect();

	LLRect windowRect = gViewerWindow->getWindowRect();
	// S32 w = (&windowRect)->getWidth();
	S32 h = (&windowRect)->getHeight();

	POINT wp;

	wp.x = rect.getCenterX();
	wp.y = h - rect.getCenterY();

	// Flyout buttons are special, require clicking near the right edge of the view
	if (dynamic_cast<LLFlyoutButton*>(view)) wp.x += rect.getWidth() / 2 - 1;

	HWND hwnd = (HWND)gViewerWindow->getPlatformWindow();

	ClientToScreen(hwnd, &wp);

	POINT wpprev;
	bool restore = GetCursorPos(&wpprev);

	SetCursorPos(wp.x, wp.y);
	simulateMouseClick(false);

	if (restore) SetCursorPos(wpprev.x, wpprev.y);

	return NULL;
}

bool TARealSense::editingObject()
{
	return ((mBuildMode) && (gFloaterTools->mPanelObject->getObject()) && (!gFloaterTools->mPanelObject->getObject().isNull()));
}

void TARealSense::triggerActionCore(const TCommandActionArgument* pCommandActionArgument, bool touchlessMouseOnly, bool enforceHandReload)
{
	// Always check if it's time to end continued touchless mouse actions

	if ((mDragLeft) && ((ACTION_LEFTDRAG != pCommandActionArgument->action) || editingObject())) simulateMouseUp(false);
	if ((mDragRight) && ((ACTION_RIGHTDRAG != pCommandActionArgument->action) || editingObject())) simulateMouseUp(true);

	if (!touchlessMouseOnly)
	{
		// Non-touchlessMouse actions

		switch (pCommandActionArgument->action)
		{
			case ACTION_STEP:			if (gAgentAvatarp->isSitting() || (0 < mGear)) return;
										mGear = 0; gAgent.clearTempRun(); gAgent.moveAt(1); return;
			case ACTION_STEPBACK:		if (gAgentAvatarp->isSitting() || (0 > mGear)) return;
										mGear = 0; gAgent.clearTempRun(); gAgent.moveAt(-1); return;
			case ACTION_FORWARD:		if (gAgentAvatarp->isSitting() || (0 < mGear)) return;
										mGear = 1; gAgent.clearTempRun(); return;
			case ACTION_BACKWARD:		if (gAgentAvatarp->isSitting() || (0 > mGear)) return;
										mGear = -1; gAgent.clearTempRun(); return;
			case ACTION_RUN:			if (gAgentAvatarp->isSitting()) return;
										mGear = 2; gAgent.setTempRun(); return;
			case ACTION_RUNBACK:		if (gAgentAvatarp->isSitting()) return;
										mGear = -2; gAgent.setTempRun(); return;
			case ACTION_STOPHORIZONTAL:	mGear = 0; gAgent.clearTempRun(); return;
			case ACTION_LEFT:			gAgent.moveYaw(1); return;
			case ACTION_RIGHT:			gAgent.moveYaw(-1); return;
			case ACTION_TURNLEFT:		if (0 >= mGearTurning) mGearTurning = 1; return;
			case ACTION_TURNRIGHT:		if (0 <= mGearTurning) mGearTurning = -1; return;
			case ACTION_STOPTURNING:	mGearTurning = 0; return;
			case ACTION_SLIDELEFT:		if (!gAgentAvatarp->isSitting()) gAgent.moveLeft(1); return;
			case ACTION_SLIDERIGHT:		if (!gAgentAvatarp->isSitting()) gAgent.moveLeft(-1); return;
			case ACTION_UP:				if (!gAgentAvatarp->isSitting()) gAgent.moveUp(1); return;
			case ACTION_DOWN:			if (!gAgentAvatarp->isSitting()) gAgent.moveUp(-1); return;
			case ACTION_CLIMB:			if (gAgentAvatarp->isSitting() || (0 < mGearVertical)) return;
										gAgent.setFlying(true); mGearVertical = 1; return;
			case ACTION_DESCEND:		if (gAgentAvatarp->isSitting() || (0 > mGearVertical)) return;
										gAgent.setFlying(true); mGearVertical = -1; return;
			case ACTION_STOPVERTICAL:	mGearVertical = 0; return;
			case ACTION_FLY:			gAgent.setFlying(!gAgentAvatarp->isSitting()); return;
			case ACTION_STOPFLYING:		gAgent.setFlying(false); return;
			case ACTION_STOP:			mGear = 0; mGearTurning = 0; mGearVertical = 0; gAgent.clearTempRun(); return;
			case ACTION_SLOW:			if (0 > mGear) mGear++; else if (0 < mGear) mGear--; 
										gAgent.clearTempRun();
										if (0 > mGearTurning) mGearTurning++; else if (0 < mGearTurning) mGearTurning--; 
										if (0 > mGearVertical) mGearVertical++; else if (0 < mGearVertical) mGearVertical--; 
										return;
			case ACTION_FAST:			if (-1 == mGear) mGear = -2; else if (1 == mGear) mGear = 2;
										if (-1 == mGearTurning) mGearTurning = -2; else if (1 == mGearTurning) mGearTurning = 2;
										if (-1 == mGearVertical) mGearVertical = -2; else if (1 == mGearVertical) mGearVertical = 2;
										if (0 != mGear) gAgent.setTempRun();
										return;
			case ACTION_WHISPER:		gChatBar->sendChatFromViewer(pCommandActionArgument->argument, CHAT_TYPE_WHISPER, false); return;
			case ACTION_SAY: 			gChatBar->sendChatFromViewer(pCommandActionArgument->argument, CHAT_TYPE_NORMAL, false); return;
			case ACTION_SHOUT:			gChatBar->sendChatFromViewer(pCommandActionArgument->argument, CHAT_TYPE_SHOUT, false); return;
			case ACTION_REGIONSAY:		gChatBar->sendChatFromViewer(pCommandActionArgument->argument, CHAT_TYPE_REGION, false); return;
			case ACTION_TALKON:			LLVoiceClient::getInstance()->setUserPTTState(true); return;
			case ACTION_TALKOFF:		LLVoiceClient::getInstance()->setUserPTTState(false); return;
			case ACTION_DICTATE:		takeDictation(); return;
			case ACTION_CLEAR:			clearFocusedControl(); return;
			case ACTION_KEYSTROKE:		if (enforceHandReload)
										{
											if (mHandReloadSeconds >= (gFrameTimeSeconds - mLastSimulatedKey)) return;
											mLastSimulatedKey = gFrameTimeSeconds;
										}
										simulateKeyStrokeSequence(string2keystrokeVector(pCommandActionArgument->argument));
										return;
			case ACTION_XUICLICK:		if (enforceHandReload)
										{
											if (mHandReloadSeconds >= (gFrameTimeSeconds - mLastSimulatedXUIClick)) return;
											mLastSimulatedXUIClick = gFrameTimeSeconds;
										}
										TCommandActionArgument* p = xuiClick(pCommandActionArgument);
										if (p) triggerActionCore(p, touchlessMouseOnly, enforceHandReload);
										return;
		}
	}

	switch (pCommandActionArgument->action)
	{
		case ACTION_LEFTCLICK:	if (enforceHandReload)
								{
									if (mHandReloadSeconds >= (gFrameTimeSeconds - mTouchlessMouseLastLeftClick)) return;
									mTouchlessMouseLastLeftClick = gFrameTimeSeconds;
								}
								simulateMouseClick(false);
								return;
		case ACTION_RIGHTCLICK: if (enforceHandReload)
								{
									if (mHandReloadSeconds >= (gFrameTimeSeconds - mTouchlessMouseLastRightClick)) return;
									mTouchlessMouseLastRightClick = gFrameTimeSeconds;
								}
								simulateMouseClick(true);
								return;
		case ACTION_LEFTDRAG:	if ((!mTouchlessMouseEnabled) || mDragLeft || editingObject()) return;
								if (enforceHandReload)
								{
									if (mHandReloadSeconds >= (gFrameTimeSeconds - mTouchlessMouseLastLeftClick)) return;
									mTouchlessMouseLastLeftClick = gFrameTimeSeconds;
								}
								simulateMouseDown(false);
								return;
		case ACTION_RIGHTDRAG:	if ((!mTouchlessMouseEnabled) || mDragRight || editingObject()) return;
								if (enforceHandReload)
								{
									if (mHandReloadSeconds >= (gFrameTimeSeconds - mTouchlessMouseLastRightClick)) return;
									mTouchlessMouseLastRightClick = gFrameTimeSeconds;
								}
								simulateMouseDown(true);
								return;
	}
}

void TARealSense::triggerAction(const TCommandActionArgument* pCommandActionArgument, bool touchlessMouseOnly, bool enforceHandReload)
{
	U32 nTimes = 1;

	if (ACTION_AGAIN == pCommandActionArgument->action)
	{
		if (!mLastActionTrigger.pCommandActionArgument) return;

		// The argument can be a number of repetitions

		try
		{
			// nTimes = min(max(1, std::stoul(pCommandActionArgument->argument)), mMaxAgains); // C++11 version
			nTimes = min(max(1, boost::lexical_cast<U32>(pCommandActionArgument->argument)), mMaxAgains);
		}
		catch (...)
		{
			nTimes = 1;
		}

		pCommandActionArgument = mLastActionTrigger.pCommandActionArgument;
		touchlessMouseOnly = mLastActionTrigger.touchlessMouseOnly;
		enforceHandReload = mLastActionTrigger.enforceHandReload;
	}
	else
	{
		mLastActionTrigger.pCommandActionArgument = pCommandActionArgument;
		mLastActionTrigger.touchlessMouseOnly = touchlessMouseOnly;
		mLastActionTrigger.enforceHandReload = enforceHandReload;
	}

	for (U32 i = 0; i < nTimes; i++)
	{
		triggerActionCore(pCommandActionArgument, touchlessMouseOnly, enforceHandReload);
	}
}

/* A few debug functions which may still become useful again

void TARealSense::say(std::string s, pxcF32 value)
{
	// For debugging purposes

	std::ostringstream ss;
	ss << s << " " << value;
	std::string sss(ss.str());

	LLFloaterChat::getInstance()->addChatHistory(sss);
}

void TARealSense::sayVector(std::string s, LLVector3 v)
{
	// For debugging purposes

	std::ostringstream ss;
	ss << s << v.mV[VX] << "," << v.mV[VY] << ", " << v.mV[VZ];
	std::string sss(ss.str());

	LLFloaterChat::getInstance()->addChatHistory(sss);
}

void TARealSense::sayQuaternion(std::string s, LLQuaternion q)
{
	// For debugging purposes

	F32 roll;
	F32 pitch;
	F32 yaw;

	q.getEulerAngles(&roll, &pitch, &yaw);

	std::ostringstream ss;
	ss << s << roll << "," << pitch << ", " << yaw;
	std::string sss(ss.str());

	LLFloaterChat::getInstance()->addChatHistory(sss);
}

*/

S32 TARealSense::handState(PXCHandData::IHand* handData)
{
	pxcI32 foldedness = 0;
	int thumb_foldedness = -1;
	int index_foldedness = -1;
	int middle_ring_pinky_foldedness = 0;
	int nFingers = 0;

	PXCHandData::FingerData fingerData;

	if (handData->QueryFingerData(PXCHandData::FINGER_THUMB, fingerData) >= PXC_STATUS_NO_ERROR)
	{
		foldedness += fingerData.foldedness;
		thumb_foldedness = fingerData.foldedness;
	}

	if (handData->QueryFingerData(PXCHandData::FINGER_INDEX, fingerData) >= PXC_STATUS_NO_ERROR)
	{
		foldedness += fingerData.foldedness;
		index_foldedness = fingerData.foldedness;
	}

	if (handData->QueryFingerData(PXCHandData::FINGER_MIDDLE, fingerData) >= PXC_STATUS_NO_ERROR)
	{
		nFingers++;
		foldedness += fingerData.foldedness;
		middle_ring_pinky_foldedness += fingerData.foldedness;
	}

	if (handData->QueryFingerData(PXCHandData::FINGER_RING, fingerData) >= PXC_STATUS_NO_ERROR)
	{
		nFingers++;
		foldedness += fingerData.foldedness;
		middle_ring_pinky_foldedness += fingerData.foldedness;
	}

	if (handData->QueryFingerData(PXCHandData::FINGER_PINKY, fingerData) >= PXC_STATUS_NO_ERROR)
	{
		nFingers++;
		foldedness += fingerData.foldedness;
		middle_ring_pinky_foldedness += fingerData.foldedness;
	}

	if (nFingers < 2) return HAND_UNKNOWN;

	if (mFoldedMiddleRingPinkyThreshold > (middle_ring_pinky_foldedness / nFingers))
	{
		// Three last fingers are folded

		if (mExtendedFingerThreshold < index_foldedness)
		{
			// Index is extended

			if (mExtendedFingerThreshold < thumb_foldedness)
			{
				// Thumb is extended

				return HAND_GUN;
			}

			return HAND_POINTING_FINGER;
		}
	}

	if (-1 < index_foldedness) nFingers++;
	if (-1 < thumb_foldedness) nFingers++;

	foldedness /= nFingers;

	if (foldedness < mHandClosedThreshold) return HAND_CLOSED;
	if (foldedness > mHandOpenThreshold) return HAND_OPEN;
	return HAND_RELAXED;
}

void TARealSense::updateTouchlessCursor(PXCPointF32 p)
{
	if (mNTouchlessCursorUpdates)
	{
		mTouchlessCursor.x = lowPass(p.x, mTouchlessCursor.x);
		mTouchlessCursor.y = lowPass(p.y, mTouchlessCursor.y);
	}
	else
	{
		LLRect windowRect = gViewerWindow->getWindowRect();
		S32 w = (&windowRect)->getWidth();
		S32 h = (&windowRect)->getHeight();

		LLCoordGL pt = gViewerWindow->getCurrentMouse();

		mTouchlessCursor.x = (0.5 * IMAGE_WIDTH) - ((1.0 * pt.mX) / w - mTouchlessOffset.x) * IMAGE_WIDTH / mHandSpeed;
		mTouchlessCursor.y = (0.5 * IMAGE_HEIGHT) - ((1.0 * pt.mY) / h - mTouchlessOffset.y) * IMAGE_HEIGHT / mHandSpeed;
	}

	mNTouchlessCursorUpdates++;
}

void TARealSense::moveTouchlessCursor()
{
	if ((mDragLeft || mDragRight) && (gViewerWindow->getCursorHidden()))
	{
		// Dragging inside main view -> move camera

		PXCPointF32 delta = mTouchlessCursor;
		delta.x -= mLatestDragPoint.x;
		delta.y -= mLatestDragPoint.y;

		mLatestDragPoint = mTouchlessCursor;

		if (gCameraBtnZoom)
		{
			const F32 RADIANS_PER_PIXEL = 20.0 * mHandSpeed / gViewerWindow->getWorldViewWidthScaled();
			gAgentCamera.cameraOrbitAround(delta.x * RADIANS_PER_PIXEL);
			gAgentCamera.cameraZoomIn(pow(0.99f, delta.y));
		}
		else if (gCameraBtnOrbit)
		{
			const F32 RADIANS_PER_PIXEL = 20.0 * mHandSpeed / gViewerWindow->getWorldViewWidthScaled();
			gAgentCamera.cameraOrbitAround(delta.x * RADIANS_PER_PIXEL);
			gAgentCamera.cameraOrbitOver(-delta.y * RADIANS_PER_PIXEL);
		}
		else if (gCameraBtnPan)
		{
			LLVector3d lookAt = gAgentCamera.getCameraPositionGlobal() - gAgentCamera.getFocusGlobal();
			F32 pixel2meter = mHandSpeed * mHandDistanceNorm * mDeltaMetersToRad * 
								lookAt.normVec() / gViewerWindow->getWorldViewWidthScaled();

			gAgentCamera.cameraPanLeft(-delta.x * pixel2meter);
			gAgentCamera.cameraPanUp(delta.y * pixel2meter);
		}

		gAgentCamera.updateCamera();

		return;
	}

	LLRect windowRect = gViewerWindow->getWindowRect();
	S32 w = (&windowRect)->getWidth();
	S32 h = (&windowRect)->getHeight();

	LLCoordGL pt;

	pt.mX = ll_round(w * (mTouchlessOffset.x + mHandSpeed * ((0.5 * IMAGE_WIDTH) - mTouchlessCursor.x) / IMAGE_WIDTH));
	pt.mY = ll_round(h * (mTouchlessOffset.y + mHandSpeed * ((0.5 * IMAGE_HEIGHT) - mTouchlessCursor.y) / IMAGE_HEIGHT));

	pt.mX = min(w, max(0, pt.mX));
	pt.mY = min(h, max(0, pt.mY));

	// Cross-platform SetCursorPosition

	LLWindow* pWindow = gViewerWindow->getWindow();
	LLCoordWindow position;
	position.set(pt.mX, h - pt.mY);
	pWindow->setCursorPosition(position);

/*  Windows-only SetCursorPosition

	POINT wp;

	wp.x = pt.mX;
	wp.y = h - pt.mY;
		
	HWND hwnd = (HWND)gViewerWindow->getPlatformWindow();
	ClientToScreen(hwnd, &wp);
	SetCursorPos(wp.x, wp.y);
*/
}

void TARealSense::updateHandState()
{
	if (PXC_STATUS_NO_ERROR != mPXCHandData->Update()) return;

	S32 newHandStateLeft = HAND_UNKNOWN;
	S32 newHandStateRight = HAND_UNKNOWN;

	pxcUID handID0;
	pxcUID handID1; 

	PXCHandData::IHand* handData0 = NULL;
	PXCHandData::IHand* handData1 = NULL;

	pxcI32 nHands = mPXCHandData->QueryNumberOfHands();

	if (1 == nHands)
	{
		mPXCHandData->QueryHandId(PXCHandData::ACCESS_ORDER_NEAR_TO_FAR, 0, handID0);

		if (mPXCHandData->QueryHandDataById(handID0, handData0) >= PXC_STATUS_NO_ERROR)
		{
			if (PXCHandData::BodySideType::BODY_SIDE_LEFT == handData0->QueryBodySide())
			{
				updateRSMassCenterLeft(handData0->QueryMassCenterWorld());
				newHandStateLeft = handState(handData0);
				if (HAND_OPEN >= newHandStateLeft) updateInitialRSMassCenterLeft();
			}
			else
			{
				updateRSMassCenterRight(handData0->QueryMassCenterWorld());
				handData1 = handData0; // Make sure handData1 is for right hand
				newHandStateRight = handState(handData0);
				if (HAND_OPEN >= newHandStateRight) updateInitialRSMassCenterRight();
			}
		}
	}
	else if (2 <= nHands)
	{
		mPXCHandData->QueryHandId(PXCHandData::ACCESS_ORDER_NEAR_TO_FAR, 0, handID0); // Closest hand
		mPXCHandData->QueryHandId(PXCHandData::ACCESS_ORDER_NEAR_TO_FAR, 1, handID1); // Next closest hand

		if ((mPXCHandData->QueryHandDataById(handID0, handData0) >= PXC_STATUS_NO_ERROR) &&
			(mPXCHandData->QueryHandDataById(handID1, handData1) >= PXC_STATUS_NO_ERROR))
		{
			if (PXCHandData::BodySideType::BODY_SIDE_LEFT != handData0->QueryBodySide())
			{
				// Make sure handData0 is for left hand

				PXCHandData::IHand* handDataTemp = handData1;
				handData1 = handData0;
				handData0 = handDataTemp;
			}

			updateRSMassCenterLeft(handData0->QueryMassCenterWorld());
			updateRSMassCenterRight(handData1->QueryMassCenterWorld());

			updateInitialRSMassCenterLeft();
			updateInitialRSMassCenterRight();

			mRSMassCenterDelta = mRSMassCenterLeft - mRSMassCenterRight;
			updateRSDistance(mRSMassCenterDelta.length());

			newHandStateLeft = handState(handData0);
			newHandStateRight = handState(handData1);
		}
	}

	if (((2 > nHands) || (HAND_UNKNOWN < newHandStateLeft)) && (newHandStateLeft != mHandStateLeft))
	{
		mHandStateLastUpdatedLeft = gFrameTimeSeconds;
		mHandStateLeft = newHandStateLeft;
	}

	if (((2 > nHands) || (HAND_UNKNOWN < newHandStateRight)) && (newHandStateRight != mHandStateRight))
	{
		mHandStateLastUpdatedRight = gFrameTimeSeconds;
		mHandStateRight = newHandStateRight;
	}

	if (mTouchlessMouseEnabled)
	{
		bool noTouchlessUpdates = true;

		if (((HAND_OPEN == newHandStateLeft) && (HAND_OPEN != newHandStateRight)) || 
			((mDragLeft || mDragRight) && (newHandStateRight < newHandStateLeft)))
		{
			updateTouchlessCursor(handData0->QueryMassCenterImage());
			noTouchlessUpdates = false;
		}
		
		if (((HAND_OPEN == newHandStateRight) && (HAND_OPEN != newHandStateLeft)) || 
			((mDragLeft || mDragRight) && (newHandStateLeft < newHandStateRight)))
		{
			updateTouchlessCursor(handData1->QueryMassCenterImage());
			noTouchlessUpdates = false;
		}

		if (noTouchlessUpdates) mNTouchlessCursorUpdates = 0;
	}
	else
	{
		mNTouchlessCursorUpdates = 0;
	}
}

LLVector3 TARealSense::real2sim(LLVector3 v)
{
	// Transform from RealSense camera to sim coordinates

	LLViewerCamera& cam(LLViewerCamera::instance());

	LLVector3 vv(-v.mV[VZ], v.mV[VX], v.mV[VY]);
	F32 vvNorm = vv.normalize();
	LLVector3 transformed = cam.rotateToAbsolute(vv);

	return vvNorm * transformed;
}

LLVector3 TARealSense::keepObjectInFrontOfCamera(LLVector3 simDeltas)
{
	LLVector3 v = gFloaterTools->mPanelObject->getObject()->getPositionEdit();
	LLViewerCamera& cam(LLViewerCamera::instance());
	LLVector3 vc = cam.transformToLocal(v + simDeltas);
	if (0.0 > vc.mV[VX]) vc.mV[VX] = 0.0;
	return cam.transformToAbsolute(vc) - v;
}

void TARealSense::saveStartRotation(bool unconditional)
{
	EGridMode gridMode = LLSelectMgr::getInstance()->getGridMode();

	if (unconditional || (gridMode != mStartGridMode))
	{
		switch (gridMode)
		{
			case GRID_MODE_LOCAL:      mStartRotation.set(LLSelectMgr::getInstance()->
										   getSelection()->getFirstObject()->getRotationEdit());
									   break;
			case GRID_MODE_REF_OBJECT: mStartRotation.set(LLSelectMgr::getInstance()->
										   getSelection()->getFirstObject()->getRenderRotation());
									   break;
			default:                   mStartRotation.loadIdentity();
		}

		mStartGridMode = gridMode;
	}
}

LLVector3 TARealSense::sim2selectionFrame(LLVector3 v)
{
	// Transform from sim coordinates to frame of selected object

	F32 norm = v.normalize();

	return norm * (v * ~mStartRotation);
}

LLVector3 TARealSense::selectionFrame2sim(LLVector3 v)
{
	// Transform from frame of selected object to sim coordinates

	F32 norm = v.normalize();

	return norm * (v * mStartRotation);
}

F32 TARealSense::symmetricThreshold(F32 value, F32 threshold)
{
	if (value > threshold)
	{
		return (value - threshold);
	}
	
	if (-value > threshold)
	{
		return (value + threshold);
	}
	
	return 0.0;
}

LLVector3 TARealSense::deltaSimMassCenter(LLVector3 rsMassCenter, LLVector3 initialRSMassCenter)
{
	LLVector3 deltaRSMassCenter;

	deltaRSMassCenter.mV[VX] = symmetricThreshold(rsMassCenter.mV[VX] - initialRSMassCenter.mV[VX], mMinRSDistance);
	deltaRSMassCenter.mV[VY] = symmetricThreshold(rsMassCenter.mV[VY] - initialRSMassCenter.mV[VY], mMinRSDistance);
	deltaRSMassCenter.mV[VZ] = symmetricThreshold(rsMassCenter.mV[VZ] - initialRSMassCenter.mV[VZ], mMinRSDistance);

	return real2sim(deltaRSMassCenter);
}

void TARealSense::defocusHandBuild()
{
	LLSelectMgr::getInstance()->saveSelectedObjectTransform(SELECT_ACTION_TYPE_PICK);
	LLSelectMgr::getInstance()->enableSilhouette(TRUE);
	gFocusMgr.setMouseCapture(NULL);
	gViewerWindow->setCursor(UI_CURSOR_ARROW);
}

void TARealSense::setHandBuildState(int state)
{
	if (state == mHandBuildState) return;

	switch (mHandBuildState)
	{
		case HANDBUILDSTATE_MOVE:			LLSelectMgr::getInstance()->sendMultipleUpdate(UPD_POSITION);
											break;
		case HANDBUILDSTATE_ROTATE_XY:      ;
		case HANDBUILDSTATE_ROTATE_XZ:      ;
		case HANDBUILDSTATE_ROTATE_YZ:      LLSelectMgr::getInstance()->sendMultipleUpdate(UPD_ROTATION);
											break;
		case HANDBUILDSTATE_RESIZE_UNIFORM:	LLSelectMgr::getInstance()->sendMultipleUpdate(UPD_SCALE | UPD_UNIFORM);
											LLSelectMgr::getInstance()->adjustTexturesByScale(TRUE, gSavedSettings.getBOOL("ScaleStretchTextures"));
											break;
		case HANDBUILDSTATE_RESIZE_X:	    ;
		case HANDBUILDSTATE_RESIZE_Y:	    ;
		case HANDBUILDSTATE_RESIZE_Z:	    ;
		case HANDBUILDSTATE_RESIZE_LEFT_X:  ;
		case HANDBUILDSTATE_RESIZE_LEFT_Y:  ;
		case HANDBUILDSTATE_RESIZE_LEFT_Z:  ;
		case HANDBUILDSTATE_RESIZE_RIGHT_X: ;
		case HANDBUILDSTATE_RESIZE_RIGHT_Y: ;
		case HANDBUILDSTATE_RESIZE_RIGHT_Z: LLSelectMgr::getInstance()->sendMultipleUpdate(UPD_SCALE | UPD_POSITION);
											LLSelectMgr::getInstance()->adjustTexturesByScale(TRUE, gSavedSettings.getBOOL("ScaleStretchTextures"));
											break;
	}

	LLViewerObject* pPrimaryObject = NULL;

	if (gSavedSettings.getBOOL("EditLinkedParts"))
	{
		LLObjectSelectionHandle selectionHandle = LLSelectMgr::getInstance()->getEditSelection();
		if (selectionHandle.get())
		{
			pPrimaryObject = selectionHandle.get()->getPrimaryObject();
		}
	}

	LLToolMgr::getInstance()->setCurrentToolset(gBasicToolset);

	if (pPrimaryObject)
	{
		gSavedSettings.setBOOL("EditLinkedParts", true);
		gFloaterTools->mCheckSelectIndividual->set(true);
		LLSelectMgr::getInstance()->deselectAll();
		LLSelectMgr::getInstance()->selectObjectOnly(pPrimaryObject);
	}

	LLSelectMgr::getInstance()->updateSelectionCenter();

	switch (state)
	{
		case HANDBUILDSTATE_MOVE:           if (!LLToolCompTranslate::startTranslating())
											{
												state = HANDBUILDSTATE_IDLE;
												defocusHandBuild();
											}
									        break;
		case HANDBUILDSTATE_ROTATE_XY:      ;
		case HANDBUILDSTATE_ROTATE_XZ:      ;
		case HANDBUILDSTATE_ROTATE_YZ:      if (LLToolCompRotate::startRotating())
											{
												saveStartRotation(true);
											}
											else
											{
												state = HANDBUILDSTATE_IDLE;
												defocusHandBuild();
											}
									        break;
		case HANDBUILDSTATE_RESIZE_UNIFORM:	;
		case HANDBUILDSTATE_RESIZE_X:	    ;
		case HANDBUILDSTATE_RESIZE_Y:	    ;
		case HANDBUILDSTATE_RESIZE_Z:	    ;
		case HANDBUILDSTATE_RESIZE_LEFT_X:  ;
		case HANDBUILDSTATE_RESIZE_LEFT_Y:  ;
		case HANDBUILDSTATE_RESIZE_LEFT_Z:  ;
		case HANDBUILDSTATE_RESIZE_RIGHT_X: ;
		case HANDBUILDSTATE_RESIZE_RIGHT_Y: ;
		case HANDBUILDSTATE_RESIZE_RIGHT_Z: if (LLToolCompScale::startScaling())
											{
												mStartScale = gFloaterTools->mPanelObject->getObject()->getScale().length();
												saveStartRotation(true);
											}
											else
											{
												state = HANDBUILDSTATE_TRACK_TWO;
												defocusHandBuild();
											}
											mInitialRSDistance = mRSDistance;
				                            break;
		case HANDBUILDSTATE_IDLE:           defocusHandBuild();
		case HANDBUILDSTATE_SWITCH:         ;
		case HANDBUILDSTATE_TRACK_TWO:      mInitialRSDistance = mRSDistance;
	}
	
	mHandBuildState = state;
}

void TARealSense::handleSpecialBuildModeGestures()
{
	if (!editingObject())
	{
		// Not editing anything

		mNRSMassCenterLeftUpdates = 0;
		mNRSMassCenterRightUpdates = 0;
		mNRSDistanceUpdates = 0;
		mNInitialRSMassCenterLeftUpdates = 0;
		mNInitialRSMassCenterRightUpdates = 0;
		mInitialCameraObjectDistance = 0.0;

		setHandBuildState(HANDBUILDSTATE_IDLE);
	}
	else if (mHandReloadSeconds < (gFrameTimeSeconds - max(mHandStateLastUpdatedLeft, mHandStateLastUpdatedRight)))
	{
		if (((HAND_OPEN < mHandStateLeft) && (HAND_OPEN > mHandStateRight)) ||
			((HAND_OPEN < mHandStateRight) && (HAND_OPEN > mHandStateLeft)))
		{
			// One active hand: move or rotate

			LLVector3 simDeltas;
			S32 hs = HAND_UNKNOWN;

			if (HAND_OPEN < mHandStateLeft)
			{
				if (0 >= mNRSMassCenterLeftUpdates) updateInitialRSMassCenterLeft();
				simDeltas = deltaSimMassCenter(mRSMassCenterLeft, mInitialRSMassCenterLeft);
				hs = mHandStateLeft;
			}
			else
			{
				if (0 >= mNRSMassCenterRightUpdates) updateInitialRSMassCenterRight();
				simDeltas = deltaSimMassCenter(mRSMassCenterRight, mInitialRSMassCenterRight);
				hs = mHandStateRight;
			}

			if (HAND_CLOSED == hs)
			{
				// Hand closed (move object)

				if (0.0 >= mInitialCameraObjectDistance)
				{
					updateInitialCameraObjectDistance();
				}

				if (0.0 < mInitialCameraObjectDistance)
				{
					setHandBuildState(HANDBUILDSTATE_MOVE);
					LLToolCompTranslate::translate(keepObjectInFrontOfCamera(simDeltas * mHandSpeed * mHandDistanceNorm * mInitialCameraObjectDistance), false);
				}
			}
			else if (HAND_GUN == hs)
			{
				// Hand "gun" (rotate object)

				saveStartRotation(false);

				LLViewerCamera& cam(LLViewerCamera::instance());
				LLVector3 camVector = sim2selectionFrame(cam.rotateToAbsolute(LLVector3(1.0, 0.0, 0.0)));

				LLVector3 transformedDeltas = sim2selectionFrame(simDeltas);

				if (abs(transformedDeltas.mV[VX]) > abs(transformedDeltas.mV[VY]))
				{
					if (abs(transformedDeltas.mV[VX]) > abs(transformedDeltas.mV[VZ]))
					{
						if (abs(camVector.mV[VY]) > abs(camVector.mV[VZ]))
						{
							setHandBuildState(HANDBUILDSTATE_ROTATE_XY);
							LLToolCompRotate::rotate(LLQuaternion(mHandSpeed * mDeltaMetersToRad * transformedDeltas.mV[VX], 
																  selectionFrame2sim(LLVector3(0.0, 0.0, camVector.mV[VY] / abs(camVector.mV[VY])))), 
													 false);
						}
						else
						{
							setHandBuildState(HANDBUILDSTATE_ROTATE_XZ);
							LLToolCompRotate::rotate(LLQuaternion(mHandSpeed * mDeltaMetersToRad * transformedDeltas.mV[VX], 
																  selectionFrame2sim(LLVector3(0.0, -camVector.mV[VY] / abs(camVector.mV[VY]), 0.0))), 
													 false);
						}
					}
					else
					{
						if (abs(camVector.mV[VX]) > abs(camVector.mV[VY]))
						{
							setHandBuildState(HANDBUILDSTATE_ROTATE_XZ);
							LLToolCompRotate::rotate(LLQuaternion(mHandSpeed * mDeltaMetersToRad * transformedDeltas.mV[VZ], 
																  selectionFrame2sim(LLVector3(0.0, camVector.mV[VX] / abs(camVector.mV[VX]), 0.0))), 
													 false);
						}
						else
						{
							setHandBuildState(HANDBUILDSTATE_ROTATE_YZ);
							LLToolCompRotate::rotate(LLQuaternion(mHandSpeed * mDeltaMetersToRad * transformedDeltas.mV[VZ], 
																  selectionFrame2sim(LLVector3(-camVector.mV[VY] / abs(camVector.mV[VY]), 0.0, 0.0))), 
													 false);
						}
					}
				}
				else
				{
					if (abs(transformedDeltas.mV[VY]) > abs(transformedDeltas.mV[VZ]))
					{
						if (abs(camVector.mV[VX]) > abs(camVector.mV[VZ]))
						{
							setHandBuildState(HANDBUILDSTATE_ROTATE_XY);
							LLToolCompRotate::rotate(LLQuaternion(mHandSpeed * mDeltaMetersToRad * transformedDeltas.mV[VY], 
																  selectionFrame2sim(LLVector3(0.0, 0.0, -camVector.mV[VX] / abs(camVector.mV[VX])))), 
													 false);
						}
						else
						{
							setHandBuildState(HANDBUILDSTATE_ROTATE_YZ);
							LLToolCompRotate::rotate(LLQuaternion(mHandSpeed * mDeltaMetersToRad * transformedDeltas.mV[VY], 
																  selectionFrame2sim(LLVector3(camVector.mV[VZ] / abs(camVector.mV[VZ]), 0.0, 0.0))), 
													 false);
						}
					}
					else
					{
						if (abs(camVector.mV[VX]) > abs(camVector.mV[VY]))
						{
							setHandBuildState(HANDBUILDSTATE_ROTATE_XZ);
							LLToolCompRotate::rotate(LLQuaternion(mHandSpeed * mDeltaMetersToRad * transformedDeltas.mV[VZ], 
																  selectionFrame2sim(LLVector3(0.0, camVector.mV[VX] / abs(camVector.mV[VX]), 0.0))), 
													 false);
						}
						else
						{
							setHandBuildState(HANDBUILDSTATE_ROTATE_YZ);
							LLToolCompRotate::rotate(LLQuaternion(mHandSpeed * mDeltaMetersToRad * transformedDeltas.mV[VZ], 
																  selectionFrame2sim(LLVector3(-camVector.mV[VY] / abs(camVector.mV[VY]), 0.0, 0.0))), 
													 false);
						}
					}
				}
			}
			else
			{
				// Meaningless hand configuration

				setHandBuildState(HANDBUILDSTATE_SWITCH);
			}
		}
		else if ((HAND_OPEN < mHandStateLeft) && (HAND_OPEN < mHandStateRight))
		{
			// Two active hands

			if ((0 < mNRSDistanceUpdates) && (mInitialRSDistance > 0.1))
			{
				F32 scaleFactor = mRSDistance / mInitialRSDistance;

				if ((HAND_CLOSED == mHandStateLeft) && (HAND_CLOSED == mHandStateRight))
				{
					// Both hands closed: resize uniformly along all axes

					setHandBuildState(HANDBUILDSTATE_RESIZE_UNIFORM);
					LLToolCompScale::scaleAll(scaleFactor, true, false);
				}
				else if ((HAND_GUN == mHandStateLeft) || (HAND_GUN == mHandStateRight))
				{
					// At least one hand is "gun"; determine axis with greatest projection

					saveStartRotation(false);

					S32 axisNumber;

					LLVector3 rsMassCenterDeltaTransformed = sim2selectionFrame(real2sim(mRSMassCenterDelta));
										
					if (abs(rsMassCenterDeltaTransformed.mV[VX]) > abs(rsMassCenterDeltaTransformed.mV[VY]))
					{
						axisNumber = (abs(rsMassCenterDeltaTransformed.mV[VX]) > abs(rsMassCenterDeltaTransformed.mV[VZ])) ? 0 : 2;
					}
					else
					{
						axisNumber = (abs(rsMassCenterDeltaTransformed.mV[VY]) > abs(rsMassCenterDeltaTransformed.mV[VZ])) ? 1 : 2;
					}

					F32 scaleDelta = (scaleFactor - 1.0) * mStartScale;

					if (HAND_GUN == mHandStateLeft)
					{
						if (HAND_GUN == mHandStateRight)
						{
							// Both hands are "guns": resize on both sides of axis with greatest projection

							switch (axisNumber)
							{
								case 0: setHandBuildState(HANDBUILDSTATE_RESIZE_X);
										LLToolCompScale::scaleOne(selectionFrame2sim(LLVector3(1.0, 0.0, 0.0)), 
																  selectionFrame2sim(LLVector3(scaleDelta, 0.0, 0.0)), true, false);
										break;
								case 1: setHandBuildState(HANDBUILDSTATE_RESIZE_Y);
										LLToolCompScale::scaleOne(selectionFrame2sim(LLVector3(0.0, 1.0, 0.0)), 
																  selectionFrame2sim(LLVector3(0.0, scaleDelta, 0.0)), true, false);
										break;
								case 2: setHandBuildState(HANDBUILDSTATE_RESIZE_Z);
										LLToolCompScale::scaleOne(selectionFrame2sim(LLVector3(0.0, 0.0, 1.0)), 
																  selectionFrame2sim(LLVector3(0.0, 0.0, scaleDelta)), true, false);
										break;
							}
						}
						else if (HAND_CLOSED == mHandStateRight)
						{
							// Left hand is "gun", right closed: resize along axis with greatest projection

							F32 sign;

							switch (axisNumber)
							{
								case 0: setHandBuildState(HANDBUILDSTATE_RESIZE_LEFT_X);
										sign = rsMassCenterDeltaTransformed.mV[VX] / abs(rsMassCenterDeltaTransformed.mV[VX]);
										LLToolCompScale::scaleOne(selectionFrame2sim(LLVector3(sign, 0.0, 0.0)), 
																  selectionFrame2sim(LLVector3(sign * scaleDelta, 0.0, 0.0)), false, false);
										break;
								case 1: setHandBuildState(HANDBUILDSTATE_RESIZE_LEFT_Y);
										sign = rsMassCenterDeltaTransformed.mV[VY] / abs(rsMassCenterDeltaTransformed.mV[VY]);
										LLToolCompScale::scaleOne(selectionFrame2sim(LLVector3(0.0, sign, 0.0)), 
																  selectionFrame2sim(LLVector3(0.0, sign * scaleDelta, 0.0)), false, false);
										break;
								case 2: setHandBuildState(HANDBUILDSTATE_RESIZE_LEFT_Z);
										sign = rsMassCenterDeltaTransformed.mV[VZ] / abs(rsMassCenterDeltaTransformed.mV[VZ]);
										LLToolCompScale::scaleOne(selectionFrame2sim(LLVector3(0.0, 0.0, sign)), 
																  selectionFrame2sim(LLVector3(0.0, 0.0, sign * scaleDelta)), false, false);
										break;
							}
						}
					}
					else if (HAND_CLOSED == mHandStateLeft)
					{
						// Right hand is "gun", left closed: resize along axis with greatest projection

						F32 sign;

						switch (axisNumber)
						{
							case 0: setHandBuildState(HANDBUILDSTATE_RESIZE_RIGHT_X);
									sign = -rsMassCenterDeltaTransformed.mV[VX] / abs(rsMassCenterDeltaTransformed.mV[VX]);
									LLToolCompScale::scaleOne(selectionFrame2sim(LLVector3(sign, 0.0, 0.0)), 
															  selectionFrame2sim(LLVector3(sign * scaleDelta, 0.0, 0.0)), false, false);
									break;
							case 1: setHandBuildState(HANDBUILDSTATE_RESIZE_RIGHT_Y);
									sign = -rsMassCenterDeltaTransformed.mV[VY] / abs(rsMassCenterDeltaTransformed.mV[VY]);
									LLToolCompScale::scaleOne(selectionFrame2sim(LLVector3(0.0, sign, 0.0)), 
															  selectionFrame2sim(LLVector3(0.0, sign * scaleDelta, 0.0)), false, false);
									break;
							case 2: setHandBuildState(HANDBUILDSTATE_RESIZE_RIGHT_Z);
									sign = -rsMassCenterDeltaTransformed.mV[VZ] / abs(rsMassCenterDeltaTransformed.mV[VZ]);
									LLToolCompScale::scaleOne(selectionFrame2sim(LLVector3(0.0, 0.0, sign)), 
															  selectionFrame2sim(LLVector3(0.0, 0.0, sign * scaleDelta)), false, false);
									break;
						}
					}
					else
					{
						// Meaningless hand configuration

						setHandBuildState(HANDBUILDSTATE_TRACK_TWO);
					}
				}
				else
				{
					// Meaningless hand configuration

					setHandBuildState(HANDBUILDSTATE_TRACK_TWO);
				}
			}
			else
			{
				// Insufficient hand distance info

				setHandBuildState(HANDBUILDSTATE_TRACK_TWO);
			}
		}
		else
		{
			// No active hands

			setHandBuildState(HANDBUILDSTATE_IDLE);
		}
	}
}

S32 TARealSense::gesture2action(std::wstring gestureName, bool body_side_left)
{
	if (L"tap" == gestureName) return mTapCommandActionArgument.action;
	if (L"click" == gestureName) return mClickCommandActionArgument.action;
	if (L"two_fingers_pinch_open" == gestureName) return mTwoFingersPinchOpenCommandActionArgument.action;
	if (L"v_sign" == gestureName) return mVSignCommandActionArgument.action;
	if (L"thumb_down" == gestureName) return mThumbDownCommandActionArgument.action;
	if (L"thumb_up" == gestureName) return mThumbUpCommandActionArgument.action;
	if (L"swipe_down" == gestureName) return mSwipeDownCommandActionArgument.action;
	if (L"swipe_up" == gestureName) return mSwipeUpCommandActionArgument.action;
	if (L"swipe" == gestureName) return (body_side_left ? mSwipeLeftCommandActionArgument.action : 
															mSwipeRightCommandActionArgument.action);
	if (L"fist" == gestureName) return mFistCommandActionArgument.action;
	if (L"wave" == gestureName) return mWaveCommandActionArgument.action;
	if (L"full_pinch" == gestureName) return mFullPinchCommandActionArgument.action;
	if (L"spreadfingers" == gestureName) return mSpreadFingersCommandActionArgument.action;
	
	return ACTION_NONE;
}

void TARealSense::cleanGestures()
{
	mSpreadFingersDirty = false;
	mFistDirty = false;
	mTapDirty = false;
	mThumbDownDirty = false;
	mThumbUpDirty = false;
	mTwoFingersPinchOpenDirty = false;
	mVSignDirty = false;
	mFullPinchDirty = false;
	mSwipeLeftDirty = false;
	mSwipeRightDirty = false;
	mWaveDirty = false;
	mClickDirty = false;
	mSwipeDownDirty = false;
	mSwipeUpDirty = false;
}

void TARealSense::noteGestures()
{
	PXCHandData::GestureData gestureData;

	mTapDirty |= (mPXCHandData->IsGestureFired(L"tap", gestureData));

	if (mPXCHandData->IsGestureFired(L"swipe", gestureData))
	{
		PXCHandData::IHand* handData;

		if (mPXCHandData->QueryHandDataById(gestureData.handId, handData) >= PXC_STATUS_NO_ERROR)
		{
			mSwipeRightDirty |= (PXCHandData::BodySideType::BODY_SIDE_LEFT == handData->QueryBodySide());
			mSwipeLeftDirty |= (PXCHandData::BodySideType::BODY_SIDE_RIGHT == handData->QueryBodySide());
		}
	}

	mTwoFingersPinchOpenDirty |= (mPXCHandData->IsGestureFired(L"two_fingers_pinch_open", gestureData));
	mVSignDirty |= (mPXCHandData->IsGestureFired(L"v_sign", gestureData));
	mThumbDownDirty |= (mPXCHandData->IsGestureFired(L"thumb_down", gestureData));
	mThumbUpDirty |= (mPXCHandData->IsGestureFired(L"thumb_up", gestureData));
	mFistDirty |= (mPXCHandData->IsGestureFired(L"fist", gestureData));
	mWaveDirty |= (mPXCHandData->IsGestureFired(L"wave", gestureData));
	mFullPinchDirty |= (mPXCHandData->IsGestureFired(L"full_pinch", gestureData));
	mSpreadFingersDirty |= (mPXCHandData->IsGestureFired(L"spreadfingers", gestureData));
	mClickDirty |= (mPXCHandData->IsGestureFired(L"click", gestureData));
	mSwipeDownDirty |= (mPXCHandData->IsGestureFired(L"swipe_down", gestureData));
	mSwipeUpDirty |= (mPXCHandData->IsGestureFired(L"swipe_up", gestureData));
}

void TARealSense::handleNormalGestures(bool touchlessMouseOnly)
{
	if (touchlessMouseOnly && (!mTouchlessMouseEnabled)) return;

	if (mTapDirty)
	{
		triggerAction(&mTapCommandActionArgument, touchlessMouseOnly, true);
		return;
	}

	if (mClickDirty)
	{
		triggerAction(&mClickCommandActionArgument, touchlessMouseOnly, true);
		return;
	}

	if (mSwipeLeftDirty)
	{
		triggerAction(&mSwipeLeftCommandActionArgument, touchlessMouseOnly, true);
		return;
	}

	if (mSwipeRightDirty)
	{
		triggerAction(&mSwipeRightCommandActionArgument, touchlessMouseOnly, true);
		return;
	}

	if (mTwoFingersPinchOpenDirty)
	{
		triggerAction(&mTwoFingersPinchOpenCommandActionArgument, touchlessMouseOnly, true);
		return;
	}

	if (mVSignDirty)
	{
		triggerAction(&mVSignCommandActionArgument, touchlessMouseOnly, true);
		return;
	}

	if (mThumbDownDirty)
	{
		triggerAction(&mThumbDownCommandActionArgument, touchlessMouseOnly, true);
		return;
	}

	if (mThumbUpDirty)
	{
		triggerAction(&mThumbUpCommandActionArgument, touchlessMouseOnly, true);
		return;
	}

	if (mSwipeDownDirty)
	{
		triggerAction(&mSwipeDownCommandActionArgument, touchlessMouseOnly, true);
		return;
	}

	if (mSwipeUpDirty)
	{
		triggerAction(&mSwipeUpCommandActionArgument, touchlessMouseOnly, true);
		return;
	}

	if (mFistDirty)
	{
		triggerAction(&mFistCommandActionArgument, touchlessMouseOnly, true);
		return;
	}
	
	if (mWaveDirty)
	{
		triggerAction(&mWaveCommandActionArgument, touchlessMouseOnly, true);
		return;
	}

	if (mFullPinchDirty)
	{
		triggerAction(&mFullPinchCommandActionArgument, touchlessMouseOnly, true);
		return;
	}
	
	if (mSpreadFingersDirty)
	{
		triggerAction(&mSpreadFingersCommandActionArgument, touchlessMouseOnly, true);
		return;
	}
}

void TARealSense::notePose()
{
	if (PXC_STATUS_NO_ERROR != mPXCFaceData->Update()) return;

	// Find closest face

	pxcI32 faceIdx = -1;
	pxcF32 minFaceDepth = 1000000.0; // 1000 m, in mm

	pxcI32 nFaces = mPXCFaceData->QueryNumberOfDetectedFaces(); 
	for (pxcI32 i = 0; i < nFaces; i++)
	{
		PXCFaceData::Face* face = mPXCFaceData->QueryFaceByIndex(i);
		PXCFaceData::DetectionData* dData = face->QueryDetection();

		pxcF32 depth;
		if (dData->QueryFaceAverageDepth(&depth))
		{
			if (depth < minFaceDepth)
			{
				minFaceDepth = depth;
				faceIdx = i;
			}
		}
	}

	// If we have a closest face, determine its pose

	if (-1 < faceIdx)
	{
		updateFaceDepth(minFaceDepth);

		PXCFaceData::Face* face = mPXCFaceData->QueryFaceByIndex(faceIdx);

		if (face)
		{
			PXCFaceData::PoseData* poseData = face->QueryPose();

			if (poseData)
			{
				poseData->QueryPoseAngles(&mPoseAngles);

				// Do not release poseData, it's internally managed
			}

			// Do not release face, it's internally managed
		}
	}
	else
	{
		mNInitialFaceDepthUpdates = 0;
	}
}

void TARealSense::handlePose()
{
	PXCFaceData::PoseEulerAngles angles = mPoseAngles;

	angles.yaw += mYawDegreesOffset;
	angles.pitch += mPitchDegreesOffset;
	angles.roll += mRollDegreesOffset;

	if (angles.yaw > mYawDegreesThreshold)
	{
		mnYawMoves -= ((angles.yaw - mYawDegreesThreshold) / (360.0 - mYawDegreesThreshold)) * 
						mYawMaxMovesPerSecond * (gFrameTimeSeconds - mLastPoseTime);

		while (mnYawMoves <= -1.0)
		{
			if (mBuildMode && (BUILD_HEAD_TRACKING_NORMAL != mPoseBuildMode))
			{
				if (BUILD_HEAD_TRACKING_ORBIT_CAMERA == mPoseBuildMode)
				{
					gAgentCamera.cameraOrbitAround(-mCameraOrbitStepDegrees * DEG_TO_RAD);
				}
			}
			else
			{
				gAgent.moveYaw(-1);
			}
			mnYawMoves += 1.0;
		}
	}
	else if (-angles.yaw > mYawDegreesThreshold)
	{
		mnYawMoves -= ((angles.yaw + mYawDegreesThreshold) / (360.0 - mYawDegreesThreshold)) * 
						mYawMaxMovesPerSecond * (gFrameTimeSeconds - mLastPoseTime);

		while (mnYawMoves >= 1.0)
		{
			if (mBuildMode && (BUILD_HEAD_TRACKING_NORMAL != mPoseBuildMode))
			{
				if (BUILD_HEAD_TRACKING_ORBIT_CAMERA == mPoseBuildMode)
				{
					gAgentCamera.cameraOrbitAround(mCameraOrbitStepDegrees * DEG_TO_RAD);
				}
			}
			else
			{
				gAgent.moveYaw(1);
			}
			mnYawMoves -= 1.0;
		}
	}

	if (!gAgentAvatarp->isSitting() || 
		(mBuildMode && (BUILD_HEAD_TRACKING_ORBIT_CAMERA == mPoseBuildMode)))
	{
		if (angles.pitch > mPitchUpDegreesThreshold)
		{
			mnPitchMoves += ((angles.pitch - mPitchUpDegreesThreshold) / (360.0 - mPitchUpDegreesThreshold)) * 
							mPitchMaxMovesPerSecond * (gFrameTimeSeconds - mLastPoseTime);

			if ((mnPitchMoves >= 2.0) &&
				((!mBuildMode) || (BUILD_HEAD_TRACKING_NORMAL == mPoseBuildMode)) && 
				(!gAgent.getFlying()))
			{
				gAgent.setFlying(true);
				mnPitchMoves -= 1.0;
			}
								
			while (mnPitchMoves >= 1.0)
			{
				if (mBuildMode && (BUILD_HEAD_TRACKING_NORMAL != mPoseBuildMode))
				{
					if (BUILD_HEAD_TRACKING_ORBIT_CAMERA == mPoseBuildMode)
					{
						gAgentCamera.cameraOrbitOver(-mCameraOrbitStepDegrees * DEG_TO_RAD);
					}
				}
				else
				{
					gAgent.moveUp(1);
				}
				mnPitchMoves -= 1.0;
			}
		}
		else if (-angles.pitch > mPitchDownDegreesThreshold)
		{
			mnPitchMoves += ((angles.pitch + mPitchDownDegreesThreshold) / (360.0 - mPitchDownDegreesThreshold)) * 
							mPitchMaxMovesPerSecond * (gFrameTimeSeconds - mLastPoseTime);

			if ((mnPitchMoves < -1.0) && 
				((!mBuildMode) || (BUILD_HEAD_TRACKING_NORMAL == mPoseBuildMode)) &&
				(!gAgent.getFlying()))
			{
				mnPitchMoves = -1.0;
			}
								
			while (mnPitchMoves <= -1.0)
			{
				if (mBuildMode && (BUILD_HEAD_TRACKING_NORMAL != mPoseBuildMode))
				{
					if (BUILD_HEAD_TRACKING_ORBIT_CAMERA == mPoseBuildMode)
					{
						gAgentCamera.cameraOrbitOver(mCameraOrbitStepDegrees * DEG_TO_RAD);
					}
				}
				else
				{
					gAgent.moveUp(-1);
				}
				mnPitchMoves += 1.0;
			}
		}

		if ((!mBuildMode) || (BUILD_HEAD_TRACKING_NORMAL == mPoseBuildMode))
		{
			if (angles.roll > mRollDegreesThreshold)
			{
				mnRollMoves -= ((angles.roll - mRollDegreesThreshold) / (360.0 - mRollDegreesThreshold)) * 
								mRollMaxMovesPerSecond * (gFrameTimeSeconds - mLastPoseTime);

				while (mnRollMoves <= -1.0)
				{
					gAgent.moveLeft(-1);
					mnRollMoves += 1.0;
				}
			}
			else if (-angles.roll > mRollDegreesThreshold)
			{
				mnRollMoves -= ((angles.roll + mRollDegreesThreshold) / (360.0 - mRollDegreesThreshold)) * 
								mRollMaxMovesPerSecond * (gFrameTimeSeconds - mLastPoseTime);

				while (mnRollMoves >= 1.0)
				{
					gAgent.moveLeft(1);
					mnRollMoves -= 1.0;
				}
			}
		}
		else if (mBuildMode && (BUILD_HEAD_TRACKING_ORBIT_CAMERA == mPoseBuildMode))
		{
			LLPointer<LLViewerObject> editObject = gFloaterTools->mPanelObject->getObject();
			if ((editObject) && (!editObject.isNull()))
			{
				LLVector3 editObjectPosition = editObject->getPosition();

				LLViewerCamera& cam(LLViewerCamera::instance());

				F32 panStep = mPanDistanceNorm * cam.visibleDistance(editObjectPosition, 0.0);

				if (angles.roll > mRollDegreesThreshold)
				{
					mnRollMoves -= ((angles.roll - mRollDegreesThreshold) / (360.0 - mRollDegreesThreshold)) * 
									mRollMaxMovesPerSecond * (gFrameTimeSeconds - mLastPoseTime);

					while (mnRollMoves <= -1.0)
					{
						gAgentCamera.cameraPanLeft(-panStep);
						gAgentCamera.updateCamera();
						mnRollMoves += 1.0;
					}
				}
				else if (-angles.roll > mRollDegreesThreshold)
				{
					mnRollMoves -= ((angles.roll + mRollDegreesThreshold) / (360.0 - mRollDegreesThreshold)) * 
									mRollMaxMovesPerSecond * (gFrameTimeSeconds - mLastPoseTime);

					while (mnRollMoves >= 1.0)
					{
						gAgentCamera.cameraPanLeft(panStep);
						gAgentCamera.updateCamera();
						mnRollMoves -= 1.0;
					}
				}
			}
		}
	}

	bool doUpdateInitialFaceDepth = true;

	if (0 < mNInitialFaceDepthUpdates)
	{
		if (mBuildMode && (BUILD_HEAD_TRACKING_ORBIT_CAMERA == mPoseBuildMode))
		{
			LLPointer<LLViewerObject> editObject = gFloaterTools->mPanelObject->getObject();
			if ((editObject) && (!editObject.isNull()))
			{
				LLVector3 editObjectPosition = editObject->getPosition();

				LLViewerCamera& cam(LLViewerCamera::instance());

				gAgentCamera.cameraOrbitIn(mHeadDistanceNorm * 
					abs(cam.visibleDistance(editObjectPosition, 0.0)) * (mInitialFaceDepth - mFaceDepth));

				doUpdateInitialFaceDepth = false;
			}
		}
	}

	if (doUpdateInitialFaceDepth) updateInitialFaceDepth();
}

void TARealSense::handleEmotions()
{
	PXCEmotion* emo = mPXCSenseManager->QueryEmotion();

	if (emo)
	{
		int nFaces = emo->QueryNumFaces();
		if (nFaces)
		{
			const pxcI32 N_EMOTIONS = 10;

			PXCEmotion::EmotionData emoData[N_EMOTIONS];

			// Look for the largest (and therefore presumably closest) face

			pxcF32 maxArea = -1.0;
			pxcF32 primaryIntensity = 0.0; 
			pxcF32 sentimentIntensity = 0.0;
			pxcI32 primaryEvidence = -10;   // Actual range is -5 to 5
			pxcI32 sentimentEvidence = -10; // Ditto

			PXCEmotion::Emotion primaryEid = PXCEmotion::EMOTION_PRIMARY_JOY;
			PXCEmotion::Emotion sentimentEid = PXCEmotion::EMOTION_SENTIMENT_POSITIVE;

			for (int i = 0; i < nFaces; i++)
			{
				emo->QueryAllEmotionData(i, emoData);

				pxcF32 faceArea = pxcF32(emoData[0].rectangle.w) * pxcF32(emoData[0].rectangle.h);
				if (faceArea > maxArea)
				{
					maxArea = faceArea;

					// Look for the dominating emotion and sentiment

					primaryEvidence = -10;
					sentimentEvidence = -10;
					primaryIntensity = 0.0;
					sentimentIntensity = 0.0;

					for (int j = 0; j < N_EMOTIONS; j++)
					{
						pxcI32 newEvidence;
						bool isPrimary = true;

						switch (emoData[j].eid)
						{
							case PXCEmotion::EMOTION_PRIMARY_ANGER:    
								newEvidence = mAngerEvidenceBias; break;
							case PXCEmotion::EMOTION_PRIMARY_CONTEMPT: 
								newEvidence = mContemptEvidenceBias; break;
							case PXCEmotion::EMOTION_PRIMARY_DISGUST:  
								newEvidence = mDisgustEvidenceBias; break;
							case PXCEmotion::EMOTION_PRIMARY_FEAR:     
								newEvidence = mFearEvidenceBias; break;
							case PXCEmotion::EMOTION_PRIMARY_JOY:      
								newEvidence = mJoyEvidenceBias; break;
							case PXCEmotion::EMOTION_PRIMARY_SADNESS:  
								newEvidence = mSadnessEvidenceBias; break;
							case PXCEmotion::EMOTION_PRIMARY_SURPRISE: 
								newEvidence = mSurpriseEvidenceBias; break;

							case PXCEmotion::EMOTION_SENTIMENT_NEGATIVE:
								newEvidence = mSadnessEvidenceBias; 
								isPrimary = false;
								break;
							case PXCEmotion::EMOTION_SENTIMENT_POSITIVE:
								newEvidence = mJoyEvidenceBias; 
								isPrimary = false;
								break;

							default: newEvidence = 0;
									 isPrimary = false;
						}

						newEvidence += emoData[j].evidence;

						if (isPrimary)
						{
							if (newEvidence > primaryEvidence)
							{
								primaryEvidence = newEvidence;
								primaryEid = emoData[j].eid;
							}

							// Keep this outside the evidence check so highest intensity is kept
							// regardless of origin (otherwise, highly biased low-intensity emotion
							// will never get over intensity threshold).
							if (emoData[j].intensity > primaryIntensity)
							{
								primaryIntensity = emoData[j].intensity;
							}
						}
						else
						{
							if (newEvidence > sentimentEvidence)
							{
								sentimentEvidence = newEvidence;
								sentimentEid = emoData[j].eid;
							}

							// Keep this outside the evidence check so highest intensity is kept
							// regardless of origin (otherwise, highly biased low-intensity emotion
							// will never get over intensity threshold).
							if (emoData[j].intensity > sentimentIntensity)
							{
								sentimentIntensity = emoData[j].intensity;
							}
						}
					}
				}
			}

			// Determine new emotion animation

			LLUUID animationID;

			if ((primaryEvidence >= 0) && (primaryIntensity > mLoEmoThreshold))
			{
				switch (primaryEid)
				{
					case PXCEmotion::EMOTION_PRIMARY_ANGER: 
						animationID = ANIM_AGENT_EXPRESS_ANGER; break;
					case PXCEmotion::EMOTION_PRIMARY_CONTEMPT: 
						animationID = ANIM_AGENT_EXPRESS_DISDAIN; break;
					case PXCEmotion::EMOTION_PRIMARY_DISGUST: 
						animationID = ANIM_AGENT_EXPRESS_REPULSED; break;
					case PXCEmotion::EMOTION_PRIMARY_FEAR: 
						animationID = ANIM_AGENT_EXPRESS_AFRAID; break;
					case PXCEmotion::EMOTION_PRIMARY_JOY: 
						animationID = (primaryIntensity < mHiEmoThreshold) ?
										ANIM_AGENT_EXPRESS_TOOTHSMILE : ANIM_AGENT_EXPRESS_LAUGH;
						break;
					case PXCEmotion::EMOTION_PRIMARY_SADNESS: 
						animationID = (primaryIntensity < mHiEmoThreshold) ?
										ANIM_AGENT_EXPRESS_SAD : ANIM_AGENT_EXPRESS_CRY;
						break;
					case PXCEmotion::EMOTION_PRIMARY_SURPRISE: 
						animationID = ANIM_AGENT_EXPRESS_SURPRISE; break;
				}
			}
			else if ((sentimentEvidence >= 0) && (sentimentIntensity > mLoEmoThreshold))
			{
				switch (sentimentEid)
				{
					case PXCEmotion::EMOTION_SENTIMENT_NEGATIVE: 
						animationID = ANIM_AGENT_EXPRESS_WORRY; break;
					case PXCEmotion::EMOTION_SENTIMENT_POSITIVE: 
						animationID = ANIM_AGENT_EXPRESS_SMILE; break;
				}
			}

			if (mEmotionAnimationID != animationID)
			{
				if (mEmotionAnimationID.notNull())
				{
					gAgent.sendAnimationRequest(mEmotionAnimationID, ANIM_REQUEST_STOP);
				}

				mEmotionAnimationID = animationID;
			}

			// Keep restarting the animation as long as the triggering emotion is detected,
			// since SL facial expressions are not looped

			if (mEmotionAnimationID.notNull())
			{
				gAgent.sendAnimationRequest(mEmotionAnimationID, ANIM_REQUEST_START);
			}
		}
	}
}

TCommandActionArgument* PXCSpeechRecognitionHandler::getOverriderPtr(S32 idx)
// IMPORTANT: Note that a command can be its own overrider. A null pointer will only be returned if the command does not exist at all.
// Next candidates on the other hand can be set to -1 if they do not exist. This is also the default value, since we don't need to
// distinguish between "not set yet" and "does not exist" (candidates are found when overriders are determined, and overriders are set
// to -1 only before they have been determined).
{
	TCommandActionArgument* p = getCommandActionArgumentPtr(idx);

	if (!p) return NULL;

	if (0 > p->overrider)
	{
		// Overrider not set yet. Let's look. Collect XUI clicks with the same command...

		std::vector<std::pair<int, int>> arglen_idx_vector;
		int first_other = -1;

		for (TCommandActionArgumentVector::iterator it = mCommandActionArgumentVector.begin(); it != mCommandActionArgumentVector.end(); it++) 
		{
			if (it->command == p->command)
			{
				if (TARealSense::ACTION_XUICLICK == it->action)
				{
					arglen_idx_vector.push_back(std::pair<int, int>(it->argument.length(), int(it - mCommandActionArgumentVector.begin())));
				}
				else if (0 > first_other)
				{
					first_other = int(it - mCommandActionArgumentVector.begin());
				}
			}
		}

		if (1 > arglen_idx_vector.size())
		{
			// No XUI click found. Return no next candidates.

			p->overrider = idx; // This command is its own overrider.
			p->nextCandidate = -1;

			return p;
		}

		if (1 == arglen_idx_vector.size())
		{
			// One XUI click found. No need to sort. Next candidate is first other action for same command, if any.

			p->overrider = arglen_idx_vector[0].second;

			p = getCommandActionArgumentPtr(p->overrider);

			p->overrider = arglen_idx_vector[0].second; // Make sure overrider overrides itself.
			p->nextCandidate = first_other;

			// Make sure all other actions with same command have same overrider.

			for (TCommandActionArgumentVector::iterator it = mCommandActionArgumentVector.begin(); it != mCommandActionArgumentVector.end(); it++) 
			{
				if ((TARealSense::ACTION_XUICLICK != it->action) && (it->command == p->command))
				{
					it->overrider = p->overrider;
				}
			}

			return p;
		}

		// Sort XUI clicks on argument lengths, longest first.

		std::sort(arglen_idx_vector.begin(), arglen_idx_vector.end(), 
				  boost::bind(&std::pair<int, int>::first, _1) > boost::bind(&std::pair<int, int>::first, _2));

		// Go down list of XUI clicks; assign same overrider to all, subsequent item as nextCandidate to each.

		p->overrider = arglen_idx_vector[0].second;

		std::vector<std::pair<int, int>>::iterator it = arglen_idx_vector.begin();
		std::vector<std::pair<int, int>>::iterator nextIt = arglen_idx_vector.begin();

		do
		{
			mCommandActionArgumentVector[it->second].overrider = p->overrider;

			nextIt++;

			if (nextIt == arglen_idx_vector.end())
			{
				// Next candidate for last XUI click is first other action for same command, if any.
				mCommandActionArgumentVector[it->second].nextCandidate = first_other;
				break;
			}

			mCommandActionArgumentVector[it->second].nextCandidate = nextIt->second;

			it++;
		}
		while (true);

		// Make sure all other actions with same command have same overrider.

		for (TCommandActionArgumentVector::iterator it = mCommandActionArgumentVector.begin(); it != mCommandActionArgumentVector.end(); it++) 
		{
			if ((TARealSense::ACTION_XUICLICK != it->action) && (it->command == p->command))
			{
				it->overrider = p->overrider;
			}
		}
	}

	return getCommandActionArgumentPtr(p->overrider);
}

void PXCAPI PXCSpeechRecognitionHandler::OnRecognition(const PXCSpeechRecognition::RecognitionData *data)
{
	if (mPaused) return;

	if (!gFocusMgr.getAppHasFocus()) return;

	if (0 > data->scores[0].label)
	{
		// Dictation sentence

		if ((mNExpectedDictationSentences > 0) && (mOwner) && (mOwnerOnDictation) && 
			(gFrameTimeSeconds> mMinDictationFrameTimeSeconds))
		{
			mNExpectedDictationSentences--;
			mOwnerOnDictation(mOwner, &(data->scores[0].sentence[0]), 0 >= mNExpectedDictationSentences);
		}

		return;
	}

	// Command

	pxcI32 bestConfidence = -1;
	S32 bestIdx = -1;

	for (S32 i = 0; i < PXCSpeechRecognition::NBEST_SIZE; i++)
	{
		if (data->scores[i].confidence > bestConfidence)
		{
			bestConfidence = data->scores[i].confidence;
			bestIdx = i;
		}
	}

	if (-1 < bestIdx)
	{
		TCommandActionArgument* p = getOverriderPtr(data->scores[bestIdx].label);

		if (!p) return;

		if ((mRSFloater) && (mRSOnRecognition))
		{
			mRSOnRecognition(mRSFloater, p, data->scores[bestIdx].confidence);
		}
		
		if ((mOwner) && (mOwnerOnRecognition))
		{
			mOwnerOnRecognition(mOwner, p, data->scores[bestIdx].confidence);
		}
	}
}

void PXCAPI PXCSpeechRecognitionHandler::OnAlert(const PXCSpeechRecognition::AlertData *data)
{
	if (mPaused) return;

	if ((mRSFloater) && (mRSOnStatus))
	{
		S32 status = TARealSense::STATUS_UNKNOWN;
		
		switch (data->label)
		{
			case PXCSpeechRecognition::ALERT_VOLUME_HIGH:				status = TARealSense::STATUS_VOLUME_HIGH; break;
			case PXCSpeechRecognition::ALERT_VOLUME_LOW:				status = TARealSense::STATUS_VOLUME_LOW; break;
			case PXCSpeechRecognition::ALERT_SNR_LOW:					status = TARealSense::STATUS_SNR_LOW; break;
			case PXCSpeechRecognition::ALERT_SPEECH_UNRECOGNIZABLE:		status = TARealSense::STATUS_SPEECH_UNRECOGNIZABLE; break;
			case PXCSpeechRecognition::ALERT_SPEECH_BEGIN:				status = TARealSense::STATUS_SPEECH_BEGIN; break;
			case PXCSpeechRecognition::ALERT_SPEECH_END:				status = TARealSense::STATUS_SPEECH_END; break;
			case PXCSpeechRecognition::ALERT_RECOGNITION_ABORTED:		status = TARealSense::STATUS_RECOGNITION_ABORTED; break;
			case PXCSpeechRecognition::ALERT_RECOGNITION_END:			status = TARealSense::STATUS_RECOGNITION_ENDED; break;
		}

		if (TARealSense::STATUS_UNKNOWN != status)
		{
			mRSOnStatus(mRSFloater, status);
		}
	}
}

void TARealSense::onVoiceStatus(void* obj, S32 status)
{
	// Nothing for now
}

void TARealSense::onVoiceRecognition(void* obj, const TCommandActionArgument* pCommandActionArgument, U32 confidence)
{
	if (obj)
	{
		TARealSense* self = (TARealSense*)obj;

		if (confidence > self->mVoiceCommandThreshold)
		{
			self->triggerAction(pCommandActionArgument, false, false);
		}
	}
}

void TARealSense::onVoiceDictation(void* obj, const pxcCHAR* pSentence, bool lastSentence)
{
	if ((obj) && (pSentence))
	{
		TARealSense* self = (TARealSense*)obj;

		self->setFocusedControlText(pSentence);

		if ((lastSentence) && (self->mPXCSpeechRecognition_Dictation))
		{
			self->mPXCSpeechRecognition_Dictation->StopRec();

			if ((self->mPXCSpeechRecognition) && (self->mPXCAudioSource))
			{
				pxcStatus result = self->mPXCSpeechRecognition->StartRec(self->mPXCAudioSource, &(self->mPXCSpeechRecognitionHandler));

				if (PXC_STATUS_NO_ERROR != result)
				{
					self->releaseSpeechRecognition();

					llinfos << "Error starting RealSense audio device for voice commands after dictation. Error code: " << result << llendl;
				}
			}
		}
	}
}

void TARealSense::onModuleProcessedFrame(void* self, pxcUID streamID, PXCBase* module, PXCCapture::Sample* sample)
{
	if (!self) return;

	if (gFocusMgr.getAppHasFocus())
	{
		if ((module->QueryInstance(PXCHandModule::CUID)) && ((TARealSense*)self)->handTrackingActive())
		{
			((TARealSense*)self)->updateHandState();
			((TARealSense*)self)->noteGestures();
			((TARealSense*)self)->mHandsDirty = true;
		}

		if ((module->QueryInstance(PXCFaceModule::CUID)) && ((TARealSense*)self)->poseTrackingActive())
		{
			((TARealSense*)self)->notePose();
			((TARealSense*)self)->mFaceDirty = true;
		}

		((TARealSense*)self)->mEmotionsDirty |= (NULL != module->QueryInstance(PXCEmotion::CUID));
	}

	if ((sample) && (sample->color) && 
		(((TARealSense*)self)->mImageDataHandlerObject) && (((TARealSense*)self)->mOnImageData))
	{
		PXCImage::ImageInfo imageInfo = sample->color->QueryInfo();

		PXCImage::ImageData imageData;
		pxcStatus result = sample->color->AcquireAccess(PXCImage::ACCESS_READ, PXCImage::PIXEL_FORMAT_RGB32, 
														&imageData);
		if (PXC_STATUS_NO_ERROR <= result)
		{
			((TARealSense*)self)->mOnImageData(((TARealSense*)self)->mImageDataHandlerObject, &imageData, &imageInfo);
			sample->color->ReleaseAccess(&imageData);
		}
	}
}
#endif

void TARealSense::processFrame()
{
#if TA_INCLUDE_REALSENSE
	if (mShutdownRealSense)
	{
		enableRealSense(false);
	}
	else if (mRestartSenseManager)
	{
		enableRealSense(true);
		runSenseManager(mHandTrackingEnabled, mPoseTrackingEnabled, mEmotionTrackingEnabled, 
						mHandTrackingPaused, mPoseTrackingPaused, mEmotionTrackingPaused);
	}
	else if (realSenseEnabled())
	{
		if (mHandsDirty)
		{
			// Gestures
			
			if (mBuildMode)
			{
				if (BUILD_HAND_TRACKING_SPECIAL == mHandBuildMode)
				{
					if (mNTouchlessCursorUpdates) moveTouchlessCursor();
					handleSpecialBuildModeGestures();
					if (HANDBUILDSTATE_IDLE == mHandBuildState) handleNormalGestures(true);
				}
				else if (BUILD_HAND_TRACKING_NORMAL == mHandBuildMode)
				{
					if (mNTouchlessCursorUpdates) moveTouchlessCursor();
					if (HANDBUILDSTATE_IDLE == mHandBuildState) handleNormalGestures(false);
				}
				else // BUILD_HAND_TRACKING_OFF
				{
					// No hands
				}
			}
			else
			{
				// Prepare for the next time we enter Build mode

				mNRSMassCenterLeftUpdates = 0;
				mNRSMassCenterRightUpdates = 0;
				mNRSDistanceUpdates = 0;
				mNInitialRSMassCenterLeftUpdates = 0;
				mNInitialRSMassCenterRightUpdates = 0;
				mInitialCameraObjectDistance = 0.0;

				// Handle cursor and gestures

				if (mNTouchlessCursorUpdates) moveTouchlessCursor();
				handleNormalGestures(false);
			}

			cleanGestures();
			mHandsDirty = false;
		}

		// Poses

		if ((0 == gMenusVisible) && (0 == gPieMenusVisible) && mFaceDirty)
		{
			handlePose();

			mFaceDirty = false;
		}

		mLastPoseTime = gFrameTimeSeconds;

		// Emotion

		if (mEmotionsDirty && emotionTrackingActive())
		{
			handleEmotions();

			mEmotionsDirty = false;
		}

		// Keep moving?

		if (gAgentAvatarp->isSitting())
		{
			mGear = 0;
			mGearVertical = 0;
		}
		else if ((!(handTrackingActive() || voiceCommandsActive())) || 
				 (mBuildMode && (BUILD_HAND_TRACKING_NORMAL != mHandBuildMode)))
		{
			mGear = 0;
			mGearTurning = 0;
			mGearVertical = 0;
		}

		if (0 < mGear)
		{
			gAgent.moveAt(1);
		}
		else if (0 > mGear)
		{
			gAgent.moveAt(-1);
		}

		if (0 < mGearTurning)
		{
			if (gFrameTimeSeconds - mLastGearTurning > mMinGearTurningIntervalSeconds)
			{
				gAgent.moveYaw(1);
				if (1 < mGearTurning) gAgent.moveYaw(1);
				mLastGearTurning = gFrameTimeSeconds;
			}
		}
		else if (0 > mGearTurning)
		{
			if (gFrameTimeSeconds - mLastGearTurning > mMinGearTurningIntervalSeconds)
			{
				gAgent.moveYaw(-1);
				if (-1 > mGearTurning) gAgent.moveYaw(-1);
				mLastGearTurning = gFrameTimeSeconds;
			}
		}

		if (0 < mGearVertical)
		{
			gAgent.moveUp(1);
			if (1 < mGearVertical) gAgent.moveUp(1);
		}
		else if (0 > mGearVertical)
		{
			if (gAgent.getFlying())
			{
				gAgent.moveUp(-1);
				if (-1 > mGearVertical) gAgent.moveUp(-1);
			}
			else
			{
				mGearVertical = 0;
			}
		}
	}
#endif
}

void TARealSense::setOnImageData(void* pImageDataHandlerObject, void (*pOnImageData)(void*, void*, void*))
{
#if TA_INCLUDE_REALSENSE
	mImageDataHandlerObject = pImageDataHandlerObject;
	mOnImageData = pOnImageData;
#endif
}

bool TARealSense::realSenseAvailable()
{
#if TA_INCLUDE_REALSENSE
	if (mPXCSession) return true;

	bool result = false;

	try
	{
		PXCSession* testSession = PXCSession::CreateInstance();
		if (testSession)
		{
			result = true;
			testSession->Release();
		}
	}
	catch (const std::exception& e)
	{
		llinfos << "std::exception thrown trying to initialize RealSense: " << e.what() << llendl;
	}
	catch(...)
	{
		llinfos << "Unknown exception thrown trying to initialize RealSense" << llendl;
	}

	return result;
#else
	return false;
#endif
}

void TARealSense::releaseSenseManager()
{
#if TA_INCLUDE_REALSENSE
	if (mPXCSenseManager)
	{
		llinfos << "Closing RealSense SenseManager pipeline" << llendflush;

		mPXCSenseManager->Close();

		// Close() releases all SenseManager modules

		mPXCFaceModule = NULL;
		mPXCHandModule = NULL;

		if (mPXCFaceConfiguration)
		{
			llinfos << "Releasing RealSense FaceConfiguration" << llendflush;

			mPXCFaceConfiguration->Release();
			mPXCFaceConfiguration = NULL;
		}

		if (mPXCFaceData)
		{
			llinfos << "Releasing RealSense FaceData" << llendflush;

			mPXCFaceData->Release();
			mPXCFaceData = NULL;
		}

		if (mPXCHandConfiguration)
		{
			llinfos << "Releasing RealSense HandConfiguration" << llendflush;

			mPXCHandConfiguration->Release();
			mPXCHandConfiguration = NULL;
		}

		if (mPXCHandData)
		{
			llinfos << "Releasing RealSense HandData" << llendflush;

			mPXCHandData->Release();
			mPXCHandData = NULL;
		}

		llinfos << "Releasing RealSense SenseManager" << llendflush;

		mPXCSenseManager->Release();
		mPXCSenseManager = NULL;

		llinfos << "RealSense SenseManager released" << llendflush;
	}

	mEmotionTrackingEnabled = false;
	mEmotionTrackingPaused = false;

	mPoseTrackingEnabled = false;
	mPoseTrackingPaused = false;

	mHandTrackingEnabled = false;
	mHandTrackingPaused = false;
#endif
}

void TARealSense::enableRealSense(bool doenable)
{
#if TA_INCLUDE_REALSENSE
	if (mShuttingDownRealSense) return;

	if (doenable)
	{
		if (mPXCSession)
		{
			llinfos << "Using existing RealSense session" << llendflush;
		}
		else
		{
			llinfos << "Creating RealSense session" << llendflush;

			mPXCSenseManager = NULL;
			mPXCSpeechRecognition = NULL;
			mPXCSpeechRecognition_Dictation = NULL;
			mPXCSession = PXCSession::CreateInstance();
		}

		if (mPXCSession)
		{
			llinfos << "Querying RealSense version" << llendflush;
			PXCSession::ImplVersion ver = mPXCSession->QueryVersion();
			llinfos << "RealSense version is: " << ver.major << "." << ver.minor  << llendflush;
		}
		else
		{
			llinfos << "Failed to create RealSense session" << llendflush;
		}
	}
	else
	{
		mShuttingDownRealSense = true;

		releaseSpeechRecognition();

		if (mPXCSession)
		{
			releaseSenseManager();

			llinfos << "Releasing RealSense session" << llendflush;

			mPXCSession->Release();
			mPXCSession = NULL;

			llinfos << "RealSense session closed" << llendflush;
		}

		mShutdownRealSense = false;
		mShuttingDownRealSense = false;
	}
#else
	llinfos << "RealSense unavailable" << llendflush;
#endif
}

bool TARealSense::realSenseEnabled()
{
#if TA_INCLUDE_REALSENSE
	return (NULL != mPXCSession);
#else
	return false;
#endif
}

void TARealSense::releaseSpeechRecognition()
{
#if TA_INCLUDE_REALSENSE
	pauseVoiceCommands(true);

	PXCSpeechRecognition* p = mPXCSpeechRecognition;

	mPXCSpeechRecognition = NULL;

	if (p)
	{
		p->StopRec();
		p->Release();
	}

	p = mPXCSpeechRecognition_Dictation;

	mPXCSpeechRecognition_Dictation = NULL;

	if (p)
	{
		p->StopRec();
		p->Release();
	}

	if (mPXCAudioSource)
	{
		mPXCAudioSource->Release();

		mPXCAudioSource = NULL;
	}

	if (mVoiceCommands)
	{
		for (size_t i = 0; i < mNVoiceCommands; i++)
		{
			delete [] mVoiceCommands[i];
		}

		mNVoiceCommands = 0;

		delete [] mVoiceCommands;

		mVoiceCommands = NULL;
	}

	mLastActionTrigger.pCommandActionArgument = NULL;

	pauseVoiceCommands(false);
#else
	llinfos << "RealSense unavailable" << llendflush;
#endif
}

void TARealSense::enableVoiceCommands(bool dopause)
{
#if TA_INCLUDE_REALSENSE
	if (!realSenseEnabled()) return;

	if (0 >= mNVoiceCommands) return;

	if (!mPXCAudioSource)
	{
		mPXCAudioSource = mPXCSession->CreateAudioSource();

		if (!mPXCAudioSource)
		{
			releaseSpeechRecognition();
		
			llinfos << "Failed to create RealSense audio source." << llendl;

			return;
		}
	}

	// Set input device

	if (L"" != mVoiceInputDevice) setVoiceInputDevice(mVoiceInputDevice);

	// Set input level

	setVoiceInputLevel(mVoiceInputLevel);

	// Start and optionally pause

	pxcStatus result = mPXCSpeechRecognition->StartRec(mPXCAudioSource, &mPXCSpeechRecognitionHandler);

	if (PXC_STATUS_NO_ERROR != result)
	{
		releaseSpeechRecognition();

		llinfos << "Error starting RealSense audio device for voice commands. Error code: " << result << llendl;

		return;
	}

	pauseVoiceCommands(dopause);
#else
	llinfos << "RealSense voice commands unavailable" << llendflush;
#endif
}

bool TARealSense::voiceCommandsEnabled()
{
#if TA_INCLUDE_REALSENSE
	return ((NULL != mPXCSpeechRecognition) && (NULL != mPXCAudioSource));
#else
	return false;
#endif
}

void TARealSense::pauseVoiceCommands(bool dopause)
{
#if TA_INCLUDE_REALSENSE
	if (voiceCommandsEnabled())
	{
		mPXCSpeechRecognitionHandler.setPaused(dopause);
	}
#else
	llinfos << "RealSense voice commands unavailable" << llendflush;
#endif
}

bool TARealSense::voiceCommandsPaused()
{
#if TA_INCLUDE_REALSENSE
	return mPXCSpeechRecognitionHandler.getPaused();
#else
	return false;
#endif
}

bool TARealSense::voiceCommandsActive()
{
#if TA_INCLUDE_REALSENSE
	return (voiceCommandsEnabled() &&  (!voiceCommandsPaused()));
#else
	return false;
#endif
}

void TARealSense::setVoiceInputDevice(std::wstring deviceID)
{
#if TA_INCLUDE_REALSENSE
	if (voiceCommandsEnabled())
	{
		pauseVoiceCommands(true);
		mPXCSpeechRecognition->StopRec();
		mPXCSpeechRecognition_Dictation->StopRec();

		mPXCAudioSource->ScanDevices();
		int nDevices = mPXCAudioSource->QueryDeviceNum();
		
		PXCAudioSource::DeviceInfo dinfo; 
		for (int i = nDevices; i > 0; i--)
		{
			pxcStatus queryDeviceResult = mPXCAudioSource->QueryDeviceInfo(i - 1, &dinfo);

			if (PXC_STATUS_NO_ERROR == queryDeviceResult)
			{
				std::wstring queried_deviceID = dinfo.did;
				if (queried_deviceID == deviceID)
				{
					pxcStatus setDeviceResult = mPXCAudioSource->SetDevice(&dinfo);

					if (PXC_STATUS_NO_ERROR == setDeviceResult)
					{
						mVoiceInputDevice = deviceID;
					}
					else
					{
						std::string id(deviceID.begin(), deviceID.end());
						llinfos << "Error setting RealSense voice input device " << id << ". Error code: " << setDeviceResult << llendl;
					}

					return;
				}
			}
			else
			{
				llinfos << "Error querying RealSense info for voice input device #" << i << ". Error code: " << queryDeviceResult << llendl;
			}
		}

		std::string id(deviceID.begin(), deviceID.end());
		llinfos << "RealSense voice input device not found. Device ID: " << id << llendl;
	}
	else
	{
		mVoiceInputDevice = deviceID;
	}
#else
	llinfos << "RealSense voice commands unavailable" << llendflush;
#endif
}

void TARealSense::setVoiceInputLevel(U32 level)
{
#if TA_INCLUDE_REALSENSE
	mVoiceInputLevel = level;

	if (voiceCommandsEnabled())
	{
		pxcF32 volume = min(1.0, max(0.0, 0.01 * mVoiceInputLevel));
		pxcStatus result = mPXCAudioSource->SetVolume(volume);

		if (PXC_STATUS_NO_ERROR != result)
		{
			llinfos << "Failed to set RealSense audio input level to " << volume << ". Error code: " << result << llendl;
		}
	}
#else
	llinfos << "RealSense voice commands unavailable" << llendflush;
#endif
}

void TARealSense::setVoiceCommandThreshold(U32 threshold)
{
#if TA_INCLUDE_REALSENSE
	mVoiceCommandThreshold = threshold;
#else
	llinfos << "RealSense voice commands unavailable" << llendflush;
#endif
}

void TARealSense::setVoiceCommands(TCommandActionArgumentVector commandActionArgument)
{
#if TA_INCLUDE_REALSENSE
	releaseSpeechRecognition();

	if (!realSenseEnabled()) return;

	if (0 >= commandActionArgument.size()) return;

	pxcStatus result = mPXCSession->CreateImpl<PXCSpeechRecognition>(&mPXCSpeechRecognition);

	if (PXC_STATUS_NO_ERROR != result)
	{
		releaseSpeechRecognition();
		llinfos << "Error instanting RealSense speech recognition for voice commands. Error code: " << result << llendl;

		return;
	}

	result = mPXCSession->CreateImpl<PXCSpeechRecognition>(&mPXCSpeechRecognition_Dictation);

	if (PXC_STATUS_NO_ERROR != result)
	{
		releaseSpeechRecognition();
		llinfos << "Error instanting RealSense speech recognition for dictation Error code: " << result << llendl;

		return;
	}

	// Set speech recognition language

	PXCSpeechRecognition::ProfileInfo pinfo;

	mPXCSpeechRecognition->QueryProfile(0, &pinfo);

	pinfo.language = PXCSpeechRecognition::LANGUAGE_US_ENGLISH;	// Default to US English

	std::string ui_language = LLUI::getLanguage();
	std::transform(ui_language.begin(), ui_language.end(), ui_language.begin(), tolower);

	if ("en-gb" == ui_language) pinfo.language = PXCSpeechRecognition::LANGUAGE_GB_ENGLISH; else
	if ("es-us" == ui_language) pinfo.language = PXCSpeechRecognition::LANGUAGE_US_SPANISH; else
	{
		ui_language = ui_language.substr(0, 2);

		if ("de" == ui_language) pinfo.language = PXCSpeechRecognition::LANGUAGE_DE_GERMAN; else
		if ("es" == ui_language) pinfo.language = PXCSpeechRecognition::LANGUAGE_LA_SPANISH; else
		if ("fr" == ui_language) pinfo.language = PXCSpeechRecognition::LANGUAGE_FR_FRENCH; else
		if ("it" == ui_language) pinfo.language = PXCSpeechRecognition::LANGUAGE_IT_ITALIAN; else
		if ("jp" == ui_language) pinfo.language = PXCSpeechRecognition::LANGUAGE_JP_JAPANESE; else
		if ("zh" == ui_language) pinfo.language = PXCSpeechRecognition::LANGUAGE_CN_CHINESE; else
		if ("br" == ui_language) pinfo.language = PXCSpeechRecognition::LANGUAGE_BR_PORTUGUESE; else
		if ("ru" == ui_language) pinfo.language = PXCSpeechRecognition::LANGUAGE_RU_RUSSIAN;
	}

	result = mPXCSpeechRecognition->SetProfile(&pinfo);

	if (PXC_STATUS_NO_ERROR != result)
	{
		releaseSpeechRecognition();
		llinfos << "Error setting RealSense speech recognition profile for voice commands. Error code: " << result << llendl;

		return;
	}

	result = mPXCSpeechRecognition_Dictation->SetProfile(&pinfo);

	if (PXC_STATUS_NO_ERROR != result)
	{
		releaseSpeechRecognition();
		llinfos << "Error setting RealSense speech recognition profile for dictation. Error code: " << result << llendl;

		return;
	}

	// Set commands

	mNVoiceCommands = commandActionArgument.size();
	mVoiceCommands = new pxcCHAR*[mNVoiceCommands];

	for (size_t i = 0; i < mNVoiceCommands; i++)
	{
		mVoiceCommands[i] = new pxcCHAR[commandActionArgument[i].command.length() + 1];
		wcscpy(mVoiceCommands[i], commandActionArgument[i].command.c_str());
	}

	result = mPXCSpeechRecognition->BuildGrammarFromStringList(1, mVoiceCommands, NULL, mNVoiceCommands);

	if (PXC_STATUS_NO_ERROR != result)
	{
		releaseSpeechRecognition();
		llinfos << "Error building RealSense voice command grammar. Error code: " << result << llendl;

		return;
	}

	result = mPXCSpeechRecognition->SetGrammar(1);

	if (PXC_STATUS_NO_ERROR != result)
	{
		releaseSpeechRecognition();
		llinfos << "Error setting RealSense voice command grammar. Error code: " << result << llendl;

		return;
	}

	mPXCSpeechRecognitionHandler.setVoiceCommands(commandActionArgument);
#else
	llinfos << "RealSense voice commands unavailable" << llendflush;
#endif
}

void TARealSense::setRSFloaterCallbacks(void* pFloater, void (*pOnStatus)(void*, S32), 
										void (*pOnRecognition)(void*, const TCommandActionArgument*, U32))
{
#if TA_INCLUDE_REALSENSE
	mPXCSpeechRecognitionHandler.setRSFloaterCallbacks(pFloater, pOnStatus, pOnRecognition);
#else
	llinfos << "RealSense voice commands unavailable" << llendflush;
#endif
}

TARealSense::TVoiceInputDeviceVector TARealSense::getVoiceInputDevices()
{
	TVoiceInputDeviceVector devices;

#if TA_INCLUDE_REALSENSE
	if (mPXCSession)
	{
		PXCAudioSource *source = mPXCSession->CreateAudioSource();

		if (source)
		{
			source->ScanDevices();
			int nDevices = source->QueryDeviceNum();
		
			PXCAudioSource::DeviceInfo dinfo; 
			for (int i = nDevices; i > 0; i--)
			{
				pxcStatus result = source->QueryDeviceInfo(i - 1, &dinfo);
				if (PXC_STATUS_NO_ERROR == result)
				{
					TVoiceInputDevice device;
					device.name = dinfo.name;
					device.id = dinfo.did;

					devices.push_back(device);
				}
				else
				{
					llinfos << "Error querying RealSense info for audio device #" << i << ". Error code: " << result << llendl;
				}
			}

			source->Release();
		}
		else
		{
			llinfos << "Failed to create RealSense audio source." << llendl;
		}
	}
#endif

	return devices;
}

void TARealSense::loadVoiceCommandList(bool defaults)
{
	TCommandActionArgumentVector commandActionArgumentVector;

	U32 nCommands = loadControlValue("RealSenseVoiceCommandCount", defaults).asInteger();

	for (U32 i = 0; i < nCommands; i++)
	{
		TCommandActionArgument commandActionArgument;

		std::string nr = std::to_string((ULONGLONG)i);

		std::string command = loadControlValue("RealSenseVoiceCommand#" + nr, defaults);

		boost::algorithm::trim(command);
		if (command.empty()) continue;

		commandActionArgument.command = s2ws(command);
		commandActionArgument.action = stringToAction(loadControlValue("RealSenseVoiceAction#" + nr, defaults));

		if (!commandActionArgument.action) continue;

		commandActionArgument.argument = loadControlValue("RealSenseVoiceArgument#" + nr, defaults);

		commandActionArgument.overrider = -1;	  // Not set yet
		commandActionArgument.nextCandidate = -1; // Not set yet

		commandActionArgumentVector.push_back(commandActionArgument);
	}

	setVoiceCommands(commandActionArgumentVector);
}

void TARealSense::restoreVoiceDefaults()
{
#if TA_INCLUDE_REALSENSE
	setVoiceInputDevice(s2ws(loadControlValue("RealSenseVoiceInputDevice", true)));

	mVoiceInputLevel = loadControlValue("RealSenseVoiceInputLevel", true).asInteger();
	mVoiceCommandThreshold = loadControlValue("RealSenseVoiceCommandThreshold", true).asInteger();

	loadVoiceCommandList(true);
#endif
}

void TARealSense::setBuildMode(bool buildmode)
{
#if TA_INCLUDE_REALSENSE
	if (buildmode == mBuildMode) return;

	mBuildMode = buildmode;

	if (mBuildMode)
	{
		if (mDragLeft) simulateMouseUp(false);
		if (mDragRight) simulateMouseUp(true);
	}
#endif
}

std::wstring TARealSense::s2ws(std::string s)
{
	std::wstring ws(s.begin(), s.end());
	return ws;
}

std::string TARealSense::ws2s(std::wstring ws)
{
	std::string s(ws.begin(), ws.end());
	return s;
}

S32 TARealSense::stringToAction(std::string s)
{
	if ("Again" == s) return ACTION_AGAIN;
	if ("Step" == s) return ACTION_STEP;
	if ("Step back" == s) return ACTION_STEPBACK;
	if ("Forward" == s) return ACTION_FORWARD;
	if ("Backward" == s) return ACTION_BACKWARD;
	if ("Run" == s) return ACTION_RUN;
	if ("Run back" == s) return ACTION_RUNBACK;
	if ("Stop horizontal" == s) return ACTION_STOPHORIZONTAL;
	if ("Left" == s) return ACTION_LEFT;
	if ("Right" == s) return ACTION_RIGHT;
	if ("Turn left" == s) return ACTION_TURNLEFT;
	if ("Turn right" == s) return ACTION_TURNRIGHT;
	if ("Stop turning" == s) return ACTION_STOPTURNING;
	if ("Slide left" == s) return ACTION_SLIDELEFT;
	if ("Slide right" == s) return ACTION_SLIDERIGHT;
	if ("Up" == s) return ACTION_UP;
	if ("Down" == s) return ACTION_DOWN;
	if ("Climb" == s) return ACTION_CLIMB;
	if ("Descend" == s) return ACTION_DESCEND;
	if ("Stop vertical" == s) return ACTION_STOPVERTICAL;
	if ("Fly" == s) return ACTION_FLY;
	if ("Stop flying" == s) return ACTION_STOPFLYING;
	if ("Stop" == s) return ACTION_STOP;
	if ("Slow" == s) return ACTION_SLOW;
	if ("Fast" == s) return ACTION_FAST;
	if ("Whisper" == s) return ACTION_WHISPER;
	if ("Say" == s) return ACTION_SAY;
	if ("Shout" == s) return ACTION_SHOUT;
	if ("Regionsay" == s) return ACTION_REGIONSAY;
	if ("Talk on" == s) return ACTION_TALKON;
	if ("Talk off" == s) return ACTION_TALKOFF;
	if ("Dictate" == s) return ACTION_DICTATE;
	if ("Clear" == s) return ACTION_CLEAR;
	if ("Keystroke" == s) return ACTION_KEYSTROKE;
	if ("Left click" == s) return ACTION_LEFTCLICK;
	if ("Right click" == s) return ACTION_RIGHTCLICK;
	if ("Left drag" == s) return ACTION_LEFTDRAG;
	if ("Right drag" == s) return ACTION_RIGHTDRAG;
	if ("XUI click" == s) return ACTION_XUICLICK;

	return ACTION_NONE;
}

std::string TARealSense::actionToString(S32 action)
{
	switch (action)
	{
		case ACTION_AGAIN:			return "Again";
		case ACTION_STEP:			return "Step";
		case ACTION_STEPBACK:		return "Step back";
		case ACTION_FORWARD:		return "Forward";
		case ACTION_BACKWARD:		return "Backward";
		case ACTION_RUN:			return "Run";
		case ACTION_RUNBACK:		return "Run back";
		case ACTION_STOPHORIZONTAL:	return "Stop horizontal";
		case ACTION_LEFT:			return "Left";
		case ACTION_RIGHT:			return "Right";
		case ACTION_TURNLEFT:		return "Turn left";
		case ACTION_TURNRIGHT:		return "Turn right";
		case ACTION_STOPTURNING:	return "Stop turning";
		case ACTION_SLIDELEFT:		return "Slide left";
		case ACTION_SLIDERIGHT:		return "Slide right";
		case ACTION_UP:				return "Up";
		case ACTION_DOWN:			return "Down";
		case ACTION_CLIMB:			return "Climb";
		case ACTION_DESCEND:		return "Descend";
		case ACTION_STOPVERTICAL:	return "Stop vertical";
		case ACTION_FLY:			return "Fly";
		case ACTION_STOPFLYING:		return "Stop flying";
		case ACTION_STOP:			return "Stop";
		case ACTION_SLOW:			return "Slow";
		case ACTION_FAST:			return "Fast";
		case ACTION_WHISPER:		return "Whisper";
		case ACTION_SAY:			return "Say";
		case ACTION_SHOUT:			return "Shout";
		case ACTION_REGIONSAY:		return "Regionsay";
		case ACTION_TALKON:			return "Talk on";
		case ACTION_TALKOFF:		return "Talk off";
		case ACTION_DICTATE:		return "Dictate";
		case ACTION_CLEAR:			return "Clear";
		case ACTION_KEYSTROKE:		return "Keystroke";
		case ACTION_LEFTCLICK:		return "Left click";
		case ACTION_RIGHTCLICK:		return "Right click";
		case ACTION_LEFTDRAG:		return "Left drag";
		case ACTION_RIGHTDRAG:		return "Right drag";
		case ACTION_XUICLICK:		return "XUI click";

		default:					return "No action";
	}
}

LLSD TARealSense::loadControlValue(const std::string& name, bool default)
{
	if (default) return gSavedPerAccountSettings.getControl(name)->getDefault();
	return gSavedPerAccountSettings.getControl(name)->getValue();
}

void TARealSense::setTouchlessMouseEnabled(bool enable)
{
#if TA_INCLUDE_REALSENSE
	mTouchlessMouseEnabled = enable;
#endif
}

bool TARealSense::getTouchlessMouseEnabled()
{
#if TA_INCLUDE_REALSENSE
	return mTouchlessMouseEnabled;
#else
	return false;
#endif
}

void TARealSense::setTouchlessOffset(F32 x, F32 y)
{
#if TA_INCLUDE_REALSENSE
	mTouchlessOffset.x = (x + 1.0) * 0.5;
	mTouchlessOffset.y = (y + 1.0) * 0.5;
#endif
}

void TARealSense::setHandSpeed(F32 speed)
{
#if TA_INCLUDE_REALSENSE
	mHandSpeed = speed;
#endif
}

void TARealSense::setHandSmoothing(F32 smoothing)
{
#if TA_INCLUDE_REALSENSE
	mSmoothingFactor = 1.0 - smoothing;
#endif
}

void TARealSense::setHandReloadSeconds(F32 reloadSeconds)
{
#if TA_INCLUDE_REALSENSE
	mHandReloadSeconds = reloadSeconds;
#endif
}

void TARealSense::requestRealSenseShutdown()
{
#if TA_INCLUDE_REALSENSE
	mShutdownRealSense = true;
#endif
}

void TARealSense::requestSenseManagerReset()
{
#if TA_INCLUDE_REALSENSE
	mRestartSenseManager = true;
#endif
}

void TARealSense::runSenseManager(bool hands, bool poses, bool emotions, 
								  bool pauseHands, bool pausePoses, bool pauseEmotions)
{
#if TA_INCLUDE_REALSENSE
	if (mStartingSenseManager) return;

	mStartingSenseManager = true;

	releaseSenseManager();

	PXCSenseManager* newPXCSenseManager = mPXCSession->CreateSenseManager(); // PXCSenseManager::CreateInstance();

	if (newPXCSenseManager)
	{

		if (emotions)
		{
			llinfos << "Enabling RealSense emotion tracking." << llendflush;
		
			pxcStatus enableEmoResult = newPXCSenseManager->EnableEmotion();

			if (PXC_STATUS_NO_ERROR != enableEmoResult)
			{
				emotions = false;
				llinfos << "Could not enable RealSense emotion tracking. Error code: " << enableEmoResult << llendflush;
			}
		}

		if (poses)
		{
			poses = false;

			llinfos << "Enabling RealSense pose tracking." << llendflush;

			pxcStatus enableFaceResult = newPXCSenseManager->EnableFace();

			if (PXC_STATUS_NO_ERROR == enableFaceResult)
			{
				// Configure face tracking

				mPXCFaceModule = newPXCSenseManager->QueryFace();

				if (mPXCFaceModule)
				{
					mPXCFaceConfiguration = mPXCFaceModule->CreateActiveConfiguration();

					if (mPXCFaceConfiguration)
					{
						llinfos << "Configuring RealSense face tracking." << llendflush;

						mPXCFaceConfiguration->SetTrackingMode(PXCFaceConfiguration::FACE_MODE_COLOR_PLUS_DEPTH);
						mPXCFaceConfiguration->detection.isEnabled = true; // To get face location, including depth
						mPXCFaceConfiguration->landmarks.isEnabled = false;
						mPXCFaceConfiguration->pose.isEnabled = true;
						mPXCFaceConfiguration->pose.maxTrackedFaces = 10;

						llinfos << "Applying RealSense face tracking configuration changes." << llendflush;

						pxcStatus applyChangesResult = mPXCFaceConfiguration->ApplyChanges();

						if (PXC_STATUS_NO_ERROR == applyChangesResult)
						{
							mPXCFaceData = mPXCFaceModule->CreateOutput();

							if (mPXCFaceData)
							{
								poses = true;
							}
							else
							{
								llinfos << "Could not create RealSense FaceData instance." << llendflush;
							}
						}
						else
						{
							llinfos << "Could not apply RealSense face tracking configuration changes. Error code: " << applyChangesResult << llendflush;
						}
					}
					else
					{
						llinfos << "Could not create RealSense FaceConfiguration." << llendflush;
					}
				}
				else
				{
					llinfos << "Could not acquire RealSense FaceModule." << llendflush;
				}
			}
			else
			{
				llinfos << "Could not enable RealSense face tracking. Error code: " << enableFaceResult << llendflush;
			}
		}

		if (hands)
		{
			hands = false;

			llinfos << "Enabling RealSense hand tracking." << llendflush;

			pxcStatus enableHandResult = newPXCSenseManager->EnableHand();

			if (PXC_STATUS_NO_ERROR == enableHandResult)
			{
				// Configure hand tracking

				mPXCHandModule = newPXCSenseManager->QueryHand();

				if (mPXCHandModule)
				{
					mPXCHandConfiguration = mPXCHandModule->CreateActiveConfiguration();

					if (mPXCHandConfiguration)
					{
						llinfos << "Configuring RealSense hand tracking." << llendflush;

						// mPXCHandConfiguration->EnableAllGestures(true);
						
						pxcI32 nGestures = mPXCHandConfiguration->QueryGesturesTotalNumber();

						llinfos << "Number of available gestures: " << nGestures << llendflush;

						for (int i = 0; i < nGestures; i++)
						{
							PXCHandData::GestureData gestureData;
							if (PXC_STATUS_NO_ERROR == mPXCHandConfiguration->QueryGestureNameByIndex(i, PXCHandData::MAX_NAME_SIZE, gestureData.name))
							{
								std::wstring gestureName(gestureData.name);
								std::string str(gestureName.begin(), gestureName.end());

								if (ACTION_NONE == gesture2action(gestureName, true))
								{
									llinfos << "Disabling gesture #" << i << ": " << str << llendflush;

									pxcStatus disableGestureResult = mPXCHandConfiguration->DisableGesture(gestureData.name);

									if (PXC_STATUS_NO_ERROR != disableGestureResult)
									{
										llinfos << "Could not disable gesture #" << i << ". Error code: " << disableGestureResult << llendflush;
									}
								}
								else
								{
									llinfos << "Enabling gesture #" << i << ": " << str << llendflush;

									pxcStatus enableGestureResult = mPXCHandConfiguration->EnableGesture(gestureData.name, true);

									if (PXC_STATUS_NO_ERROR != enableGestureResult)
									{
										llinfos << "Could not enable gesture #" << i << ". Error code: " << enableGestureResult << llendflush;
									}
								}
							}
						}

						llinfos << "Applying RealSense hand tracking configuration changes." << llendflush;

						pxcStatus applyChangesResult = mPXCHandConfiguration->ApplyChanges();

						if (PXC_STATUS_NO_ERROR == applyChangesResult)
						{
							llinfos << "Updating RealSense hand tracking configuration." << llendflush;

							pxcStatus updateResult = mPXCHandConfiguration->Update();

							// RS SDK BUG? This check used to work with version 3, but updateResult is always PXC_STATUS_NOT_INITIALIZED with version 4.
							// Ignoring it seems to work... :(
							if (true) // (PXC_STATUS_NO_ERROR == updateResult)
							{
								mPXCHandData = mPXCHandModule->CreateOutput();

								if (mPXCHandData)
								{
									hands = true;
								}
								else
								{
									llinfos << "Could not create RealSense HandData instance." << llendflush;
								}
							}
							else
							{
								llinfos << "Could not update RealSense hand tracking configuration. Error code: " << updateResult << llendflush;
							}
						}
						else
						{
							llinfos << "Could not apply RealSense hand tracking configuration changes. Error code: " << applyChangesResult << llendflush;
						}
					}
					else
					{
						llinfos << "Could not create RealSense HandConfiguration." << llendflush;
					}
				}
				else
				{
					llinfos << "Could not acquire RealSense HandModule." << llendflush;
				}
			}
			else
			{
				llinfos << "Could not enable RealSense hand tracking. Error code: " << enableHandResult << llendflush;
			}
		}

		if ((emotions) || (poses) || (hands))
		{
			llinfos << "Initializing RealSense SenseManager pipeline." << llendflush;

			if (PXC_STATUS_NO_ERROR == newPXCSenseManager->Init(&mPXCSenseManagerHandler))
			{
				mPXCSenseManager = newPXCSenseManager;

				llinfos << "RealSense SenseManager pipeline initialized." << llendflush;

				mEmotionTrackingEnabled = emotions;
				mPoseTrackingEnabled = poses;
				mHandTrackingEnabled = hands;

				pauseEmotionTracking(pauseEmotions);
				pausePoseTracking(pausePoses);
				pauseHandTracking(pauseHands);

				llinfos << "Querying RealSense capture manager." << llendflush;

				PXCCaptureManager *pPXCCaptureManager = mPXCSenseManager->QueryCaptureManager();

				if (pPXCCaptureManager)
				{
					pxcStatus locateStreamsResult = pPXCCaptureManager->LocateStreams();

					if (PXC_STATUS_NO_ERROR == locateStreamsResult)
					{
						PXCCapture::Device *pDevice = pPXCCaptureManager->QueryDevice();

						if (pDevice)
						{
							PXCCapture::DeviceInfo dinfo;
							memset(&dinfo, 0, sizeof(dinfo));
							pDevice->QueryDeviceInfo(&dinfo);

							llinfos << "RealSense capture device model: " << dinfo.model << llendflush;

							if (dinfo.model == PXCCapture::DEVICE_MODEL_IVCAM)
							{
								pxcStatus setDepthConfidenceThresholdResult = pDevice->SetDepthConfidenceThreshold(1);

								if (PXC_STATUS_NO_ERROR != setDepthConfidenceThresholdResult)
								{
									llinfos << "Could not set RealSense depth confidence threshold. Error code: " << 
												setDepthConfidenceThresholdResult << llendflush;
								}

								pxcStatus setIVCAMFilterOptionResult = pDevice->SetIVCAMFilterOption(6);

								if (PXC_STATUS_NO_ERROR != setIVCAMFilterOptionResult)
								{
									llinfos << "Could not set RealSense IVCAM filter option. Error code: " << 
												setIVCAMFilterOptionResult << llendflush;
								}
							}
						}
						else
						{
							llinfos << "Could not query RealSense capture device." << llendflush;
						}
					}
					else
					{
						llinfos << "Could not locate RealSense streams. Error code: " << locateStreamsResult << llendflush;
					}

					pxcStatus streamFramesResult = mPXCSenseManager->StreamFrames(FALSE);

					if (PXC_STATUS_NO_ERROR != streamFramesResult)
					{
						llinfos << "Could not start RealSense frame stream. Error code: " << streamFramesResult << llendflush;
					}

					// pPXCCaptureManager is internally managed, do not release it.
				}
				else
				{
					llinfos << "Could not query RealSense capture manager." << llendflush;
				}
			}
			else
			{
				llinfos << "Failed to initialize RealSense SenseManager pipeline." << llendflush;
			}
		}
	}
	else
	{
		llinfos << "Failed to create RealSense SenseManager." << llendflush;
	}

	mRestartSenseManager = false;
	mStartingSenseManager = false;
#endif
}

void TARealSense::enableEmotionTracking(bool dopause)
{
#if TA_INCLUDE_REALSENSE
	if (emotionTrackingEnabled())
	{
		pauseEmotionTracking(dopause);
		return;
	}

	mEmotionTrackingEnabled = true;
	mEmotionTrackingPaused = dopause;

	requestSenseManagerReset();
#else
	llinfos << "RealSense emotion tracking unavailable" << llendflush;
#endif
}

bool TARealSense::emotionTrackingEnabled()
{
#if TA_INCLUDE_REALSENSE
	return (NULL != mPXCSenseManager) && (mEmotionTrackingEnabled);
#else
	return false;
#endif
}

void TARealSense::pauseEmotionTracking(bool dopause)
{
#if TA_INCLUDE_REALSENSE
	if (emotionTrackingEnabled())
	{
		mPXCSenseManager->PauseEmotion(dopause);
		mEmotionTrackingPaused = dopause;
	}
#else
	llinfos << "RealSense emotion tracking unavailable" << llendflush;
#endif
}

bool TARealSense::emotionTrackingPaused()
{
#if TA_INCLUDE_REALSENSE
	return mEmotionTrackingPaused;
#else
	return false;
#endif
}

bool TARealSense::emotionTrackingActive()
{
#if TA_INCLUDE_REALSENSE
	return (emotionTrackingEnabled() && (!mEmotionTrackingPaused));
#else
	return false;
#endif
}

void TARealSense::setEmotionThresholds(F32 lo, F32 hi)
{
#if TA_INCLUDE_REALSENSE
	mLoEmoThreshold = lo;
	mHiEmoThreshold = hi;
#endif
}

void TARealSense::setAngerEvidenceBias(S32 bias)
{
#if TA_INCLUDE_REALSENSE
	mAngerEvidenceBias = bias;
#endif
}

void TARealSense::setContemptEvidenceBias(S32 bias)
{
#if TA_INCLUDE_REALSENSE
	mContemptEvidenceBias = bias;
#endif
}

void TARealSense::setDisgustEvidenceBias(S32 bias)
{
#if TA_INCLUDE_REALSENSE
	mDisgustEvidenceBias = bias;
#endif
}

void TARealSense::setFearEvidenceBias(S32 bias)
{
#if TA_INCLUDE_REALSENSE
	mFearEvidenceBias = bias;
#endif
}

void TARealSense::setJoyEvidenceBias(S32 bias)
{
#if TA_INCLUDE_REALSENSE
	mJoyEvidenceBias = bias;
#endif
}

void TARealSense::setSadnessEvidenceBias(S32 bias)
{
#if TA_INCLUDE_REALSENSE
	mSadnessEvidenceBias = bias;
#endif
}

void TARealSense::setSurpriseEvidenceBias(S32 bias)
{
#if TA_INCLUDE_REALSENSE
	mSurpriseEvidenceBias = bias;
#endif
}

void TARealSense::restoreEmotionDefaults()
{
#if TA_INCLUDE_REALSENSE
	mLoEmoThreshold = loadControlValue("RealSenseEmotionLoThreshold", true);
	mHiEmoThreshold = loadControlValue("RealSenseEmotionHiThreshold", true);

	mAngerEvidenceBias	  = loadControlValue("RealSenseEmotionEvidenceBiasAnger", true);
	mContemptEvidenceBias = loadControlValue("RealSenseEmotionEvidenceBiasContempt", true);
	mDisgustEvidenceBias  = loadControlValue("RealSenseEmotionEvidenceBiasDisgust", true);
	mFearEvidenceBias     = loadControlValue("RealSenseEmotionEvidenceBiasFear", true);
	mJoyEvidenceBias      = loadControlValue("RealSenseEmotionEvidenceBiasJoy", true);
	mSadnessEvidenceBias  = loadControlValue("RealSenseEmotionEvidenceBiasSadness", true);
	mSurpriseEvidenceBias = loadControlValue("RealSenseEmotionEvidenceBiasSurprise", true);
#endif
}

void TARealSense::loadPersonalRealSenseSettings()
{
#if TA_INCLUDE_REALSENSE
	setTouchlessMouseEnabled(gSavedPerAccountSettings.getBOOL("RealSenseTouchlessMouseEnabled"));
	setTouchlessOffset(gSavedPerAccountSettings.getF32("RealSenseTouchlessOffsetX"), 
					   gSavedPerAccountSettings.getF32("RealSenseTouchlessOffsetY"));
	setHandSpeed(gSavedPerAccountSettings.getF32("RealSenseHandSpeed"));
	setHandSmoothing(gSavedPerAccountSettings.getF32("RealSenseHandSmoothing"));
	setHandReloadSeconds(gSavedPerAccountSettings.getF32("RealSenseHandReloadSeconds"));

	setHandBuildMode(gSavedPerAccountSettings.getS32("RealSenseHandBuildMode"));

	setSpreadFingersAction(gSavedPerAccountSettings.getString("RealSenseGestureSpreadFingersAction"));
	setFistAction(gSavedPerAccountSettings.getString("RealSenseGestureFistAction"));
	setTapAction(gSavedPerAccountSettings.getString("RealSenseGestureTapAction"));
	setThumbDownAction(gSavedPerAccountSettings.getString("RealSenseGestureThumbDownAction"));
	setThumbUpAction(gSavedPerAccountSettings.getString("RealSenseGestureThumbUpAction"));
	setTwoFingersPinchOpenAction(gSavedPerAccountSettings.getString("RealSenseGestureTwoFingersPinchOpenAction"));
	setVSignAction(gSavedPerAccountSettings.getString("RealSenseGestureVSignAction"));
	setFullPinchAction(gSavedPerAccountSettings.getString("RealSenseGestureFullPinchAction"));
	setSwipeLeftAction(gSavedPerAccountSettings.getString("RealSenseGestureSwipeLeftAction"));
	setSwipeRightAction(gSavedPerAccountSettings.getString("RealSenseGestureSwipeRightAction"));
	setWaveAction(gSavedPerAccountSettings.getString("RealSenseGestureWaveAction"));
	setClickAction(gSavedPerAccountSettings.getString("RealSenseGestureClickAction"));
	setSwipeDownAction(gSavedPerAccountSettings.getString("RealSenseGestureSwipeDownAction"));
	setSwipeUpAction(gSavedPerAccountSettings.getString("RealSenseGestureSwipeUpAction"));

	setSpreadFingersArgument(gSavedPerAccountSettings.getString("RealSenseGestureSpreadFingersArgument"));
	setFistArgument(gSavedPerAccountSettings.getString("RealSenseGestureFistArgument"));
	setTapArgument(gSavedPerAccountSettings.getString("RealSenseGestureTapArgument"));
	setThumbDownArgument(gSavedPerAccountSettings.getString("RealSenseGestureThumbDownArgument"));
	setThumbUpArgument(gSavedPerAccountSettings.getString("RealSenseGestureThumbUpArgument"));
	setTwoFingersPinchOpenArgument(gSavedPerAccountSettings.getString("RealSenseGestureTwoFingersPinchOpenArgument"));
	setVSignArgument(gSavedPerAccountSettings.getString("RealSenseGestureVSignArgument"));
	setFullPinchArgument(gSavedPerAccountSettings.getString("RealSenseGestureFullPinchArgument"));
	setSwipeLeftArgument(gSavedPerAccountSettings.getString("RealSenseGestureSwipeLeftArgument"));
	setSwipeRightArgument(gSavedPerAccountSettings.getString("RealSenseGestureSwipeRightArgument"));
	setWaveArgument(gSavedPerAccountSettings.getString("RealSenseGestureWaveArgument"));
	setClickArgument(gSavedPerAccountSettings.getString("RealSenseGestureClickArgument"));
	setSwipeDownArgument(gSavedPerAccountSettings.getString("RealSenseGestureSwipeDownArgument"));
	setSwipeUpArgument(gSavedPerAccountSettings.getString("RealSenseGestureSwipeUpArgument"));

	setYawDegreesOffset(gSavedPerAccountSettings.getF32("RealSensePoseYawDegreesOffset"));
	setPitchDegreesOffset(gSavedPerAccountSettings.getF32("RealSensePosePitchDegreesOffset"));
	setRollDegreesOffset(gSavedPerAccountSettings.getF32("RealSensePoseRollDegreesOffset"));
	setYawDegreesThreshold(gSavedPerAccountSettings.getF32("RealSensePoseYawDegreesThreshold"));
	setPitchUpDegreesThreshold(gSavedPerAccountSettings.getF32("RealSensePosePitchUpDegreesThreshold"));
	setPitchDownDegreesThreshold(gSavedPerAccountSettings.getF32("RealSensePosePitchDownDegreesThreshold"));
	setRollDegreesThreshold(gSavedPerAccountSettings.getF32("RealSensePoseRollDegreesThreshold"));
	setYawMaxMovesPerSecond(gSavedPerAccountSettings.getF32("RealSensePoseYawMaxMovesPerSecond"));
	setPitchMaxMovesPerSecond(gSavedPerAccountSettings.getF32("RealSensePosePitchMaxMovesPerSecond"));
	setRollMaxMovesPerSecond(gSavedPerAccountSettings.getF32("RealSensePoseRollMaxMovesPerSecond"));

	setPoseBuildMode(gSavedPerAccountSettings.getS32("RealSensePoseBuildMode"));

	setEmotionThresholds(gSavedPerAccountSettings.getF32("RealSenseEmotionLoThreshold"), 
						 gSavedPerAccountSettings.getF32("RealSenseEmotionHiThreshold"));

	setAngerEvidenceBias(gSavedPerAccountSettings.getS32("RealSenseEmotionEvidenceBiasAnger"));
	setContemptEvidenceBias(gSavedPerAccountSettings.getS32("RealSenseEmotionEvidenceBiasContempt"));
	setDisgustEvidenceBias(gSavedPerAccountSettings.getS32("RealSenseEmotionEvidenceBiasDisgust"));
	setFearEvidenceBias(gSavedPerAccountSettings.getS32("RealSenseEmotionEvidenceBiasFear"));
	setJoyEvidenceBias(gSavedPerAccountSettings.getS32("RealSenseEmotionEvidenceBiasJoy"));
	setSadnessEvidenceBias(gSavedPerAccountSettings.getS32("RealSenseEmotionEvidenceBiasSadness"));
	setSurpriseEvidenceBias(gSavedPerAccountSettings.getS32("RealSenseEmotionEvidenceBiasSurprise"));

	mHandTrackingEnabled = gSavedPerAccountSettings.getBOOL("RealSenseHandTrackingEnabled");
	mPoseTrackingEnabled = gSavedPerAccountSettings.getBOOL("RealSensePoseTrackingEnabled");
	mEmotionTrackingEnabled = gSavedPerAccountSettings.getBOOL("RealSenseEmotionTrackingEnabled");

	setVoiceInputDevice(s2ws(gSavedPerAccountSettings.getString("RealSenseVoiceInputDevice")));

	mVoiceInputLevel = gSavedPerAccountSettings.getU32("RealSenseVoiceInputLevel");
	mVoiceCommandThreshold = gSavedPerAccountSettings.getU32("RealSenseVoiceCommandThreshold");

	// Voice commands can only be loaded when RealSense is enabled

	if (gSavedSettings.getBOOL("RealSenseEnabled") && realSenseAvailable())
	{
		requestSenseManagerReset();

		if (gSavedPerAccountSettings.getBOOL("RealSenseVoiceCommandsEnabled"))
		{
			enableRealSense(true);
			loadVoiceCommandList(false);
			enableVoiceCommands(voiceCommandsPaused());
		}
	}
	else
	{
		requestRealSenseShutdown();
	}
#endif
}

void TARealSense::enablePoseTracking(bool dopause)
{
#if TA_INCLUDE_REALSENSE
	if (poseTrackingEnabled())
	{
		pausePoseTracking(dopause);
		return;
	}

	mPoseTrackingEnabled = true;
	mPoseTrackingPaused = dopause;

	requestSenseManagerReset();
#else
	llinfos << "RealSense pose tracking unavailable" << llendflush;
#endif
}

bool TARealSense::poseTrackingEnabled()
{
#if TA_INCLUDE_REALSENSE
	return ((NULL != mPXCSenseManager) && (mPoseTrackingEnabled));
#else
	return false;
#endif
}

void TARealSense::pausePoseTracking(bool dopause)
{
#if TA_INCLUDE_REALSENSE
	if (poseTrackingEnabled())
	{
		mPXCSenseManager->PauseFace(dopause);
		mPoseTrackingPaused = dopause;
	}
#else
	llinfos << "RealSense pose tracking unavailable" << llendflush;
#endif
}

bool TARealSense::poseTrackingPaused()
{
#if TA_INCLUDE_REALSENSE
	return mPoseTrackingPaused;
#else
	return false;
#endif
}

bool TARealSense::poseTrackingActive()
{
#if TA_INCLUDE_REALSENSE
	return (poseTrackingEnabled() && (!mPoseTrackingPaused));
#else
	return false;
#endif
}

void TARealSense::setYawDegreesOffset(F32 offset)
{
#if TA_INCLUDE_REALSENSE
	mYawDegreesOffset = offset;
#endif
}

void TARealSense::setPitchDegreesOffset(F32 offset)
{
#if TA_INCLUDE_REALSENSE
	mPitchDegreesOffset = offset;
#endif
}

void TARealSense::setRollDegreesOffset(F32 offset)
{
#if TA_INCLUDE_REALSENSE
	mRollDegreesOffset = offset;
#endif
}

void TARealSense::setYawDegreesThreshold(F32 degrees)
{
#if TA_INCLUDE_REALSENSE
	mYawDegreesThreshold = degrees;
#endif
}

void TARealSense::setPitchUpDegreesThreshold(F32 degrees)
{
#if TA_INCLUDE_REALSENSE
	mPitchUpDegreesThreshold = degrees;
#endif
}

void TARealSense::setPitchDownDegreesThreshold(F32 degrees)
{
#if TA_INCLUDE_REALSENSE
	mPitchDownDegreesThreshold = degrees;
#endif
}

void TARealSense::setRollDegreesThreshold(F32 degrees)
{
#if TA_INCLUDE_REALSENSE
	mRollDegreesThreshold = degrees;
#endif
}

void TARealSense::setYawMaxMovesPerSecond(F32 movesPerSecond)
{
#if TA_INCLUDE_REALSENSE
	mYawMaxMovesPerSecond = movesPerSecond;
#endif
}

void TARealSense::setPitchMaxMovesPerSecond(F32 movesPerSecond)
{
#if TA_INCLUDE_REALSENSE
	mPitchMaxMovesPerSecond = movesPerSecond;
#endif
}

void TARealSense::setRollMaxMovesPerSecond(F32 movesPerSecond)
{
#if TA_INCLUDE_REALSENSE
	mRollMaxMovesPerSecond = movesPerSecond;
#endif
}

void TARealSense::setPoseBuildMode(S32 buildMode)
{
#if TA_INCLUDE_REALSENSE
	mPoseBuildMode = buildMode;
#endif
}

void TARealSense::restorePoseDefaults()
{
#if TA_INCLUDE_REALSENSE
	mYawDegreesOffset			= loadControlValue("RealSensePoseYawDegreesOffset", true);
	mPitchDegreesOffset			= loadControlValue("RealSensePosePitchDegreesOffset", true);
	mRollDegreesOffset			= loadControlValue("RealSensePoseRollDegreesOffset", true);
	
	mYawDegreesThreshold		= loadControlValue("RealSensePoseYawDegreesThreshold", true);
	mPitchUpDegreesThreshold	= loadControlValue("RealSensePosePitchUpDegreesThreshold", true);
	mPitchDownDegreesThreshold	= loadControlValue("RealSensePosePitchDownDegreesThreshold", true);
	mRollDegreesThreshold		= loadControlValue("RealSensePoseRollDegreesThreshold", true);

	mYawMaxMovesPerSecond		= loadControlValue("RealSensePoseYawMaxMovesPerSecond", true);
	mPitchMaxMovesPerSecond		= loadControlValue("RealSensePosePitchMaxMovesPerSecond", true);
	mRollMaxMovesPerSecond		= loadControlValue("RealSensePoseRollMaxMovesPerSecond", true);

	mPoseBuildMode				= loadControlValue("RealSensePoseBuildMode", true);
#endif
}

void TARealSense::enableHandTracking(bool dopause)
{
#if TA_INCLUDE_REALSENSE
	if (handTrackingEnabled())
	{
		pauseHandTracking(dopause);
		return;
	}

	mHandTrackingEnabled = true;
	mHandTrackingPaused = dopause;

	requestSenseManagerReset();
#else
	llinfos << "RealSense gesture tracking unavailable" << llendflush;
#endif
}

bool TARealSense::handTrackingEnabled()
{
#if TA_INCLUDE_REALSENSE
	return ((NULL != mPXCSenseManager) && (mHandTrackingEnabled));
#else
	return false;
#endif
}

void TARealSense::pauseHandTracking(bool dopause)
{
#if TA_INCLUDE_REALSENSE
	if (handTrackingEnabled())
	{
		mPXCSenseManager->PauseHand(dopause);
		mHandTrackingPaused = dopause;
	}
#else
	llinfos << "RealSense pose tracking unavailable" << llendflush;
#endif
}

bool TARealSense::handTrackingPaused()
{
#if TA_INCLUDE_REALSENSE
	return mHandTrackingPaused;
#else
	return false;
#endif
}

bool TARealSense::handTrackingActive()
{
#if TA_INCLUDE_REALSENSE
	return (handTrackingEnabled() && (!mHandTrackingPaused));
#else
	return false;
#endif
}

void TARealSense::setHandBuildMode(S32 buildMode)
{
#if TA_INCLUDE_REALSENSE
	mHandBuildMode = buildMode;
#endif
}

void TARealSense::restoreHandDefaults()
{
#if TA_INCLUDE_REALSENSE
	setTouchlessMouseEnabled(loadControlValue("RealSenseTouchlessMouseEnabled", true));
	setTouchlessOffset(loadControlValue("RealSenseTouchlessOffsetX", true),
					   loadControlValue("RealSenseTouchlessOffsetY", true));
	setHandSpeed(loadControlValue("RealSenseHandSpeed", true));
	setHandSmoothing(loadControlValue("RealSenseHandSmoothing", true));
	setHandReloadSeconds(loadControlValue("RealSenseHandReloadSeconds", true));

	setHandBuildMode(loadControlValue("RealSenseHandBuildMode", true));
#endif
}

void TARealSense::setGestureAction(std::string action_string, S32 &action)
{
#if TA_INCLUDE_REALSENSE
	S32 new_action = stringToAction(action_string);
	if (new_action != action)
	{
		action = new_action;
		requestSenseManagerReset();
	}
#endif
}

void TARealSense::setSpreadFingersAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mSpreadFingersCommandActionArgument.action);
#endif
}

void TARealSense::setFistAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mFistCommandActionArgument.action);
#endif
}

void TARealSense::setTapAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mTapCommandActionArgument.action);
#endif
}

void TARealSense::setThumbDownAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mThumbDownCommandActionArgument.action);
#endif
}

void TARealSense::setThumbUpAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mThumbUpCommandActionArgument.action);
#endif
}

void TARealSense::setTwoFingersPinchOpenAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mTwoFingersPinchOpenCommandActionArgument.action);
#endif
}

void TARealSense::setVSignAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mVSignCommandActionArgument.action);
#endif
}

void TARealSense::setFullPinchAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mFullPinchCommandActionArgument.action);
#endif
}

void TARealSense::setSwipeLeftAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mSwipeLeftCommandActionArgument.action);
#endif
}

void TARealSense::setSwipeRightAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mSwipeRightCommandActionArgument.action);
#endif
}

void TARealSense::setWaveAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mWaveCommandActionArgument.action);
#endif
}

void TARealSense::setClickAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mClickCommandActionArgument.action);
#endif
}

void TARealSense::setSwipeDownAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mSwipeDownCommandActionArgument.action);
#endif
}

void TARealSense::setSwipeUpAction(std::string action_string)
{
#if TA_INCLUDE_REALSENSE
	setGestureAction(action_string, mSwipeUpCommandActionArgument.action);
#endif
}

void TARealSense::setSpreadFingersArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mSpreadFingersCommandActionArgument.argument = argument;
#endif
}

void TARealSense::setFistArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mFistCommandActionArgument.argument = argument;
#endif
}

void TARealSense::setTapArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mTapCommandActionArgument.argument = argument;
#endif
}

void TARealSense::setThumbDownArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mThumbDownCommandActionArgument.argument = argument;
#endif
}

void TARealSense::setThumbUpArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mThumbUpCommandActionArgument.argument = argument;
#endif
}

void TARealSense::setTwoFingersPinchOpenArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mTwoFingersPinchOpenCommandActionArgument.argument = argument;
#endif
}

void TARealSense::setVSignArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mVSignCommandActionArgument.argument = argument;
#endif
}

void TARealSense::setFullPinchArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mFullPinchCommandActionArgument.argument = argument;
#endif
}

void TARealSense::setSwipeLeftArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mSwipeLeftCommandActionArgument.argument = argument;
#endif
}

void TARealSense::setSwipeRightArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mSwipeRightCommandActionArgument.argument = argument;
#endif
}

void TARealSense::setWaveArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mWaveCommandActionArgument.argument = argument;
#endif
}

void TARealSense::setClickArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mClickCommandActionArgument.argument = argument;
#endif
}

void TARealSense::setSwipeDownArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mSwipeDownCommandActionArgument.argument = argument;
#endif
}

void TARealSense::setSwipeUpArgument(std::string argument)
{
#if TA_INCLUDE_REALSENSE
	mSwipeUpCommandActionArgument.argument = argument;
#endif
}

void TARealSense::restoreGestureDefaults()
{
#if TA_INCLUDE_REALSENSE
	mSpreadFingersCommandActionArgument.action			= stringToAction(loadControlValue("RealSenseGestureSpreadFingersAction", true));
	mFistCommandActionArgument.action					= stringToAction(loadControlValue("RealSenseGestureFistAction", true));
	mTapCommandActionArgument.action					= stringToAction(loadControlValue("RealSenseGestureTapAction", true));
	mThumbDownCommandActionArgument.action				= stringToAction(loadControlValue("RealSenseGestureThumbDownAction", true));
	mThumbUpCommandActionArgument.action				= stringToAction(loadControlValue("RealSenseGestureThumbUpAction", true));
	mTwoFingersPinchOpenCommandActionArgument.action	= stringToAction(loadControlValue("RealSenseGestureTwoFingersPinchOpenAction", true));
	mVSignCommandActionArgument.action					= stringToAction(loadControlValue("RealSenseGestureVSignAction", true));
	mFullPinchCommandActionArgument.action				= stringToAction(loadControlValue("RealSenseGestureFullPinchAction", true));
	mSwipeLeftCommandActionArgument.action				= stringToAction(loadControlValue("RealSenseGestureSwipeLeftAction", true));
	mSwipeRightCommandActionArgument.action				= stringToAction(loadControlValue("RealSenseGestureSwipeRightAction", true));
	mWaveCommandActionArgument.action					= stringToAction(loadControlValue("RealSenseGestureWaveAction", true));
	mClickCommandActionArgument.action					= stringToAction(loadControlValue("RealSenseGestureClickAction", true));
	mSwipeDownCommandActionArgument.action				= stringToAction(loadControlValue("RealSenseGestureSwipeDownAction", true));
	mSwipeUpCommandActionArgument.action				= stringToAction(loadControlValue("RealSenseGestureSwipeUpAction", true));

	mSpreadFingersCommandActionArgument.argument		= loadControlValue("RealSenseGestureSpreadFingersArgument", true);
	mFistCommandActionArgument.argument					= loadControlValue("RealSenseGestureFistArgument", true);
	mTapCommandActionArgument.argument					= loadControlValue("RealSenseGestureTapArgument", true);
	mThumbDownCommandActionArgument.argument			= loadControlValue("RealSenseGestureThumbDownArgument", true);
	mThumbUpCommandActionArgument.argument				= loadControlValue("RealSenseGestureThumbUpArgument", true);
	mTwoFingersPinchOpenCommandActionArgument.argument	= loadControlValue("RealSenseGestureTwoFingersPinchOpenArgument", true);
	mVSignCommandActionArgument.argument				= loadControlValue("RealSenseGestureVSignArgument", true);
	mFullPinchCommandActionArgument.argument			= loadControlValue("RealSenseGestureFullPinchArgument", true);
	mSwipeLeftCommandActionArgument.argument			= loadControlValue("RealSenseGestureSwipeLeftArgument", true);
	mSwipeRightCommandActionArgument.argument			= loadControlValue("RealSenseGestureSwipeRightArgument", true);
	mWaveCommandActionArgument.argument					= loadControlValue("RealSenseGestureWaveArgument", true);
	mClickCommandActionArgument.argument				= loadControlValue("RealSenseGestureClickArgument", true);
	mSwipeDownCommandActionArgument.argument			= loadControlValue("RealSenseGestureSwipeDownArgument", true);
	mSwipeUpCommandActionArgument.argument				= loadControlValue("RealSenseGestureSwipeUpArgument", true);

#endif
}

